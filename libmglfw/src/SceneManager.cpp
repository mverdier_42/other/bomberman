/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SceneManager.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrouthie <jrouthie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/29 23:35:02 by jrouthie          #+#    #+#             */
/*   Updated: 2018/05/18 18:28:08 by jrouthie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SceneManager.hpp"
#include "Core.hpp"

SceneManager::SceneManager() :
	_activeScene(NULL)
{
	
}
SceneManager::~SceneManager()
{
	for (size_t i = 0; i < _scenes.size(); ++i)
		delete _scenes[i];
	_scenes.clear();
}
void SceneManager::update(Core &core)
{
	for	(unsigned int i = 0; i < _scenes.size(); ++i)
		_scenes[i]->update(core);
}
void SceneManager::updateLoop(Core &core)
{
	for	(unsigned int i = 0; i < _scenes.size(); ++i)
		_scenes[i]->updateLoop(core);
}
void SceneManager::render(Core &core)
{
	for	(unsigned int i = 0; i < _scenes.size(); ++i)
		_scenes[i]->render(core);
}
void SceneManager::eventResize(Core &core, int width, int height)
{
	for	(unsigned int i = 0; i < _scenes.size(); ++i)
		_scenes[i]->eventResize(core, width, height);
}
void SceneManager::setActiveScene(Scene *s)
{
	size_t i = 0;
	for (std::vector<Scene*>::iterator it = _scenes.begin();
		it != _scenes.end(); it++, i++)
	{
		if (*it == s)
		{
			_activeScene = s;
			return ;
		}
    }
	throw Except("setActiveScene scene not avalide");
}
void SceneManager::setActiveScene(const std::string &name)
{
	for (std::vector<Scene*>::iterator it = _scenes.begin();
		it != _scenes.end(); it++)
	{
		if ((*it)->getName() == name)
		{
			_activeScene = *it;
			return ;
		}
    }
	throw Except("setActiveScene scene not avalide");
}
Scene &SceneManager::getActiveScene()
{
	if (_scenes.empty() || !_activeScene)
		throw Except("getActiveScene not scene avalide");
	return (*_activeScene);
}
void SceneManager::addScene(Scene *s)
{
	for (std::vector<Scene*>::iterator it = _scenes.begin();
		it != _scenes.end(); it++)
		if (*it == s)
			throw Except("addScene already exist");
	_scenes.push_back(s);
	if (!Core::instance)
		throw Except("Core not init");
	Core::instance->sendEventResize(*s);
}
void SceneManager::addSceneBack(Scene *s)
{
	for (std::vector<Scene*>::iterator it = _scenes.begin();
		it != _scenes.end(); it++)
		if (*it == s)
			throw Except("addScene already exist");
	_scenes.insert(_scenes.begin(), s);
}
Scene *SceneManager::getScene(const std::string &name)
{
	for (std::vector<Scene*>::iterator it = _scenes.begin();
		it != _scenes.end(); it++)
	{
		if ((*it)->getName() == name)
			return (*it);
    }
	return (NULL);
}
void SceneManager::removeScene(Scene *s)
{
	if (_activeScene == s)
		_activeScene = NULL;
	for (std::vector<Scene*>::iterator it = _scenes.begin();
		it != _scenes.end(); it++)
	{
		if (*it == s)
		{
			_scenes.erase(it);
			return ;
		}
    }
	throw Except("removeScene scene not exist");
}