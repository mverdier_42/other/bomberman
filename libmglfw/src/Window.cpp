#include <cmath>
#include <iostream>
#include "Window.hpp"
#include "Core.hpp"

static void character_callback(GLFWwindow* window, unsigned int codepoint)
{
	Window	*win;

	(void)codepoint;
	win = reinterpret_cast<Window *>(glfwGetWindowUserPointer(window));
	if (win->getCallback())
		win->getCallback()->eventChar(codepoint);
}

static void key_callback(GLFWwindow* window, int key, int scancode,
	int action, int mods)
{
	Window	*win;

	(void)scancode;
	(void)mods;
	win = reinterpret_cast<Window *>(glfwGetWindowUserPointer(window));
	if (win->getCallback())
	{
		if (action == GLFW_PRESS)
			win->getCallback()->eventKeyPress(key, mods);
		else if (action == GLFW_RELEASE)
			win->getCallback()->eventKeyRelease(key, mods);
	}
	/*if (action == GLFW_PRESS)
	{
		if (key == GLFW_KEY_ESCAPE)
			win->close();
		else if (key == GLFW_KEY_SPACE)
		{
		}
	}*/
}

static void win_resize_callback(GLFWwindow *window, int width, int height)
{
	Window	*win;

	win = reinterpret_cast<Window *>(glfwGetWindowUserPointer(window));
	win->setSize(width, height);
	glViewport(0, 0, width, height);
	if (win->getCallback())
		win->getCallback()->eventResize(width, height);
}


static void win_mouse_move_callback(GLFWwindow *window, double x, double y)
{
	Window	*win;

	win = reinterpret_cast<Window *>(glfwGetWindowUserPointer(window));
	win->setMousePosCallback(x, y);
	if (win->getCallback())
		win->getCallback()->eventMouseMove(x, y);
}

static void win_mouse_button_callback(GLFWwindow *window, int button, int action, int mods)
{
	Window	*win;

	win = reinterpret_cast<Window *>(glfwGetWindowUserPointer(window));
	if (win->getCallback())
	{
		if (action == GLFW_PRESS)
			win->getCallback()->eventButtonPress(button, mods);
		else if (action == GLFW_RELEASE)
			win->getCallback()->eventButtonRelease(button, mods);
	}
}

static void win_scroll_callback(GLFWwindow *window, double x, double y)
{
	Window	*win;

	win = reinterpret_cast<Window *>(glfwGetWindowUserPointer(window));
	if (win->getCallback())
		win->getCallback()->eventScroll(x, y);
}

static void win_close_callback(GLFWwindow *window)
{
	Window	*win;

	win = reinterpret_cast<Window *>(glfwGetWindowUserPointer(window));
	if (win->getCallback())
		win->getCallback()->eventClose();
}


Window::Window(const std::string &name, unsigned int width, unsigned int height) :
	_callback(NULL)
{
	if (!glfwInit())
	{
		throw std::exception();//"Erreur init glfw!"
	}
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	//glfwWindowHint(GLFW_DECORATED, false);
	//glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	// GLFWmonitor *m = glfwGetPrimaryMonitor();
	// const GLFWvidmode *vm = glfwGetVideoMode(m);
	// height = vm->height - 22;
	if ((_win = glfwCreateWindow(width, height, name.c_str(), NULL, NULL)))
	{
		// glfwSetWindowPos(_win, vm->width - width, 22);
		glfwMakeContextCurrent(_win);
		glfwSetWindowUserPointer(_win, this);//glfwGetWindowUserPointer(window)
		glfwSetCharCallback(_win, character_callback);
		glfwSetKeyCallback(_win, key_callback);
		glfwSetCursorPosCallback(_win, win_mouse_move_callback);
		glfwSetMouseButtonCallback(_win, win_mouse_button_callback);
		glfwSetScrollCallback(_win, win_scroll_callback);
		glfwSetFramebufferSizeCallback(_win, win_resize_callback);
		glfwSetWindowCloseCallback(_win, win_close_callback);
		glewExperimental = GL_TRUE;
		if (glewInit() != GLEW_OK)
		{
			glfwDestroyWindow(_win);
			glfwTerminate();
			throw std::exception();//"Erreur init glew!"
		}
		win_resize_callback(_win, width, height);
	}
	else
	{
		glfwTerminate();
		throw std::exception();//"La fenetre n'a pas pu etre creer."
	}
	glEnable(GL_BLEND);
	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glfwSwapInterval(1);//sincro vertical
	glEnable(GL_CULL_FACE);
	// glCullFace(GL_FRONT);
	// glCullFace(GL_BACK);
	// gettimeofday(&_tvStart, 0);
}

Window::~Window()
{
	if (_win)
		glfwDestroyWindow(_win);
	glfwTerminate();
}

Window::Window(Window const &copy)
{
	*this = copy;
}

Window	&Window::operator=(Window const &rhs)
{
	(void)rhs;
	return (*this);
}

void Window::loop(void)
{
	while (isOpen())
	{
		update();
		render();
	}
}

void Window::update(void)
{
	glfwPollEvents();
}

void Window::render(void)
{
	// glBindFramebuffer(GL_FRAMEBUFFER, 0);
	// glViewport(0, 0, _width, _height);
	glfwSwapBuffers(_win);
	glClearColor(0.0, 0.0, 0.0, 1);
	// glClearColor(0.6, 0.6, 0.6, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Window::close(void)
{
	glfwSetWindowShouldClose(_win, GLFW_TRUE);
}

bool Window::isOpen(void)
{
	return (!glfwWindowShouldClose(_win));
}

GLFWwindow *Window::getGLFW(void)
{
	return (_win);
}

void Window::setSize(unsigned int width, unsigned int height)
{
	_width = width;
	_height = height;
	_scale = static_cast<double>(width) / static_cast<double>(height);
}
unsigned int Window::getWidth() const
{
	return (_width);
}
unsigned int Window::getHeight() const
{
	return (_height);
}

void Window::resetElapsed()
{
	glfwSetTime(0);
	// gettimeofday(&_tvStart, 0);
}
void Window::setElapsed(double e)
{
	/*double t = floor(e);
	_tvStart.tv_sec = static_cast<long>(t);
	_tvStart.tv_usec = static_cast<long>((e - t) * 1000000.0);*/
	glfwSetTime(e);
}
double Window::getElapsed()
{
	/*timeval tv;
	gettimeofday(&tv, 0);

	return (tv.tv_sec - _tvStart.tv_sec +
		(tv.tv_usec - _tvStart.tv_usec) / 1000000.0);*/
	return (glfwGetTime());
}

void Window::setCallback(Core *callback)
{
	_callback = callback;
}
Core *Window::getCallback()
{
	return (_callback);
}
void Window::setMousePosCallback(double x, double y)
{
	_mouseX = x;
	_mouseY = y;
}
double Window::getMouseX() const
{
	return (_mouseX);
}
double Window::getMouseY() const
{
	return (_mouseY);
}
double Window::getScale() const
{
	return (_scale);
}
bool Window::isFullscreen() const
{
    return (glfwGetWindowMonitor(_win) != NULL);
}
void Window::setWindowOption(int width, int height, bool fullscreen)
{
	GLFWmonitor *monitor = glfwGetPrimaryMonitor();
	const GLFWvidmode *mode = glfwGetVideoMode(monitor);
	int xpos, ypos;
	glfwGetWindowPos(_win, &xpos, &ypos);
	if (width <= 0)
		width = mode->width;
	if (height <= 0)
		height = mode->height;
	xpos += _width / 2 - width / 2;
	ypos += _height / 2 - height / 2;
	glfwSetWindowMonitor(_win, fullscreen ? monitor : NULL, xpos, ypos, width, height, 0);
}
const GLFWvidmode *Window::getVideoModes(int &count) const
{
	return (glfwGetVideoModes(glfwGetPrimaryMonitor(), &count));
}