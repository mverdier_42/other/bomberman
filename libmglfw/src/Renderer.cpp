/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Renderer.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrouthie <jrouthie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/30 03:23:46 by jrouthie          #+#    #+#             */
/*   Updated: 2018/05/22 15:47:57 by jrouthie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Renderer.hpp"


Renderer::Renderer()
{

	Shader vec2_v("vec2_v.glsl");
	Shader vec3_v("vec3_v.glsl");
	Shader vec4_v("vec4_v.glsl");
	Shader color_f("color_f.glsl");
	Shader texture_f("ui/texture_f.glsl");

	_progArrow = new ProgramGlsl(vec2_v, color_f, Shader("arrow_g.glsl"));
	_progRect = new ProgramGlsl(vec4_v, color_f, Shader("rect_g.glsl"));
	_progTexture = new ProgramGlsl(vec4_v, texture_f, Shader("rect_g.glsl"));
	_progCircle = new ProgramGlsl(vec3_v, Shader("circle_f.glsl"), Shader("circle_g.glsl"));

	_progArrow_uni_scale = _progArrow->getUniformLocation("uni_scale");
	_progArrow_uni_color = _progArrow->getUniformLocation("uni_color");
	_progRect_uni_color = _progRect->getUniformLocation("uni_color");
	_progTexture_uni_color = _progTexture->getUniformLocation("uni_color");
	_progTexture_uni_tex = _progTexture->getUniformLocation("uni_tex");
	_progCircle_uni_scale = _progCircle->getUniformLocation("uni_scale");
	_progCircle_uni_color = _progCircle->getUniformLocation("uni_color");


	glGenVertexArrays(3, _vao);
	glGenBuffers(3, _vbo);
	for (int i = 0; i < 3; ++i)
	{
		glBindVertexArray(_vao[i]);
		glBindBuffer(GL_ARRAY_BUFFER, _vbo[i]);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2 + i, GL_FLOAT, GL_FALSE, 0, NULL);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}
	_scale = 25.0f;
}
Renderer::~Renderer()
{
	glDeleteVertexArrays(3, _vao);
	glDeleteBuffers(3, _vbo);
	delete _progArrow;
	delete _progRect;
	delete _progCircle;
}
void Renderer::resizeEvent(int width, int height)
{
	_width = width;
	_height = height;
	_textRenderer.update_font_data(width, height);
	_scale = (width > height ? height : width) * 0.05;
	_scaleX = _scale / width;
	_scaleY = _scale / height;
}
void Renderer::renderRect(const Vec4 &rect, const Vec4 &color)
{
	_progRect->bind();
	fillBuffer(rect);
	glUniform4fv(_progRect_uni_color, 1, color);
	glDrawArrays(GL_POINTS, 0, 1);
	glBindVertexArray(0);
}
void Renderer::renderTexture(const Vec4 &rect, const Vec4 &color)
{
	_progTexture->bind();
	fillBuffer(rect);
	glUniform4fv(_progTexture_uni_color, 1, color);
	glUniform1i(_progTexture_uni_tex, 0);
	glDrawArrays(GL_POINTS, 0, 1);
	glBindVertexArray(0);
}
void Renderer::renderCircle(const Vec3 &circle, const Vec4 &color)
{
	_progCircle->bind();
	fillBuffer(circle);
	glUniform2f(_progCircle_uni_scale, _scaleX, _scaleY);
	glUniform4fv(_progCircle_uni_color, 1, color);
	glDrawArrays(GL_POINTS, 0, 1);
	glBindVertexArray(0);
}
void Renderer::renderArrow(const Vec2 &pos, const Vec4 &color)
{
	_progArrow->bind();
	fillBuffer(pos);
	glUniform2f(_progArrow_uni_scale, _scaleX, _scaleY);
	glUniform4fv(_progArrow_uni_color, 1, color);
	glDrawArrays(GL_POINTS, 0, 1);
	glBindVertexArray(0);
}
void Renderer::renderText(const std::string str, const Vec2 &v)
{
	_textRenderer.render_text(str, v);
}

void Renderer::fillBuffer(const Vec2 &v)
{
	glBindBuffer(GL_ARRAY_BUFFER, _vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 2, v,
		GL_DYNAMIC_DRAW);
	glBindVertexArray(_vao[0]);
}
void Renderer::fillBuffer(const Vec3 &v)
{
	glBindBuffer(GL_ARRAY_BUFFER, _vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3, v,
		GL_DYNAMIC_DRAW);
	glBindVertexArray(_vao[1]);
}
void Renderer::fillBuffer(const Vec4 &v)
{
	glBindBuffer(GL_ARRAY_BUFFER, _vbo[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 4, v,
		GL_DYNAMIC_DRAW);
	glBindVertexArray(_vao[2]);
}