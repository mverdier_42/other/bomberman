#include <iostream>
#include <algorithm>
#include "MenuRenderer.hpp"
#include "Core.hpp"







MenuButton::MenuButton() {

}
MenuButton::MenuButton(const std::string &str, MenuCallback callback,
	void *userdata) : _txt(str), _callback(callback), _userData(userdata) {
}
MenuButton::~MenuButton() {

}
const Vec2 &MenuButton::getSize() const {
	return (_size);
}
const Vec2 &MenuButton::getPos() const {
	return (_pos);
}
void MenuButton::setPos(const Vec2 &pos) {
	_pos = pos;
}
void MenuButton::setSize(const Vec2 &size) {
	_size = size;
}
void MenuButton::resize(Core &core) {
	calcSize(core);
}
void MenuButton::click(const Vec2 &pos) {
	(void)pos;
	if (_callback)
		_callback(*this, MenuKeyEnter, _userData);
}
void MenuButton::keyPress(const MenuModuleKey key) {
	if (_callback)
		_callback(*this, key, _userData);
}
void MenuButton::render(Core &core, const Vec4 &color) {
	Renderer &rend = core.getRenderer();

	rend.renderRect(Vec4(_pos[0], _pos[1], _size[0], _size[1]),
		color);
	rend.renderText(_txt, _txtPos + _pos);
}
void MenuButton::calcSize(Core &core) {
	Renderer &rend = core.getRenderer();
	const Vec2 cSize = rend.getTextSize("X");
	_txtPos = Vec2(cSize[0], cSize[1] * 2);
	_size = rend.getTextSize(_txt) + cSize * 2;
}
void MenuButton::setText(const std::string &txt){
	_txt = txt;
	if (_parent)
		_parent->needUpdate();
}
const std::string &MenuButton::getText() const {
	return (_txt);
}








MenuRenderer::MenuRenderer() : _needUpdateRender(false), _selected(0) {
}
MenuRenderer::MenuRenderer(const MenuRenderer& src) {
	*this = src;
}
MenuRenderer::~MenuRenderer() {
	for (size_t i = 0; i < _arr.size(); ++i)
		delete _arr[i];
	_arr.clear();
}
MenuRenderer& MenuRenderer::operator=(const MenuRenderer& rhs) {
(void)rhs;
	return (*this);
}
void MenuRenderer::update(Core &core) {
	(void)core;
}
void MenuRenderer::keyPress(Core &core, int key) {
	(void)core;
	if (key == GLFW_KEY_ENTER)
		keyPress(core, MenuKeyEnter);
	else if (key == GLFW_KEY_LEFT)
		keyPress(core, MenuKeyLeft);
	else if (key == GLFW_KEY_RIGHT)
		keyPress(core, MenuKeyRight);
	else if (key == GLFW_KEY_UP)
		keyPress(core, MenuKeyUp);
	else if (key == GLFW_KEY_DOWN)
		keyPress(core, MenuKeyDown);
}
void MenuRenderer::keyPress(Core &core, MenuModuleKey key) {
	(void)core;
	if (key == MenuKeyUp)
		_selected = (_selected ? _selected : _arr.size()) - 1;
	else if (key == MenuKeyDown)
		_selected = (_selected >= _arr.size() - 1 ? 0 : _selected + 1);
	else
		_arr[_selected]->keyPress(key);
}
void MenuRenderer::buttonPress(Core &core, const Vec2 &pos) {
	(void)core;
	for (size_t i = 0; i < _arr.size(); ++i)
	{
		Vec2 p = _arr[i]->getPos();
		if (pos[0] >= p[0] && pos[1] >= p[1])
		{
			p += _arr[i]->getSize();
			if (pos[0] <= p[0] && pos[1] <= p[1])
			{
				_selected = i;
				_arr[i]->click(pos);
				return;
			}
		}
	}
}
void MenuRenderer::render(Core &core) {
	if (_needUpdateRender)
	{
		_needUpdateRender = false;
		updateRender(core);
	}
	const Vec4 colSelect(0.8, 0.5, 0.5, 1);
	const Vec4 colNormal(0.5, 0.5, 0.5, 1);
	for (size_t i = 0; i < _arr.size(); ++i)
	{
		_arr[i]->render(core, i == _selected ? colSelect : colNormal);
	}
}
void MenuRenderer::addModule(IMenuModule *module) {
	if (!module)
		return ;
	module->setParent(this);
	_arr.push_back(module);
	_needUpdateRender = true;
}
void MenuRenderer::removeModule(IMenuModule *module) {
	if (!module)
		return ;
	module->setParent(NULL);
	std::vector<IMenuModule*>::iterator pos = std::find(_arr.begin(), _arr.end(), module);
	if (pos != _arr.end())
		_arr.erase(pos);
	_needUpdateRender = true;
}
void MenuRenderer::resize(Core &core) {
	(void)core;
	_needUpdateRender = true;
}
IMenuModule *MenuRenderer::getActiveModule() {
	return _arr[_selected];
}
std::vector<IMenuModule*> *MenuRenderer::getModules() {
	return (&_arr);
}
void MenuRenderer::updateRender(Core &core) {
	float ttHeight = 0;
	float maxWidth = 0;

	if (!_arr.size())
		return ;
	for (size_t i = 0; i < _arr.size(); ++i)
	{
		_arr[i]->resize(core);
		const Vec2 &size = _arr[i]->getSize();
		ttHeight += size[1];
		if (size[0] > maxWidth)
			maxWidth = size[0];
	}
	ttHeight = ttHeight * 1.05;
	float hh = ttHeight / _arr.size();
	float hhst = ttHeight * 0.5 - hh * 0.5;
	float mid = maxWidth * -0.5;
	for (size_t i = 0; i < _arr.size(); ++i)
	{
		_arr[i]->setPos(Vec2(mid, hhst + i * -hh));
		_arr[i]->setSize(Vec2(maxWidth, _arr[i]->getSize()[1]));
	}
}
void MenuRenderer::needUpdate()
{
	_needUpdateRender = true;
}
