/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Core.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrouthie <jrouthie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/29 23:29:59 by jrouthie          #+#    #+#             */
/*   Updated: 2018/10/13 15:44:29 by jrouthie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sstream>
#include "Core.hpp"
#include "SceneManager.hpp"

Core *Core::instance = NULL;


Core::Core() : _win("bomberman"), _texManager(), _manager(), _renderer()
{
	_win.setCallback(this);
	if (instance)
		throw Except("Core already init");
	instance = this;
}
Core::~Core()
{
	instance = NULL;
}
void Core::run()
{
	double lastUpdate = 0.0f;
	double lastFps = 0.0f;
	int fps = 0;
	int frame = 0;

	eventResize(_win.getWidth(), _win.getHeight());
	_win.resetElapsed();
	while (_win.isOpen())
	{
		double elapsed = _win.getElapsed();
		
		if (elapsed > lastUpdate + (1.0 / 61.0))
		{
			if (elapsed >= lastFps + 1.0)
			{
				lastFps += 1.0;
				fps = frame;
				frame = 0;
			}
			_win.update();
			_manager.update(*this);
			std::ostringstream str;
			str << "Fps : " << fps;
			_renderer.renderText(str.str(), Vec2(-1, 1));
			_manager.render(*this);
			_win.render();
			lastUpdate = _win.getElapsed();
			frame++;
		}
		else
			_manager.updateLoop(*this);
	}
}
Renderer &Core::getRenderer()
{
	return (_renderer);
}
Window &Core::getWindow()
{
	return (_win);
}
SceneManager &Core::getSceneManager()
{
	return (_manager);
}
void Core::eventResize(int width, int height)
{
	_renderer.resizeEvent(width, height);
	_manager.eventResize(*this, width, height);
}
void Core::sendEventResize(Scene &scene)
{
	int width = _win.getWidth();
	int height = _win.getHeight();
	scene.eventResize(*this, width, height);
}
void Core::eventChar(unsigned int key)
{
	Scene &scene = _manager.getActiveScene();
	scene.eventChar(*this, key);
}
void Core::eventKeyPress(int key, int mods)
{
	Scene &scene = _manager.getActiveScene();
	scene.eventKeyPress(*this, key, mods);
}
void Core::eventKeyRelease(int key, int mods)
{
	Scene &scene = _manager.getActiveScene();
	scene.eventKeyRelease(*this, key, mods);
}
void Core::eventMouseMove(double x, double y)
{
	Scene &scene = _manager.getActiveScene();
	scene.eventMouseMove(*this, x, y);
}
void Core::eventButtonPress(int key, int mods)
{
	Scene &scene = _manager.getActiveScene();
	scene.eventButtonPress(*this, key, mods);
}
void Core::eventButtonRelease(int key, int mods)
{
	Scene &scene = _manager.getActiveScene();
	scene.eventButtonRelease(*this, key, mods);
}
void Core::eventScroll(double x, double y)
{
	Scene &scene = _manager.getActiveScene();
	scene.eventScroll(*this, x, y);
}
void Core::eventClose()
{
	Scene &scene = _manager.getActiveScene();
	scene.eventClose(*this);
}
void Core::setUserData(void *userdata)
{
	_userdata = userdata;
}
void *Core::getUserData()
{
	return (_userdata);
}