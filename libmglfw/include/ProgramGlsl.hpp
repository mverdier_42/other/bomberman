#ifndef PROGRAMGLSL_HPP
# define PROGRAMGLSL_HPP

# include <string>
# include "Shader.hpp"

class ProgramGlsl
{
public:
	ProgramGlsl();
	ProgramGlsl(const Shader &frag);
	ProgramGlsl(const Shader &vertex, const Shader &frag);
	ProgramGlsl(const Shader &vertex, const Shader &frag, const Shader &geom);
	ProgramGlsl(ProgramGlsl const &copy);
	~ProgramGlsl();
	ProgramGlsl	&operator=(ProgramGlsl const &rhs);
	void bind() const;
	GLuint getProg() const;
	GLint getUniformLocation(const std::string &str) const;
protected:
private:

	GLuint _prog;
};

#endif
