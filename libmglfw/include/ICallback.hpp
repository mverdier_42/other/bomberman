#ifndef ICALLBACK_HPP
# define ICALLBACK_HPP

# include <string>
# include <vector>
// # include <stdexcept>

class Core;

class ICallback
{
public:
	virtual ~ICallback() {};
	virtual void eventResize(Core &core, int width, int height) {(void)core;(void)width;(void)height;};
	virtual void eventChar(Core &core, unsigned int key) {(void)core;(void)key;};
	virtual void eventKeyPress(Core &core, int key, int mods) {(void)core;(void)key;(void)mods;};
	virtual void eventKeyRelease(Core &core, int key, int mods) {(void)core;(void)key;(void)mods;};
	virtual void eventMouseMove(Core &core, double x, double y) {(void)core;(void)x;(void)y;};
	virtual void eventButtonPress(Core &core, int key, int mods) {(void)core;(void)key;(void)mods;};
	virtual void eventButtonRelease(Core &core, int key, int mods) {(void)core;(void)key;(void)mods;};
	virtual void eventScroll(Core &core, double x, double y) {(void)core;(void)x;(void)y;};
	virtual void eventClose(Core &core) {(void)core;};
private:
};

#endif


