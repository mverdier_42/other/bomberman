#ifndef RENDERER_HPP
# define RENDERER_HPP

# include "mmatrix.hpp"
# include "TextRenderer.hpp"

class Renderer
{
public:
	Renderer();
	~Renderer();
	void resizeEvent(int width, int height);
	void renderRect(const Vec4 &rect, const Vec4 &color);
	void renderTexture(const Vec4 &rect, const Vec4 &color);
	void renderCircle(const Vec3 &circle, const Vec4 &color);
	void renderArrow(const Vec2 &pos, const Vec4 &color);
	void renderText(const std::string str, const Vec2 &v);
	Vec2 getTextSize(const std::string str) {return (_textRenderer.getTextSize(str));};
protected:
private:
	void fillBuffer(const Vec2 &v);
	void fillBuffer(const Vec3 &v);
	void fillBuffer(const Vec4 &v);

	TextRenderer _textRenderer;
	ProgramGlsl *_progArrow;
	GLint _progArrow_uni_scale;
	GLint _progArrow_uni_color;
	ProgramGlsl *_progRect;
	GLint _progRect_uni_color;
	ProgramGlsl *_progTexture;
	GLint _progTexture_uni_color;
	GLint _progTexture_uni_tex;
	ProgramGlsl *_progCircle;
	GLint _progCircle_uni_scale;
	GLint _progCircle_uni_color;
	GLuint _vao[3];
	GLuint _vbo[3];
	int _width;
	int _height;
	float _scale;
	float _scaleX;
	float _scaleY;
};

#endif
