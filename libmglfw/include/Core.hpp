#ifndef CORE_HPP
# define CORE_HPP

# include <string>
# include <vector>
# include "Window.hpp"
# include "SceneManager.hpp"
# include "Renderer.hpp"
# include "TextureManager.hpp"

class Core
{
public:
	static Core *instance;

	Core();
	~Core();
	void run();
	Renderer &getRenderer();
	Window &getWindow();
	SceneManager &getSceneManager();
	void sendEventResize(Scene &scene);
	void eventResize(int width, int height);
	void eventChar(unsigned int key);
	void eventKeyPress(int key, int mods);
	void eventKeyRelease(int key, int mods);
	void eventMouseMove(double x, double y);
	void eventButtonPress(int key, int mods);
	void eventButtonRelease(int key, int mods);
	void eventScroll(double x, double y);
	void eventClose();
	void setUserData(void *userdata);
	void *getUserData();
private:
	Window _win;
	TextureManager _texManager;
	SceneManager _manager;
	Renderer _renderer;
	void *_userdata;
};

#endif