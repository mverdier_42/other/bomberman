#ifndef WINDOW_HPP
# define WINDOW_HPP

# include <string>
# include <iostream>
# include <stdexcept>
# define GLEW_STATIC
# include <GL/glew.h>
# include <GLFW/glfw3.h>
# include <sys/time.h>
# ifndef TORADIANS
#  define TORADIANS(x)	(x * 0.0174533f)
# endif
# include "Except.hpp"
# include <string>
# include "debug.h"

class Core;

class Window
{
public:
	Window(const std::string &name = "", unsigned int width = 720, unsigned int height = 480);
	~Window();
	void loop(void);
	void update(void);
	void render(void);
	void close(void);
	bool isOpen(void);
	GLFWwindow *getGLFW(void);
	void setSize(unsigned int width, unsigned int height);
	unsigned int getWidth() const;
	unsigned int getHeight() const;
	void resetElapsed();
	void setElapsed(double e);
	double getElapsed();
	void setCallback(Core *callback);
	Core *getCallback();
	void setMousePosCallback(double x, double y);
	double getMouseX() const;
	double getMouseY() const;
	double getScale() const;
	bool isFullscreen() const;
	void setWindowOption(int width, int height, bool fullscreen);
	const GLFWvidmode *getVideoModes(int &count) const;
protected:
private:
	Window(Window const &copy);
	Window	&operator=(Window const &rhs);

	GLFWwindow *_win;
	unsigned int _width;
	unsigned int _height;
	double _scale;
	// timeval _tvStart;
	double _mouseX;
	double _mouseY;
	Core *_callback;
};

#endif
