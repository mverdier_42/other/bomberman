#ifndef SCENEMANAGER_HPP
# define SCENEMANAGER_HPP

# include <string>
# include <vector>
# include "ICallback.hpp"

// # include <stdexcept>

class Scene : public ICallback
{
public:
	// Scene();
	virtual ~Scene() {};
	virtual void update(Core &core) = 0;
	virtual void updateLoop(Core &core) {(void)core;}
	virtual void render(Core &core) = 0;
	virtual const std::string &getName() = 0;
	// void setActive(bool active);
	// bool isActive();
private:
	// bool _active;
};

class SceneManager
{
public:
	SceneManager();
	virtual ~SceneManager();
	void update(Core &core);
	void updateLoop(Core &core);
	void render(Core &core);
	void eventResize(Core &core, int width, int height);
	void setActiveScene(Scene *s);
	void setActiveScene(const std::string &name);
	Scene &getActiveScene();
	void addScene(Scene *s);
	void addSceneBack(Scene *s);
	Scene *getScene(const std::string &name);
	void removeScene(Scene *s);
protected:
private:
	std::vector<Scene*> _scenes;
	Scene *_activeScene;
};

#endif
