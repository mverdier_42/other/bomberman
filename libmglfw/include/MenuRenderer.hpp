#ifndef MENURENDERER_HPP
# define MENURENDERER_HPP

# include <iostream>
# include <vector>
# include <string>
# include "mmatrix.hpp"

class Core;
class MenuRenderer;

enum MenuModuleKey {
	MenuKeyEnter,
	MenuKeyLeft,
	MenuKeyRight,
	MenuKeyUp,
	MenuKeyDown
};

class IMenuModule
{
public:
	virtual ~IMenuModule() {};
	virtual const Vec2 &getSize() const = 0;
	virtual const Vec2 &getPos() const = 0;
	virtual void setPos(const Vec2 &pos) = 0;
	virtual void setSize(const Vec2 &size) = 0;
	virtual void resize(Core &core) = 0;
	virtual void click(const Vec2 &pos) = 0;
	virtual void keyPress(const MenuModuleKey key) = 0;
	virtual void render(Core &core, const Vec4 &color) = 0;
	virtual void setParent(MenuRenderer *menu) {_parent = menu;}
	virtual MenuRenderer *getParent() const {return (_parent);}
protected:
	MenuRenderer *_parent;
};

typedef void (*MenuCallback)(IMenuModule &mod, const MenuModuleKey key,
	void *userData);

class MenuButton : public IMenuModule
{
public:
	MenuButton();
	MenuButton(const std::string &str, MenuCallback callback = NULL,
		void *userdata = NULL);
	virtual ~MenuButton();
	virtual const Vec2 &getSize() const;
	virtual const Vec2 &getPos() const;
	virtual void setPos(const Vec2 &pos);
	virtual void setSize(const Vec2 &size);
	virtual void resize(Core &core);
	virtual void click(const Vec2 &pos);
	virtual void keyPress(const MenuModuleKey key);
	virtual void render(Core &core, const Vec4 &color);
	void setText(const std::string &txt);
	const std::string &getText() const;
private:
	void calcSize(Core &core);

	std::string _txt;
	Vec2 _size;
	Vec2 _pos;
	Vec2 _txtPos;
	MenuCallback _callback;
	void *_userData;
};

class MenuRenderer
{
public:
	MenuRenderer();
	MenuRenderer(const MenuRenderer& src);
	virtual ~MenuRenderer();
	MenuRenderer& operator=(const MenuRenderer& rhs);
	void update(Core &core);
	void keyPress(Core &core, int key);
	void keyPress(Core &core, MenuModuleKey key);
	void buttonPress(Core &core, const Vec2 &pos);
	void render(Core &core);
	void addModule(IMenuModule *module);
	void removeModule(IMenuModule *module);
	void resize(Core &core);
	IMenuModule* getActiveModule();
	std::vector<IMenuModule*> *getModules();
	void needUpdate();
protected:
private:
	void updateRender(Core &core);

	std::vector<IMenuModule*> _arr;
	bool _needUpdateRender;
	size_t _selected;
};

#endif
