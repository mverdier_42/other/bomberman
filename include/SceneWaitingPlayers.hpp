#ifndef SCENEWAITINGPLAYERS_HPP
# define SCENEWAITINGPLAYERS_HPP

# include <string>
# include <vector>
# include "SceneManager.hpp"
# include "ProgramGlsl.hpp"
# include "MenuRenderer.hpp"

class SceneWaitingPlayers : public Scene {
	public:
		SceneWaitingPlayers(Core &core);
		SceneWaitingPlayers(Core &core, int nbPlayers);
		virtual ~SceneWaitingPlayers();
		void eventResize(Core &core, int width, int height);
		void eventKeyPress(Core &core, int key, int mods);
		void eventButtonPress(Core &core, int key, int mods);
		void update(Core &core);
		void render(Core &core);
		const std::string &getName();

	private:
		SceneWaitingPlayers(const SceneWaitingPlayers &cpy);
		SceneWaitingPlayers &operator=(const SceneWaitingPlayers &rhs);

		MenuRenderer	_menu;
		int				_nbPlayers;
		int				_nbPlayersConn;
		bool			_host;
		bool			_connected;
};

#endif
