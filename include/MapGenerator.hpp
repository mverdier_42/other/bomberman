#ifndef MAPGENERATOR_HPP
#define MAPGENERATOR_HPP

#include <iostream>
#include <vector>
#include "Core.hpp"
#include "AMapObject.hpp"

class Map;
class Character;

enum	eMapName {
	RANDSOLO,
	RANDMULTI,
	RANDBOSS
};

struct	s_mapObj {
	Vec2	pos;
	OBJTYPE	type;
	int		hp;
	int		val;
};

typedef struct s_mapObj t_mapObj;
typedef std::vector<t_mapObj> t_mapDesc;

namespace MapGenerator {

	void	initMapGenerator();

	Map     *genRandomMulti(size_t nbPlayers = 1, size_t nbIAs = 1, Character *player = NULL, bool client = false);
	Map     *genRandomSolo(size_t nbPlayers = 1, size_t nbIAs = 1, Character *player = NULL, bool client = false);
	Map     *genRandomBoss(size_t nbPlayers = 1, size_t nbIAs = 1, Character *player = NULL, bool client = false);

	Map		*genMap(size_t nbPlayers, size_t nbIAs, int n, Character *player = NULL, bool client = false);

	void		printMap(Map *map);
	t_mapDesc   genMapDesc(Map *map);
	Map			*genMapFromDesc(t_mapDesc desc);

	typedef Map* (*toto)(size_t, size_t, Character*, bool);
	static std::vector<toto>	maps;

	namespace {		// Anonymous namespace, act like private.
	}

};


#endif
