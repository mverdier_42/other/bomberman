#ifndef SCENESOUNDS_HPP
# define SCENESOUNDS_HPP

# include <string>
# include <vector>
# include <fstream>
# include "SceneManager.hpp"
# include "ProgramGlsl.hpp"
# include "MenuRenderer.hpp"
#include "SceneGame.hpp"

class SceneSounds : public Scene {
	public:
		SceneSounds(Core &core);
		virtual ~SceneSounds();
		virtual void eventChar(Core &core, unsigned int key);
		void eventResize(Core &core, int width, int height);
		void eventKeyPress(Core &core, int key, int mods);
		void eventButtonPress(Core &core, int key, int mods);
		void update(Core &core);
		void render(Core &core);
		const std::string &getName();

	private:
		SceneSounds(const SceneSounds &cpy);
		SceneSounds &operator=(const SceneSounds &rhs);

		MenuRenderer	_menu;
		t_gameInputs	_gameInputs;
}; 

#endif
