#ifndef FASTMATH_HPP
#define FASTMATH_HPP

#include <iostream>
#include <string>

namespace FastMath
{

extern inline bool isDigit(const char c) {return (c >= '0' && c <= '9');}
extern inline int pr_atoi(const char *str, int *c)
{
	char	neg;
	int		out;

	out = 0;
	while (str[*c] == ' ' || str[*c] == '\t')
		(*c)++;
	if (str[*c] == '-')
	{
		neg = 1;
		(*c)++;
	}
	else if (str[*c] == '+')
	{
		neg = 0;
		(*c)++;
	}
	else
		neg = 0;
	while (isDigit(str[*c]))
	{
		out = str[*c] - '0' + out * 10;
		(*c)++;
	}
	return (neg ? -out : out);
}
extern inline int pr_atoi(const std::string &str, int *c) {return (pr_atoi(str.c_str(), c));}
extern inline int pr_atoi(const char *str, int c = 0) {return (pr_atoi(str, &c));}
extern inline int pr_atoi(const std::string &str, int c = 0) {return (pr_atoi(str.c_str(), &c));}
extern inline double pr_atod(const char *str, int *c)
{
	char	neg = 0;
	double	out = 0;
	double	f = 0.1;
	int		val;

	while (str[*c] == ' ' || str[*c] == '\t')
		(*c)++;
	if (str[*c] == '-')
	{
		neg = 1;
		(*c)++;
	}
	else if (str[*c] == '+')
		(*c)++;
	while (isDigit(str[*c]))
	{
		out = str[*c] - '0' + out * 10;
		(*c)++;
	}
	if (str[*c] == '.' || str[*c] == ',')
	{
		(*c)++;
		while (isDigit(str[*c]))
		{
			out = (str[*c] - '0') * f + out;
			f /= 10;
			(*c)++;
		}
		if (str[*c] == 'e' || str[*c] == 'E')
		{
			(*c)++;
			val = pr_atoi(str, c);
			f = 1;
			while (val < 0)
			{
				f = f / 10;
				val++;
			}
			while (val > 0)
			{
				f = f * 10;
				val--;
			}
			out = out * f;
		}
	}
	return (neg ? -out : out);
}
extern inline double pr_atod(const std::string &str, int *c) {return (pr_atod(str.c_str(), c));}
extern inline double pr_atod(const char *str, int c = 0) {return (pr_atod(str, &c));}
extern inline double pr_atod(const std::string &str, int c = 0) {return (pr_atod(str.c_str(), &c));}

};

#endif