#ifndef GAME_HPP
#define GAME_HPP

class Game {
	public:
		Game();
		virtual ~Game();

		Game& operator=(const Game& rhs);

	private:
		Game(const Game& src);

};

#endif
