#ifndef PARTICLESYSTEM_HPP
#define PARTICLESYSTEM_HPP

#include <mmatrix.hpp>
#include <Texture.hpp>
#include <ProgramGlsl.hpp>


class Particle
{
public:
	Particle() : pos(0), dir(0), lifetime(0) {}
	Particle(const Vec3 &pos, const Vec3 &dirSpeed, float lifetime) : pos(pos), dir(dirSpeed),
		lifetime(lifetime) {}
	Vec3 pos;
	Vec3 dir;
	float lifetime;
};

class ParticleSystem {
public:
	ParticleSystem();
	ParticleSystem(const Texture *tex, const ProgramGlsl *prog, unsigned int maxParticle);
	ParticleSystem(const ParticleSystem& src);
	virtual ~ParticleSystem();
	ParticleSystem& operator=(const ParticleSystem& rhs);

	void setMaxParticle(unsigned int maxParticle);
	void addParticles(const Vec3 &pos, float speed, float lifetime, unsigned int count);
	void addParticle(const Vec3 &pos, const Vec3 &dirSpeed, float lifetime);
	unsigned int getMaxParticle() const;
	const Texture *getTexture() const;
	const ProgramGlsl *getProg() const;
	void update(float delta);
	void render(const Mat4x4 &projection, const Mat4x4 &cam);

private:
	const Texture *_tex;
	const ProgramGlsl *_prog;
	unsigned int _maxParticle;
	Particle *_particles;
	unsigned int _lastAdded;
	GLuint _vao[1];
	GLuint _vbo[1];
};

#endif
