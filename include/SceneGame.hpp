/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SceneGame.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrouthie <jrouthie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/30 01:50:22 by jrouthie          #+#    #+#             */
/*   Updated: 2018/10/13 15:50:41 by jrouthie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCENEGAME_HPP
# define SCENEGAME_HPP

# include <fstream>
# include <string>
# include <vector>
# include <nlohmann/json.hpp>
# include "SceneManager.hpp"
# include "ProgramGlsl.hpp"
# include "MenuRenderer.hpp"
# include "Map.hpp"
# include "Bomb.hpp"
# include "Character.hpp"
# include "PowerUp.hpp"
# include "Models.hpp"
# include "ParticleSystem.hpp"
# include "Camera.hpp"
# include "AudioManager.hpp"
# include "Client.hpp"
# include "Server.hpp"

#define BOSSRATIO 5

typedef struct	s_gameInputs {
	std::string		moveUp;
	std::string		moveDown;
	std::string		moveLeft;
	std::string		moveRight;
	std::string		bomb;
	std::string		activate;
	int				effectVolume;
	int				musicVolume;
	int				screenWidth;
	int				screenHeight;
	bool			fullScreen;
}				t_gameInputs;

typedef struct	s_gameSave {
	long		seed;
	int			level;
	t_player	player;
	int			score;
}				t_gameSave;

class Character;

class SceneGame : public Scene
{
public:
	SceneGame(Core &core, Map *map = NULL);
	virtual ~SceneGame();

	void	eventClose(Core &core);
	void	eventResize(Core &core, int width, int height);
	void	eventKeyPress(Core &core, int key, int mods);
	void	eventKeyRelease(Core &core, int key, int mods);
	void	eventButtonPress(Core &core, int key, int mods);
	void	eventMouseMove(Core &core, double x, double y);
	void	update(Core &core);
	void	render(Core &core);
	void	renderUiStat(Core &core, const Vec2 &pos, const Vec2 &pixelSize,
						const std::string &str);
	const std::string &getName();

	void	addExplosion(const Vec2i &pos, unsigned int up, unsigned int down,
						unsigned int left, unsigned int right);
	void	setMap(Map *map);
	void	loadGameConfigs();
	void	loadGameSave();
	t_gameInputs	getGameInputs() const;
	t_gameSave		getGameSave() const;
	void	setGameInputs(t_gameInputs gameInputs);
	void	setGameSave();
	void	initGameInputs();
	void	initGameSave();
	Map		*getMap();
	void	nextLevel();
	void	renderBackground();
	void	resetGameTime();
	void    loadMap(size_t nbPlayers, size_t nbIAs, eMapName name);
	int		getLevel();
	void	saveGame();
	std::string	encryptSave(std::string src);
	std::string	decryptSave(std::string src);
	void	loadPlayer(t_player player);
	void	unloadPlayer();

	static SceneGame	*instance;
	Core				&core;
	AudioManager		audioManager;
	Client				client;
	bool				gotMapDesc;

private:
	SceneGame(const SceneGame &cpy);
	SceneGame &operator=(const SceneGame &rhs);

	Map			*_map;
	int			_level;
	bool		_resetTimer;

	ParticleSystem _particle;

	ModelsHuman	_model;
	ModelsIa	_modelIa;
	ModelsCube	_modelCube;
	ModelsFloor	_modelFloor;
	ModelsBomb	_modelBomb;
	ModelsPower	_modelPower;
	ModelsBackground _modelBackground;

	Texture	_power_speed;
	Texture	_power_bomb;
	Texture	_power_power;
	Texture _power_bombWall;
	Texture _power_remote;
	Texture _power_life;
	Texture _wall;
	Texture _grass;
	Texture _uiTime;
	Texture _trapDoor;
	Texture _trapDoorOpen;
public :
	Texture	_background;
private:

	ProgramGlsl	*_prog;
	Mat4x4		_matProjection;
	double		_oldElapsed;
	Camera		_cam;
	bool		_freeCam;
	t_player	_savPlayer;

	pthread_mutex_t	_mutex;

	t_gameInputs	_gameInputs;
	std::fstream	_fileConfig;
	nlohmann::json			_config;
	static const nlohmann::json _defaultConfig;

	t_gameSave		_gameSave;
	std::fstream	_fileSave;
	nlohmann::json 			_save;
	static const nlohmann::json _defaultSave;

	std::string		_key;
};

#endif
