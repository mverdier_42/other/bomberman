#ifndef RENDERDATA_HPP
# define RENDERDATA_HPP

# include <iostream>
# include <mmatrix.hpp>
# include "Character.hpp"

struct RenderData
{
	RenderData() {}
	RenderData(const Mat4x4 &model);
	RenderData(const Vec3 &v);
	RenderData(const Vec2 &v);
	RenderData(const AMapObject &obj);
	Mat4x4 _model;
};

struct RenderDataCharacter : RenderData
{
	RenderDataCharacter() {}
	RenderDataCharacter(const Mat4x4 &model, float speed);
	RenderDataCharacter(const Vec2 &pos, const Vec2 &dir, float speed);
	RenderDataCharacter(const Character &ch);
	float _speed;
};

#endif

