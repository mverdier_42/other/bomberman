#ifndef WALL_HPP
#define WALL_HPP

#include "AMapObject.hpp"

class PowerUp;

class Wall : public AMapObject {
	public:
		Wall(Vec2 pos = Vec2(-1, -1), int hp = -1, PowerUp *pow = NULL);
		Wall(Wall const & src);
		virtual ~Wall();

		Wall& operator=(Wall const& rhs);

		bool	exploded(Map *map, bool drill, Character *owner);
		bool	isCollide();
		void	update(Map *map);

		void 	setPow(PowerUp *pow);
		PowerUp *getPow();



		OBJTYPE			getObjType() const;
	private:
		PowerUp		*_pow;
};

#endif
