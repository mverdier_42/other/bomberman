#ifndef SCENESCREEN_HPP
# define SCENESCREEN_HPP

# include <string>
# include <vector>
# include "SceneManager.hpp"
# include "ProgramGlsl.hpp"
# include "MenuRenderer.hpp"
# include "SceneGame.hpp"

class SceneScreen : public Scene {
	public:
		SceneScreen(Core &core);
		virtual ~SceneScreen();
		void eventChar(Core &core, unsigned int key);
		void eventResize(Core &core, int width, int height);
		void eventKeyPress(Core &core, int key, int mods);
		void eventButtonPress(Core &core, int key, int mods);
		void update(Core &core);
		void render(Core &core);
		const std::string &getName();

		static t_gameInputs	gameInputs;
	private:
		SceneScreen(const SceneScreen &cpy);
		SceneScreen &operator=(const SceneScreen &rhs);

		MenuRenderer	_menu;
};

#endif