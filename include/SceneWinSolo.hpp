#ifndef SCENEWINSOLO_HPP
# define SCENEWINSOLO_HPP

# include <string>
# include <vector>
# include "SceneManager.hpp"
# include "ProgramGlsl.hpp"
# include "MenuRenderer.hpp"
# include "MapGenerator.hpp"

class SceneWinSolo : public Scene {
	public:
		SceneWinSolo(Core &core, int score);
		virtual ~SceneWinSolo();
		void eventResize(Core &core, int width, int height);
		void eventKeyPress(Core &core, int key, int mods);
		void eventButtonPress(Core &core, int key, int mods);
		void update(Core &core);
		void render(Core &core);
		const std::string &getName();
		void setMap(int mapName);

		static int		map;
	private:
		SceneWinSolo(const SceneWinSolo &cpy);
		SceneWinSolo &operator=(SceneWinSolo &rhs);

		MenuRenderer	_menu;
		int				_score;
};

#endif