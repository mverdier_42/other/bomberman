#ifndef TEXTUREMANAGER_HPP
# define TEXTUREMANAGER_HPP

# include <iostream>
# include <list>
# include "Texture.hpp"

struct TextureContainer
{
	TextureContainer();
	TextureContainer(const std::string &file);
	~TextureContainer();
	bool operator==(const std::string &rhs);
	bool operator==(const Texture &rhs);
	const std::string _file;	
	Texture _texture;
	unsigned int _ref;
};

class TextureManager
{
public:
	~TextureManager();
	static TextureManager &getManager();
	Texture &loadTexture(const std::string &file);
	void unloadTexture(const std::string &file);
	void unloadTexture(const Texture &texture);
protected:
private:
	std::list<TextureContainer*>::iterator searchTexture(const std::string &file);
	std::list<TextureContainer*>::iterator searchTexture(const Texture &texture);
	TextureManager();

	static TextureManager *_manager;
	std::list<TextureContainer*> _list;
};

#endif
