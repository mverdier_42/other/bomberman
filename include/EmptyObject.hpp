#ifndef EMPTYOBJECT_HPP
#define EMPTYOBJECT_HPP

#include "AMapObject.hpp"

class Map;

class EmptyObject : public AMapObject {
	public:
		EmptyObject();
		EmptyObject(EmptyObject const & src);
		virtual ~EmptyObject();

		EmptyObject& operator=(EmptyObject const& rhs);

		bool	exploded(Map *map, bool drill, Character *owner);
		bool	isCollide();
		void	update(Map *map);
		bool 	isEmpty() const;
		OBJTYPE			getObjType() const;

	private:

};

#endif
