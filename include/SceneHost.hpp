#ifndef SCENEHOST_HPP
# define SCENEHOST_HPP

# include <string>
# include <vector>
# include "SceneManager.hpp"
# include "ProgramGlsl.hpp"
# include "MenuRenderer.hpp"

class SceneHost : public Scene {
	public:
		SceneHost(Core &core);
		virtual ~SceneHost();
		void eventResize(Core &core, int width, int height);
		void eventKeyPress(Core &core, int key, int mods);
		void eventButtonPress(Core &core, int key, int mods);
		void update(Core &core);
		void render(Core &core);
		const std::string &getName();

		static int	nbPlayers;
		static int	nbIAs;
		static bool	validated;

	private:
		SceneHost(const SceneHost &cpy);
		SceneHost &operator=(const SceneHost &rhs);

		MenuRenderer	_menu;
};

#endif
