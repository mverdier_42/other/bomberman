#ifndef CHARACTER_HPP
#define CHARACTER_HPP

#include "AMapObject.hpp"
#include <vector>
#include <IA.hpp>

class PowerUp;

class Bomb;

struct s_player;
typedef struct s_player t_player;

class Character : public AMapObject {
	public:
		Character(Vec2 spawn = Vec2(-1, -1), std::string name = "Player",
			int speed = 5, bool client = false, int maxBombs = 1, int bombPow = 2, bool drillBomb = false,
			bool bombControl = false);
		Character(t_player player, bool client = false);
		Character(Character const & src);
		virtual ~Character();

		Character& operator=(Character const& rhs);

		bool	exploded(Map *map, bool drill, Character *owner);
		bool	isCollide();
		void	update(Map *map);
		int		getTp() const;
		void	setTp(int pos);
		void	tpMinus();
		void	putBomb();
		void	unputBomb();
		void	detonate(Map *map);
		void	lessBomb();
		void	move(const Vec2 direction);
		void	unmove();
		void	unmove(const Vec2 direction);

		void	addEffect(PowerUp *pow);
		void	removeEffect(PowerUp *pow);
		void	addPow(PowerUp *pow);
		void	removePow(int tpos);
		void	setSpeed(int speed);
		void	setMaxBombs(int maxBombs);
		void	setBombPow(int bombPow);
		void	setBombDrill(bool drillBomb);
		void	setBombControl(bool controlBomb);
		void	setName(std::string name);

		void	addBomb(Bomb *bomb);
		void	removeBomb(int tpos);
		Bomb	*getBomb(size_t tpos);
		std::vector<Bomb *>		getBombs();
		const Vec2 &getDir() const;
		float	getSpeed() const;
		int		getMaxBombs() const;
		int		getBombPow() const;
		void	setPosition(Vec2 pos, Vec2 dir = Vec2(), bool anim = false);
		bool	isDead() const;
		void	putBombAt(Vec2 pos, Map *map);
		void	setIA(eIAType val);
		float	getWalkAnim(float delta);
		bool	getAnimate() const;
		void	addScore(int toAdd);
		bool	hasDrill() const;
		bool	hasControl() const;
		std::string	getName() const;
		t_player	toStruct() const;
		bool	isClient() const;

		int			getScore() const;
		void		setScore(int val);

		OBJTYPE			getObjType() const;

	private:

	protected:

		int				_base_speed;
		int				_base_maxBombs;
		int				_base_bombPow;
		struct timeval	_ltt;
		struct timeval	_ntt;
		AMapObject		*_overBomb;
		int				_tab_pos;
		int				_speed;
		bool			_moving;
		bool			_animate;
		Vec2			_dest;
		Vec2			_dir;
		bool			_wannaPutBomb;
		std::vector<Vec2>	_mov;
		int				_bombNb;
		int				_maxBombs;
		int 			_bombPow;
		bool			_drillBomb;
		std::vector<PowerUp *> _powers;
		std::vector<Bomb *> _bombs;
		bool			_controlBomb;
		bool			_dead;
		eIAType			_IA;
		int				_score;
		double 			_time;
		std::string		_name;
		bool			_client;

		//powerup mods
		int				_mod_speed;
		int				_mod_maxBombs;
		int 			_mod_bombPow;
		float			_walkAnim;
};

#endif
