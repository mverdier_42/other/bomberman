#ifndef CAMERA_HPP
# define CAMERA_HPP

# include <mmatrix.hpp>

class Camera
{
public:
	Camera(const Vec3 &origin = 0, const Vec3 &pos = 0, const Vec3 &euler = 0);
	Camera(const Camera& src);
	const Mat4x4 &getCam();
	const Vec3 &getPos() const;
	const Vec3 &getOrigin() const;
	const Vec3 &getEuler() const;
	Vec3 &getPos();
	Vec3 &getOrigin();
	Vec3 &getEuler();
	void setCam(const Mat4x4 &cam);
	void setPos(const Vec3 &pos);
	void setOrigin(const Vec3 &origin);
	void setEuler(const Vec3 &euler);
private:
	Mat4x4 _cam;
	Vec3 _origin;
	Vec3 _pos;
	Vec3 _euler;
	bool _update;
};

#endif
