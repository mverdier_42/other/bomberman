#ifndef SCENEJOIN_HPP
# define SCENEJOIN_HPP

# include <string>
# include <vector>
# include "SceneManager.hpp"
# include "ProgramGlsl.hpp"
# include "MenuRenderer.hpp"

class SceneJoin : public Scene {
	public:
		SceneJoin(Core &core);
		virtual ~SceneJoin();
		void eventChar(Core &core, unsigned int key);
		void eventResize(Core &core, int width, int height);
		void eventKeyPress(Core &core, int key, int mods);
		void eventButtonPress(Core &core, int key, int mods);
		void update(Core &core);
		void render(Core &core);
		const std::string &getName();

		static std::string	ip;
		static std::string	port;
	private:
		SceneJoin(const SceneJoin &cpy);
		SceneJoin &operator=(const SceneJoin &rhs);

		MenuRenderer		_menu;
};

#endif
