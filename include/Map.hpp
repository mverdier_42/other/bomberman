#ifndef MAP_HPP
#define MAP_HPP

#include "mmatrix.hpp"
#include "EmptyObject.hpp"
#include "Wall.hpp"
#include "MapGenerator.hpp"
#include <Except.hpp>
#include <vector>
//#include <IA.hpp>

// _size is Vec2 object, it contains 2 floats.
#define MAP_X		_size[0]
#define MAP_Y		_size[1]

#define INT(x)		static_cast<int>(x)
#define UINT(x)		static_cast<size_t>(x)
#define FLOAT(x)	static_cast<float>(x)

class AMapObject;
class SceneGame;
class IA;
class Character;
enum eMapName;

class Map {
	public:
		Map(Vec2i size, eMapName mapName);
		Map(Map const & src);
		virtual ~Map();

		Map& operator=(Map const& rhs);

		void						update();
		void						explode(const Vec2i position, int radius, bool drill, Character *owner);
		bool						putObject(AMapObject* object);
		bool						removeObject(Vec2 pos);
		bool						removeObject(AMapObject* object);
		void						addPlayer(Character *player);
		void						addIA(IA *ia);
		void						removePlayer(int tpos);
		bool						playersAreDeads();
		Character					*getPlayer(unsigned int tpos);
		IA							*getIA(unsigned int tpos);
		std::vector<Character *>	&getPlayers();
		std::vector<IA*>			&getIAs();
		AMapObject					*getObj(Vec2 vec);
		AMapObject					*getObj(int x, int y);
		const Vec2i					&getSize() const;
		eMapName					getName() const;
		size_t						getNbPlayers() const;
		size_t						getNbIAs() const;
		bool						victory() const;
		void						checkDeads() const;

	private:
		Map();

		AMapObject**				_map;
		eMapName					_mapName;
		Vec2i						_size;
		EmptyObject					_empty;
		Wall						_full;
		std::vector<Character *>	_players;
		std::vector<IA*>			_IAs;
		bool						_mapChanged;

	protected:

};

#endif
