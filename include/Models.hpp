#ifndef MODELS_HPP
#define MODELS_HPP

#include <iostream>
#include <vector>
#include <string>
#include "Texture.hpp"
#include "mmatrix.hpp"
#include "ProgramGlsl.hpp"
#include <cmath>
#include "Core.hpp"


struct Material
{
	Material();
	~Material();
	std::string _name;
	float _Ns;			//specular exponent
	Vec3 _Ka;			//couleur ambiante
	Vec3 _Kd;			//couleur diffuse
	Vec3 _Ks;			//couleur speculaire
	Vec3 _Ke;			//couleur emissive
	Vec3 _Ni;			//densite optique
	float _d;			//transparance 1==opaque
	int _illum;			//param lumiere
	Texture *_map_Kd;	//texture diffuse default
	Texture *_map_Ks;	//texture specular
	Texture *_map_Ka;	//texture ambiante
};

struct MeshObject
{
	MeshObject() : _name(), _start(0), _size(0), _mat(NULL), _model(Mat4x4::Identity()) {}
	std::string _name;
	size_t _start;
	size_t _size; 
	Material *_mat;
	Mat4x4 _model;
};

struct Mesh
{
	Mesh(size_t vs, bool ts, bool ns);
	~Mesh();
	size_t _vc;
	Vec3 *_verV;
	Vec2 *_texV;
	Vec3 *_norV;
	std::vector<MeshObject*> _obj;
};

struct MeshMat
{
	size_t _meshId;
	size_t _matId;
	size_t _start;
	size_t _size;
};
class Models
{
public:
	Models();
	Models(const std::string &file);
	Models(const Models& src);
	virtual ~Models();
	Models& operator=(const Models& rhs);

	void addMesh(Mesh *mesh);
	void loadModel(const std::string &file);
	void setScale(unsigned int mesh, float scale);
	void setScale(unsigned int mesh, const Vec3 &scale);
	void translate(unsigned int mesh, const Vec3 &pos);
	void setMeshObjectOrigin(unsigned int mesh, const Vec3 &v);
	void updateGl();
	virtual void render(const ProgramGlsl &prog);
	Mesh &getMesh(int id);
private:
protected:
	Material *getMaterial(const std::string &name);
	void parseMtl(const std::string &file);
	void parseObj(const std::string &file);
	std::vector<MeshMat> _render;
	std::vector<Mesh*> _meshs;
	std::vector<Material*> _mat;
	std::string _name;
	GLuint _vao[1];
	GLuint _vbo[3];
};

class ModelsHuman : public Models
{
public:
	ModelsHuman();
	void anim(float e);
	Mat4x4 *_defaultModel;
};

class ModelsIa : public Models
{
public:
	ModelsIa();
	void anim(float e);
	Mat4x4 *_defaultModel;
};

class ModelsCube : public Models
{
public:
	ModelsCube() : Models("res/bomber/cube.obj") {setScale(0, Vec3(1.0f, 0.5f, 1.0f));}
};

class ModelsFloor : public Models
{
public:
	ModelsFloor() : Models("res/bomber/floor.obj") {}
};
class ModelsBackground : public Models
{
public:
	ModelsBackground() : Models("res/bomber/floor.obj") {
		setScale(0, 100.0f);
		translate(0, Vec3(0.0f, -0.05f, 0.0f));
		Mesh &msh = *_meshs[0];
		for (size_t i = 0; i < msh._vc; ++i)
		{
			msh._texV[i] *= 100.0f;
		}
		updateGl();
	}
};
class ModelsBomb : public Models
{
public:
	ModelsBomb() : Models("res/bomb/Bomb.obj") {setScale(0, 0.5f);}
};

class ModelsPower : public Models
{
public:
	ModelsPower() : Models("res/power/cube.obj") {setScale(0, 0.5f);}
};

#endif