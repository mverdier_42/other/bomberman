#ifndef SCENEKEYBOARD_HPP
# define SCENEKEYBOARD_HPP

# include <string>
# include <vector>
# include <fstream>
# include "SceneManager.hpp"
# include "ProgramGlsl.hpp"
# include "MenuRenderer.hpp"
# include "SceneGame.hpp"

class SceneKeyboard : public Scene {
	public:
		SceneKeyboard(Core &core);
		virtual ~SceneKeyboard();
		virtual void eventChar(Core &core, unsigned int key);
		void eventResize(Core &core, int width, int height);
		void eventKeyPress(Core &core, int key, int mods);
		void eventButtonPress(Core &core, int key, int mods);
		void update(Core &core);
		void render(Core &core);
		bool checkDuplicates(std::string keystr, std::string module);
		void swapKeys(std::string src, std::string dup, std::string keystr);
		void updateText(std::string module, std::string keystr);
		const std::string &getName();

	private:
		SceneKeyboard(const SceneKeyboard &cpy);
		SceneKeyboard &operator=(const SceneKeyboard &rhs);

		MenuRenderer	_menu;
		t_gameInputs	_gameInputs;
}; 

#endif
