#ifndef POWERUP_HPP
#define POWERUP_HPP

#include "AMapObject.hpp"


enum Effect { NOEFFECT, SPEED, BNB, BPOW, DBOMB, CBOMB, EXIT};

class Character;

class PowerUp : public AMapObject {
	public:
		PowerUp();
		PowerUp(const Vec2 pos = Vec2(-1, -1), Effect eff = NOEFFECT, int value = 0, int ttime = -1, Character *parent = NULL);
		PowerUp(PowerUp const & src);
		virtual ~PowerUp();

		PowerUp& operator=(PowerUp const& rhs);

		bool	exploded(Map *map, bool drill, Character *owner);
		bool	isCollide();
		void	update(Map *map);
		void 	updateTime();

		int		getTp();
		void	setTp(int pos);
		void	tpMinus();

		Effect 	getEffect();
		int		getValue();

		Character 	*getParent();
		void		setParent(Character *parent);


		OBJTYPE			getObjType() const;

	private:
		int						_time;
		struct timeval			*_ltt;
		struct timeval			*_ntt;
		Effect					_effect;
		int						_value;
		int 					_tab_pos;
		Character				*_parent;

	protected:

};

#endif
