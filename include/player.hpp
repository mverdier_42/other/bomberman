#ifndef PLAYER_HPP
# define PLAYER_HPP

typedef struct	s_player {
	bool		saved;
	int			speed;
	int			maxBombs;
	int			bombPow;
	bool		drillBomb;
	bool		bombCtrl;
}				t_player;

#endif
