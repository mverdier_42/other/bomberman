// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Server.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: fperruch <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/03/12 17:09:16 by fperruch          #+#    #+#             //
//   Updated: 2018/06/27 15:37:16 by fperruch         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef SERVER_HPP
#define SERVER_HPP

#include <bomberman.hpp>
#include <Map.hpp>
#include <MapGenerator.hpp>
#include <map>
#include <math.h>
#include "player.hpp"

enum eCmd {NOCMD, START, SETPLAYER, SETMAP, POSITION, STOP, DIE, PUTBOMB, TRIGBOMB, QUIT, LOADED, NBPLAYERS, TIMERAND, NEXTLVL, SERVRDY, SCORE};

class IA;
class Character;

struct s_seMsg {
	eCmd		cmd;
	int			playerId;
	long		val;
	size_t		nbPlayers;
	size_t		nbIAs;
	Vec2		pos;
	Vec2		dir;
};

typedef struct s_seMsg t_seMsg;

struct s_conn {
    int             		fd;
    int             		playerId;
    bool            		connected;
	std::vector<t_seMsg>	*msgs;
    pthread_t       		threadId;
    pthread_mutex_t 		*mutex;
};

typedef struct s_conn t_conn;

class Server {
public:
	Server();
	Server(Server const & src);
	virtual ~Server();

	Server& operator=(Server const& rhs);

	void	run(size_t nbPlayer, size_t nbIAs, eMapName map, t_player player, long seed);
	void	stop();
	void    setRandSeed();
	long	getRandSeed();

private:
	pthread_mutex_t			_connMutex;
	pthread_mutex_t			_mapMutex;
	pthread_mutex_t			_msgsMutex;
	Map						*_map;
	std::vector<t_seMsg>	_msgs;
	std::vector<t_conn> 	_conns;
	long					_timeRand;
	std::string             _strIp;
	std::string             _strPort;
	int						_level;
	bool					_loadMap;
	eMapName				_mapName;
	size_t					_nbPlayers;
	bool					_mapChange;
	Character				*_savPlayer;
	bool					_stop;
	bool					_savedSeed;

	void	mainLoop();
	void    waitConnexions(size_t nbPlayer, size_t nbIAs, int soc, eMapName map);
	void	sendInputs();
	void	kickClients();
	void	sendCharPos();
	int		initServ(size_t nbPlayer);
	bool	checkValidPos(const Vec2 pos);
	void	sendToAll(eCmd cmd, int playerId = 0, int val = 0, Vec2 pos = Vec2(), Vec2 dir = Vec2(), size_t nbPlayers = 0, size_t nbIAs = 0);
	void	sendToAll(const t_seMsg &msg);
	void	sendToOne(const t_conn &conn, eCmd cmd, int playerId, int val = 0, Vec2 pos = Vec2(), Vec2 dir = Vec2());
	void	sendToOne(const t_conn &conn, const t_seMsg &msg);
	void	playIAs();
	void    inputHandler(const t_seMsg &msg);
	void    deleteMap();
	void    changeMapClient(size_t nbPlayers, size_t nbIAs, int map);
	void    changeMapServ(size_t nbPlayers, size_t nbIAs, int map);
	size_t  nbPlayersConn();
	void	reset();

protected:

};

#include <IA.hpp>

#endif
