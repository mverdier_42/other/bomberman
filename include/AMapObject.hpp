#ifndef AMAPOBJECT_HPP
#define AMAPOBJECT_HPP

#include <iostream>
#include <ctime>	// clock_t.
#include "mmatrix.hpp"

// _pos is Vec2 object, it contains 2 floats.
#define POS_X	_pos[0]
#define POS_Y	_pos[1]

#define SECOND 1000000.0


enum OBJTYPE { NOTYPE, AMAPOBJECT, EMPTY, CHARACTER, WALL, BOMB, POWERUP, IAOBJ};

class Map;
class Character;

class AMapObject {
	public:
		virtual ~AMapObject() {};
		virtual bool	exploded(Map *map, bool drill, Character *owner) = 0;
		virtual bool 	isCollide() = 0;
		virtual void	update(Map *map) = 0;
		virtual bool	isEmpty() const;
		virtual int		getHp() const;
		int 			getX() const;
		int 			getY() const;
		float 			getFX() const;
		float 			getFY() const;
		const Vec2		&getPos() const;

		virtual OBJTYPE			getObjType() const;


	private:

	protected:
		Vec2		_pos;
		int			_hp;
};

#endif
