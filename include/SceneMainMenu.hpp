/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SceneMainMenu.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrouthie <jrouthie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/30 01:50:22 by jrouthie          #+#    #+#             */
/*   Updated: 2018/06/05 16:26:16 by jrouthie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCENEMAINMENU_HPP
# define SCENEMAINMENU_HPP

# include <string>
# include <vector>
# include "SceneManager.hpp"
# include "ProgramGlsl.hpp"
#include "MenuRenderer.hpp"
#include "MapGenerator.hpp"

class SceneMainMenu : public Scene
{
public:
	SceneMainMenu(Core &core);
	virtual ~SceneMainMenu();
	void eventResize(Core &core, int width, int height);
	void eventKeyPress(Core &core, int key, int mods);
	void eventButtonPress(Core &core, int key, int mods);
	void update(Core &core);
	void render(Core &core);
	const std::string &getName();

private:
	SceneMainMenu(const SceneMainMenu &cpy);
	SceneMainMenu &operator=(const SceneMainMenu &rhs);

	MenuRenderer _menu;
	bool _gameRun;
};

#endif
