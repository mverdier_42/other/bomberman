#ifndef SCENEOPTIONS_HPP
# define SCENEOPTIONS_HPP

# include <string>
# include <vector>
# include "SceneManager.hpp"
# include "ProgramGlsl.hpp"
# include "MenuRenderer.hpp"

class SceneOptions : public Scene {
	public:
		SceneOptions(Core &core);
		virtual ~SceneOptions();
		void eventResize(Core &core, int width, int height);
		void eventKeyPress(Core &core, int key, int mods);
		void eventButtonPress(Core &core, int key, int mods);
		void update(Core &core);
		void render(Core &core);
		const std::string &getName();

	private:
		SceneOptions(const SceneOptions &cpy);
		SceneOptions &operator=(const SceneOptions &rhs);

		MenuRenderer	_menu;
};

#endif
