#ifndef AUDIOMANAGER_HPP
#define AUDIOMANAGER_HPP

#include <iostream>
#include <cmath>
#include <pthread.h>
#include <unistd.h>
#include <vector>
#include <algorithm>
#include <SDL_mixer.h>
#include <cstdlib>

typedef enum	e_musics {
	MAIN,
	ONE
}				t_musics;

typedef enum	e_sounds {
	EXP,
	DEATH
}				t_sounds;

class AudioManager {
	public:
		AudioManager();
		virtual ~AudioManager();

		void		playMusic(t_musics name);
		void		playRandomMusic();
		void		playSound(t_sounds name);
		void		stopSounds();
		int			setEffectVolume(int newVolume);
		int			setMusicVolume(int newVolume);
		int			effectVolumeUp();
		int			musicVolumeUp();
		int			effectVolumeDown();
		int			musicVolumeDown();


	private:
		AudioManager(const AudioManager &cpy);
		AudioManager &operator=(const AudioManager &rhs);

		int						_effectVolume;
		int						_musicVolume;
		std::vector<Mix_Music*>	_musics;
		std::vector<Mix_Chunk*>	_sounds;
};

#endif
