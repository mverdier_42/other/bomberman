#ifndef SCENELOAD_HPP
# define SCENELOAD_HPP

# include <string>
# include <vector>
# include <fstream>
# include "SceneManager.hpp"
# include "ProgramGlsl.hpp"
# include "MenuRenderer.hpp"
#include "SceneGame.hpp"

class SceneLoad : public Scene {
	public:
		SceneLoad(Core &core);
		virtual ~SceneLoad();
		virtual void eventChar(Core &core, unsigned int key);
		void eventResize(Core &core, int width, int height);
		void eventKeyPress(Core &core, int key, int mods);
		void eventButtonPress(Core &core, int key, int mods);
		void update(Core &core);
		void render(Core &core);
		const std::string &getName();

		static t_gameSave gameSave;
	private:
		SceneLoad(const SceneLoad &cpy);
		SceneLoad &operator=(const SceneLoad &rhs);

		MenuRenderer	_menu;
}; 

#endif