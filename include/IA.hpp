// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   IA.hpp                                             :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: fperruch <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/05/08 18:15:59 by fperruch          #+#    #+#             //
//   Updated: 2018/06/06 13:15:14 by fperruch         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef IA_HPP
#define IA_HPP

#include <iostream>
#include <Map.hpp>
#include <Server.hpp>

class Bomb;

enum eIAType {NOTIA, GHOSTLIKE, PLAYERLIKE};

class Character;

struct	s_tile {
	Vec2	pos;
	float	dFrom;
	float	dTo;
	float	val;
};

typedef struct	s_tile t_tile;
typedef std::vector<t_mapObj> t_mapDesc;

class IA {
public:
	IA(Character *chara, eIAType type = GHOSTLIKE);
	IA(const IA& src);
	virtual ~IA();

	IA& operator=(const IA& rhs);

	void					play(Map *map);
	std::vector<t_seMsg>	getMsgs() const;
	void					clearMsgs();
	eIAType					getType() const;
	Character				*getChara() const;
	void					update(Map *map);
	void					pause(bool val);
	bool					isDead() const;
	Vec2					getPos() const;
	int						getHp() const;

private:
	eIAType					_type;
	Character				*_chara;
	std::vector<t_seMsg>	_msgs;
	Vec2					_dir;
	int						_nbMov;
	std::vector<Vec2>		_dirs;
	Vec2					_oldPos;
	bool					_putBomb;
	bool					_goSafe;
	Vec2					_goTo;
	bool					_closeToPl;
	Vec2					_dangerPos;
	bool					_pause;

	IA();
	void				init();
	Vec2				searchPath(Map *map, Vec2 from, Vec2 to);
	void				playerLike(Map *map);
	void				ghostLike(Map *map);
	std::vector<Vec2>	getEmpties(Map *map);
	bool				isAdj(Vec2 orig, Vec2 check);
	std::vector<t_tile>	getPath(const std::vector<t_tile> &list, Vec2 start, Vec2 end);
	void				addMsg(eCmd cmd, int val, Vec2 pos);
	Vec2				getSafePlace(Map *map, std::vector<Bomb*> &bombs, Vec2 pos);
	Vec2                getSafePlace(Map *map, Vec2 &danger, Vec2 pos);

protected:
};

#endif
