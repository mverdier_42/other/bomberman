// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Client.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: fperruch <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/03/12 17:23:24 by fperruch          #+#    #+#             //
//   Updated: 2018/06/25 17:01:49 by fperruch         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef CLIENT_HPP
#define CLIENT_HPP

#include <bomberman.hpp>
#include <Map.hpp>
#include <Server.hpp>

enum eClientStatus {IDLE, WAITING, CONNECTED, TIMEOUT};

struct s_game {
    size_t		nbPlayer;
    size_t		nbIAs;
	eMapName	map;
	long		seed;
	t_player	player;
	std::string	ip;
	Server		*serv;
};

typedef struct s_game t_game;

class Client
{
public:
	static int	port;
	static bool	error;
	static bool stopped;
	static bool mapChanged;
	static long	timeRand;

	Client();
	Client(Client const & src);
	virtual ~Client();

	Client& operator=(Client const& rhs);

	void		newGame(size_t nbPlayer, size_t nbIAs, eMapName map, t_player player, long seed = -1);
	void		newGame(size_t nbPlayer, size_t nbIAs, eMapName map);
	void	    joinGame(std::string ip, int port);
	void	    run(std::string ip);
	void	    disconnect();
	void		sendToServ(const t_seMsg &msg);
	void	    sendToServ(eCmd cmd, int val = 0, Vec2 pos = Vec2(), Vec2 dir = Vec2());
	void	    startGame();
	void		setPlayerId(int id);

	int			getPlayerId();
	void		connectClient(const std::string &ip, time_t timeoutStart);
	int			getNbPlayersConn();
	int			getNbPlayers();
	eClientStatus	getStatus();
	bool		serverIsReady() const;
	void		setServerReady();

private:
	int			_socket;
	int			_playerIndex;
	pthread_t	_threadIdCli;
	pthread_t	_threadIdServ;
	t_game		_game;
	bool		_pause;
	SceneGame	*_sG;
	size_t		_nbPlayersConn;
	size_t		_nbPlayers;
	eClientStatus	_status;
	bool		_serverReady;
	Server		*_serv;

	void		clientCommLoop();
	void		plActions(t_seMsg &msg);
	void		syncPos(const t_seMsg &msg);
	void		setPlayer(const t_seMsg &msg);
	void		setMap(const t_seMsg &msg);

protected:

};

#endif
