// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   bomberman.hpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: fperruch <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/03/12 14:14:01 by fperruch          #+#    #+#             //
//   Updated: 2018/06/25 18:48:29 by fperruch         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef BOMBERMAN_HPP
#define BOMBERMAN_HPP

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <pthread.h>
#include <vector>
#include <iostream>
#include <unistd.h>
#include <exception>
#include <string.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <stdlib.h>

#define BUFFSIZE 501
#define TIMEOUTVAL 5
#define IAINDEXDEC 10

typedef struct sockaddr_in t_sockaddr;

#endif
