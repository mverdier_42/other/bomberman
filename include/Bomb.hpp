#ifndef BOMB_HPP
#define BOMB_HPP

#include "AMapObject.hpp"
#include "Character.hpp"

class Bomb : public AMapObject {
	public:
		Bomb();
		Bomb(Character *owner = NULL, const Vec2 pos = Vec2(-1, -1), PowerUp *pow = NULL, int radius = 3, bool canTick = true, bool drillbomb = false, bool controlBomb = false);
		Bomb(Bomb const & src);
		virtual ~Bomb();

		Bomb& operator=(Bomb const& rhs);

		bool	exploded(Map *map, bool drill, Character *owner);
		bool	isCollide();
		void	update(Map *map);
		void	tick(Map *map);
		int		getTp();
		void	setTp(int pos);
		void	tpMinus();
		bool	getControl() const;
		bool	getDrill() const;
		void	setDrill(bool drill);
		void	setPlayer(Character *player);
		int		getRadius() const;
		void	setPow(PowerUp *poww);
		PowerUp *getPow(void);

		OBJTYPE			getObjType() const;

	private:
		int		_radius;
		bool	_canTick;
		Character *_owner;
		bool	_drillBomb;
		bool 	_controlBomb;
		PowerUp	*_pow;
		int		_tab_pos;
		bool	_client;
		struct timeval			*_ltt;
		struct timeval			*_ntt;

};

#endif
