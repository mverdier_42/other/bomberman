#ifndef MMATRIX_HPP
# define MMATRIX_HPP

#include <iostream>

class	Vec2;
class	Vec3;
class	Vec4;
class	Mat4x4;
class	Quat;
class	Vec2i;
class	Vec3i;

class Vec2
{
public:
	Vec2();
	Vec2(const float v);
	Vec2(const float x, const float y);
	Vec2(const Vec2 &copy);
	float &operator[](const unsigned int i);
	const float &operator[](const unsigned int i) const;
	Vec2 &operator=(const Vec2 &v);
	bool operator==(const Vec2 &v) const;
	bool operator!=(const Vec2 &v) const;
	Vec2 operator+(const Vec2 &v) const;
	Vec2 &operator+=(const Vec2 &v);
	Vec2 operator-(const Vec2 &v) const;
	Vec2 &operator-=(const Vec2 &v);
	Vec2 operator*(const Vec2 &v) const;
	Vec2 &operator*=(const Vec2 &v);
	Vec2 operator/(const Vec2 &v) const;
	Vec2 &operator/=(const Vec2 &v);
	Vec2 operator+(float v) const;
	Vec2 &operator+=(float v);
	Vec2 operator-(float v) const;
	Vec2 &operator-=(float v);
	Vec2 operator*(float v) const;
	Vec2 &operator*=(float v);
	Vec2 operator/(float v) const;
	Vec2 &operator/=(float v);
	float dot(const Vec2 &v) const;
	float length() const;
	Vec2 &normalise();
	Vec2 &negate();
	operator const float *() const;
	operator float *();

	float x;
	float y;
};

class Vec3
{
public:
	Vec3();
	Vec3(const float v);
	Vec3(const float x, const float y, const float z);
	Vec3(const Vec2 &v, const float z);
	Vec3(const Vec3 &copy);
	Vec3(const Vec3i &copy);
	float &operator[](const unsigned int i);
	const float &operator[](const unsigned int i) const;
	Vec3 &operator=(const Vec3 &v);
	bool operator==(const Vec3 &v) const;
	bool operator!=(const Vec3 &v) const;
	Vec3 operator+(const Vec3 &v) const;
	Vec3 &operator+=(const Vec3 &v);
	Vec3 operator-(const Vec3 &v) const;
	Vec3 &operator-=(const Vec3 &v);
	Vec3 operator*(const Vec3 &v) const;
	Vec3 &operator*=(const Vec3 &v);
	Vec3 operator/(const Vec3 &v) const;
	Vec3 &operator/=(const Vec3 &v);
	Vec3 operator+(float v) const;
	Vec3 &operator+=(float v);
	Vec3 operator-(float v) const;
	Vec3 &operator-=(float v);
	Vec3 operator*(float v) const;
	Vec3 &operator*=(float v);
	Vec3 operator/(float v) const;
	Vec3 &operator/=(float v);
	Vec3 cross(const Vec3 &v) const;
	float dot(const Vec3 &v) const;
	float length() const;
	Vec3 &normalise();
	Vec3 &negate();
	operator const float *() const;
	operator float *();

	float x;
	float y;
	float z;
};
class Vec4
{
public:
	Vec4();
	Vec4(const float v);
	Vec4(const float x, const float y, const float z, const float w);
	Vec4(const Vec2 &v, const float z, const float w);
	Vec4(const Vec2 &a, const Vec2 &b);
	Vec4(const Vec3 &v, const float w);
	Vec4(const Vec4 &copy);
	float &operator[](const unsigned int i);
	const float &operator[](const unsigned int i) const;
	Vec4 &operator=(const Vec4 &v);
	bool operator==(const Vec4 &v) const;
	bool operator!=(const Vec4 &v) const;
	Vec4 operator+(const Vec4 &v) const;
	Vec4 &operator+=(const Vec4 &v);
	Vec4 operator-(const Vec4 &v) const;
	Vec4 &operator-=(const Vec4 &v);
	Vec4 operator*(const Vec4 &v) const;
	Vec4 &operator*=(const Vec4 &v);
	Vec4 operator/(const Vec4 &v) const;
	Vec4 &operator/=(const Vec4 &v);
	Vec4 operator+(float v) const;
	Vec4 &operator+=(float v);
	Vec4 operator-(float v) const;
	Vec4 &operator-=(float v);
	Vec4 operator*(float v) const;
	Vec4 &operator*=(float v);
	Vec4 operator/(float v) const;
	Vec4 &operator/=(float v);
	Vec4 cross(const Vec4 &v) const;
	float dot(const Vec4 &v) const;
	float length() const;
	Vec4 &normalise();
	Vec4 &negate();
	Vec3 &xyz();
	const Vec3 &xyz() const;
	Vec2 &xy();
	const Vec2 &xy() const;
	operator const float *() const;
	operator float *();

	float x;
	float y;
	float z;
	float w;
};
class	Mat4x4
{
public:
	Mat4x4();
	Mat4x4(const float v);
	Mat4x4(const float v[4][4]);
	Mat4x4(const Quat &v);
	Mat4x4(const Mat4x4 &v);
	static Mat4x4 Identity();
	static Mat4x4 FromEuler(const Vec3 &v);
	static Mat4x4 Frustum(const float v[6]);
	static Mat4x4 LookAt(const Vec3 &eye, const Vec3 &center, const Vec3 &up);
	static Mat4x4 Ortho(const float v[6]);
	static Mat4x4 Perspective(const float y_fov, const float aspect,
		const float near, const float far);
	static Mat4x4 Translate(const Vec3 &v);
	Vec4 &operator[](const unsigned int i);
	const Vec4 &operator[](const unsigned int i) const;
	Mat4x4 &operator=(const Mat4x4 &v);
	bool operator==(const Mat4x4 &v) const;
	bool operator!=(const Mat4x4 &v) const;
	Mat4x4 operator+(const Mat4x4 &v) const;
	Mat4x4 &operator+=(const Mat4x4 &v);
	Mat4x4 operator-(const Mat4x4 &v) const;
	Mat4x4 &operator-=(const Mat4x4 &v);
	Mat4x4 operator*(const Mat4x4 &v) const;
	Mat4x4 &operator*=(const Mat4x4 &v);
	Vec4 operator*(const Vec4 &v) const;
	Mat4x4 operator*(const Quat &v) const;
	Mat4x4 &operator*=(const Quat &v);
	Vec4 row(const unsigned int i) const;
	float dot(const Mat4x4 &v) const;
	float length() const;
	Mat4x4 &scale(const float v);
	Mat4x4 &scale_aniso(const Vec3 &v);
	Mat4x4 &rotate(const Vec3 &up, const float angle);
	Mat4x4 &rotate_x(const float angle);
	Mat4x4 &rotate_y(const float angle);
	Mat4x4 &rotate_z(const float angle);
	Mat4x4 &set_translate(const Vec3 &v);
	Mat4x4 &translate_in_place(const Vec3 &v);
	Mat4x4 transpose() const;
	Vec3 getForward() const;
	Vec3 getUp() const;
	Vec3 getRight() const;
	operator const float *() const;
	operator float *();
private:
	Vec4 _val[4];
};
class	Quat
{
public:
	Quat();
	Quat(const float x, const float y, const float z, const float w);
	Quat(const Mat4x4 &v);
	static Quat Identity();
	static Quat Rotate(const Vec3 &up, const float angle);
	static Quat SetRotate(const Vec3 &angle);
	float &operator[](const int i);
	const float &operator[](const int i) const;
	Quat &operator=(const Quat &v);
	bool operator==(const Quat &v) const;
	bool operator!=(const Quat &v) const;
	Quat operator+(const Quat &v) const;
	Quat &operator+=(const Quat &v);
	Quat operator-(const Quat &v) const;
	Quat &operator-=(const Quat &v);
	Quat operator*(const Quat &v) const;
	Quat &operator*=(const Quat &v);
	Vec3 operator*(const Vec3 &v) const;
	float dot(const Quat &v) const;
	Quat &scale(const float v);
	Vec3 &xyz();
	const Vec3 &xyz() const;
	operator const float *() const;
	operator float *();
private:
	float _val[4];
};

#pragma pack(push, 1)
struct mat2
{
public:
	float x, y, z, w;
};
#pragma pack(pop)

class Vec2i
{
public:
	Vec2i();
	Vec2i(const int v);
	Vec2i(const int x, const int y);
	Vec2i(const Vec2 &v);
	Vec2i(const Vec2i &copy);
	int &operator[](const unsigned int i);
	const int &operator[](const unsigned int i) const;
	Vec2i &operator=(const Vec2i &v);
	bool operator==(const Vec2i &v) const;
	bool operator!=(const Vec2i &v) const;
	Vec2i operator+(const Vec2i &v) const;
	Vec2i &operator+=(const Vec2i &v);
	Vec2i operator-(const Vec2i &v) const;
	Vec2i &operator-=(const Vec2i &v);
	Vec2i operator*(const Vec2i &v) const;
	Vec2i &operator*=(const Vec2i &v);
	Vec2i operator/(const Vec2i &v) const;
	Vec2i &operator/=(const Vec2i &v);
	Vec2i operator+(int v) const;
	Vec2i &operator+=(int v);
	Vec2i operator-(int v) const;
	Vec2i &operator-=(int v);
	Vec2i operator*(int v) const;
	Vec2i &operator*=(int v);
	Vec2i operator/(int v) const;
	Vec2i &operator/=(int v);
	Vec2i &negate();
	operator const int *() const;
	operator int *();

	int x;
	int y;
};

class Vec3i
{
public:
	Vec3i();
	Vec3i(const int v);
	Vec3i(const int x, const int y, const int z);
	Vec3i(const Vec2i &v, const int z);
	Vec3i(const Vec3 &v);
	Vec3i(const Vec3i &copy);
	int &operator[](const unsigned int i);
	const int &operator[](const unsigned int i) const;
	Vec3i &operator=(const Vec3i &v);
	bool operator==(const Vec3i &v) const;
	bool operator!=(const Vec3i &v) const;
	Vec3i operator+(const Vec3i &v) const;
	Vec3i &operator+=(const Vec3i &v);
	Vec3i operator-(const Vec3i &v) const;
	Vec3i &operator-=(const Vec3i &v);
	Vec3i operator*(const Vec3i &v) const;
	Vec3i &operator*=(const Vec3i &v);
	Vec3i operator/(const Vec3i &v) const;
	Vec3i &operator/=(const Vec3i &v);
	Vec3i operator+(int v) const;
	Vec3i &operator+=(int v);
	Vec3i operator-(int v) const;
	Vec3i &operator-=(int v);
	Vec3i operator*(int v) const;
	Vec3i &operator*=(int v);
	Vec3i operator/(int v) const;
	Vec3i &operator/=(int v);
	Vec3i &negate();
	Vec2i &xy();
	const Vec2i &xy() const;
	operator const int *() const;
	operator int *();

	int x;
	int y;
	int z;
};

class Vec4i
{
public:
	Vec4i();
	Vec4i(const int v);
	Vec4i(const int x, const int y, const int z, const int w);
	Vec4i(const Vec2i &v, const int z, const int w);
	Vec4i(const Vec2i &a, const Vec2i &b);
	Vec4i(const Vec3i &v, const int w);
	Vec4i(const Vec4 &v);
	Vec4i(const Vec4i &copy);
	int &operator[](const unsigned int i);
	const int &operator[](const unsigned int i) const;
	Vec4i &operator=(const Vec4i &v);
	bool operator==(const Vec4i &v) const;
	bool operator!=(const Vec4i &v) const;
	Vec4i operator+(const Vec4i &v) const;
	Vec4i &operator+=(const Vec4i &v);
	Vec4i operator-(const Vec4i &v) const;
	Vec4i &operator-=(const Vec4i &v);
	Vec4i operator*(const Vec4i &v) const;
	Vec4i &operator*=(const Vec4i &v);
	Vec4i operator/(const Vec4i &v) const;
	Vec4i &operator/=(const Vec4i &v);
	Vec4i operator+(int v) const;
	Vec4i &operator+=(int v);
	Vec4i operator-(int v) const;
	Vec4i &operator-=(int v);
	Vec4i operator*(int v) const;
	Vec4i &operator*=(int v);
	Vec4i operator/(int v) const;
	Vec4i &operator/=(int v);
	Vec4i &negate();
	Vec3i &xyz();
	const Vec3i &xyz() const;
	operator const int *() const;
	operator int *();

	int x;
	int y;
	int z;
	int w;
};

std::ostream& operator<<(std::ostream& os, const Vec2& rhs);
std::ostream& operator<<(std::ostream& os, const Vec3& rhs);
std::ostream& operator<<(std::ostream& os, const Vec4& rhs);
std::ostream& operator<<(std::ostream& os, const Quat& rhs);
std::ostream& operator<<(std::ostream& os, const Mat4x4& rhs);
std::ostream& operator<<(std::ostream& os, const Vec2i& rhs);
std::ostream& operator<<(std::ostream& os, const Vec3i& rhs);
std::ostream& operator<<(std::ostream& os, const Vec4i& rhs);

#endif
