#include "mmatrix.hpp"
#include <cmath>
#include <cstring>

Vec2::Vec2() {
	x = 0;
	y = 0;
}
Vec2::Vec2(const float v) {
	x = v;
	y = v;
}
Vec2::Vec2(const float x, const float y) {
	this->x = x;
	this->y = y;
}
Vec2::Vec2(const Vec2 &v) {
	x = v[0];
	y = v[1];
}
float &Vec2::operator[](const unsigned int i) {
	return ((&x)[i]);
}
const float	&Vec2::operator[](const unsigned int i) const {
	return ((&x)[i]);
}
Vec2  &Vec2::operator=(const Vec2 &v) {
	memcpy(this, &v, sizeof(Vec2));
	return (*this);
}
bool  Vec2::operator==(const Vec2 &v) const {
	return (
		x == v[0] &&
		y == v[1]);
}
bool  Vec2::operator!=(const Vec2 &v) const {
	return (!(*this == v));
}
Vec2 Vec2::operator+(const Vec2 &v) const {
	return (Vec2(x + v[0], y + v[1]));
}
Vec2  &Vec2::operator+=(const Vec2 &v) {
	x += v[0];
	y += v[1];
	return (*this);
}
Vec2 Vec2::operator-(const Vec2 &v) const {
	return (Vec2(x - v[0], y - v[1]));
}
Vec2  &Vec2::operator-=(const Vec2 &v) {
	x -= v[0];
	y -= v[1];
	return (*this);
}
Vec2 Vec2::operator*(const Vec2 &v) const {
	return (Vec2(x * v[0], y * v[1]));
}
Vec2  &Vec2::operator*=(const Vec2 &v) {
	x *= v[0];
	y *= v[1];
	return (*this);
}
Vec2 Vec2::operator/(const Vec2 &v) const {
	return (Vec2(x / v[0], y / v[1]));
}
Vec2  &Vec2::operator/=(const Vec2 &v) {
	x /= v[0];
	y /= v[1];
	return (*this);
}
Vec2 Vec2::operator+(float v) const {
	return (Vec2(x + v, y + v));
}
Vec2  &Vec2::operator+=(float v) {
	x += v;
	y += v;
	return (*this);
}
Vec2 Vec2::operator-(float v) const {
	return (Vec2(x - v, y - v));
}
Vec2  &Vec2::operator-=(float v) {
	x -= v;
	y -= v;
	return (*this);
}
Vec2 Vec2::operator*(float v) const {
	return (Vec2(x * v, y * v));
}
Vec2  &Vec2::operator*=(float v) {
	x *= v;
	y *= v;
	return (*this);
}
Vec2 Vec2::operator/(float v) const {
	return (Vec2(x / v, y / v));
}
Vec2  &Vec2::operator/=(float v) {
	x /= v;
	y /= v;
	return (*this);
}
float Vec2::dot(const Vec2 &v) const {
	return (x * v[0] + y * v[1]);
}
float Vec2::length() const {
	float	len = x * x +
 		y * y;
	if (len == 0.0f)
 		return (0.0f);
	return (sqrtf(len));
}
Vec2  &Vec2::normalise() {
	float	len = length();
	if (len != 0.0f)
	{
		len = 1.0f / len;
		x *= len;
		y *= len;
	}
	return (*this);
}
Vec2 &Vec2::negate() {
	x = -x;
	y = -y;
	return (*this);
}
Vec2::operator const float *() const {
	return (&x);
}
Vec2::operator float *() {
	return (&x);
}
