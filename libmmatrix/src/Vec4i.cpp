#include "mmatrix.hpp"
#include <cmath>
#include <cstring>

Vec4i::Vec4i() {
	x = 0;
    y = 0;
    z = 0;
    w = 0;
}
Vec4i::Vec4i(const int v) {
	x = v;
	y = v;
	z = v;
	w = v;
}
Vec4i::Vec4i(const int x, const int y, const int z, const int w) {
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
}
Vec4i::Vec4i(const Vec2i &v, const int z, const int w)
{
	x = v[0];
	y = v[1];
	this->z = z;
	this->w = w;
}
Vec4i::Vec4i(const Vec2i &a, const Vec2i &b)
{
	x = a[0];
	y = a[1];
	z = b[0];
	w = b[1];
}
Vec4i::Vec4i(const Vec3i &v, const int w) {
	x = v[0];
	y = v[1];
	z = v[2];
	this->w = w;
}
Vec4i::Vec4i(const Vec4 &v) : Vec4i(static_cast<int>(v[0]),
	static_cast<int>(v[1]), static_cast<int>(v[2]), static_cast<int>(v[3])) {}
Vec4i::Vec4i(const Vec4i &v) {
	x = v[0];
	y = v[1];
	z = v[2];
	w = v[3];
}
int &Vec4i::operator[](const unsigned int i) {
	return ((&x)[i]);
}
const int	&Vec4i::operator[](const unsigned int i) const {
	return ((&x)[i]);
}
Vec4i &Vec4i::operator=(const Vec4i &v) {
	memcpy(this, &v, sizeof(Vec4i));
	return (*this);
}
bool Vec4i::operator==(const Vec4i &v) const {
	return (
		x == v[0] &&
		y == v[1] &&
		z == v[2] &&
		w == v[3]);
}
bool Vec4i::operator!=(const Vec4i &v) const {
	return (!(*this == v));
}
Vec4i Vec4i::operator+(const Vec4i &v) const {
	return (Vec4i(
		x + v[0],
		y + v[1],
		z + v[2],
		w + v[3]));
}
Vec4i &Vec4i::operator+=(const Vec4i &v) {
	x += v[0];
	y += v[1];
	z += v[2];
	w += v[3];
	return (*this);
}
Vec4i Vec4i::operator-(const Vec4i &v) const {
	return (Vec4i(
		x - v[0],
		y - v[1],
		z - v[2],
		w - v[3]));
}
Vec4i &Vec4i::operator-=(const Vec4i &v) {
	x -= v[0];
	y -= v[1];
	z -= v[2];
	w -= v[3];
	return (*this);
}
Vec4i Vec4i::operator*(const Vec4i &v) const {
	return (Vec4i(
		x * v[0],
		y * v[1],
		z * v[2],
		w * v[3]));
}
Vec4i &Vec4i::operator*=(const Vec4i &v) {
	x *= v[0];
	y *= v[1];
	z *= v[2];
	w *= v[3];
	return (*this);
}
Vec4i Vec4i::operator/(const Vec4i &v) const {
	return (Vec4i(
		x / v[0],
		y / v[1],
		z / v[2],
		w / v[3]));
}
Vec4i &Vec4i::operator/=(const Vec4i &v) {
	x /= v[0];
	y /= v[1];
	z /= v[2];
	w /= v[3];
	return (*this);
}
Vec4i Vec4i::operator+(int v) const {
	return (Vec4i(
		x + v,
		y + v,
		z + v,
		w + v));
}
Vec4i &Vec4i::operator+=(int v) {
	x += v;
	y += v;
	z += v;
	w += v;
	return (*this);
}
Vec4i Vec4i::operator-(int v) const {
	return (Vec4i(
		x - v,
		y - v,
		z - v,
		w - v));
}
Vec4i &Vec4i::operator-=(int v) {
	x -= v;
	y -= v;
	z -= v;
	w -= v;
	return (*this);
}
Vec4i Vec4i::operator*(int v) const {
	return (Vec4i(
		x * v,
		y * v,
		z * v,
		w * v));
}
Vec4i &Vec4i::operator*=(int v) {
	x *= v;
	y *= v;
	z *= v;
	w *= v;
	return (*this);
}
Vec4i Vec4i::operator/(int v) const {
	return (Vec4i(
		x / v,
		y / v,
		z / v,
		w / v));
}
Vec4i &Vec4i::operator/=(int v) {
	x /= v;
	y /= v;
	z /= v;
	w /= v;
	return (*this);
}
Vec4i &Vec4i::negate() {
	x = -x;
	y = -y;
	z = -z;
	w = -w;
	return (*this);
}
Vec3i &Vec4i::xyz() {
	return (*reinterpret_cast<Vec3i*>(this));
}
const Vec3i &Vec4i::xyz() const {
	return (*reinterpret_cast<const Vec3i*>(this));
}
Vec4i::operator const int *() const {
	return (&x);
}
Vec4i::operator int *() {
	return (&x);
}
