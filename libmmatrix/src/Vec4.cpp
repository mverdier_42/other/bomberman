#include "mmatrix.hpp"
#include <cmath>
#include <cstring>

Vec4::Vec4() {
	x = 0;
	y = 0;
	z = 0;
	w = 0;
}
Vec4::Vec4(const float v) {
	x = v;
	y = v;
	z = v;
	w = v;
}
Vec4::Vec4(const float x, const float y, const float z, const float w) {
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
}
Vec4::Vec4(const Vec2 &v, const float z, const float w)
{
	x = v[0];
	y = v[1];
	this->z = z;
	this->w = w;
}
Vec4::Vec4(const Vec2 &a, const Vec2 &b)
{
	x = a[0];
	y = a[1];
	z = b[0];
	w = b[1];
}
Vec4::Vec4(const Vec3 &v, const float w) {
	x = v[0];
	y = v[1];
	z = v[2];
	this->w = w;
}
Vec4::Vec4(const Vec4 &v) {
	x = v[0];
	y = v[1];
	z = v[2];
	w = v[3];
}
float &Vec4::operator[](const unsigned int i) {
	return ((&x)[i]);
}
const float	&Vec4::operator[](const unsigned int i) const {
	return ((&x)[i]);
}
Vec4 &Vec4::operator=(const Vec4 &v) {
	memcpy(this, &v, sizeof(Vec4));
	return (*this);
}
bool Vec4::operator==(const Vec4 &v) const {
	return (
		x == v[0] &&
		y == v[1] &&
		z == v[2] &&
		w == v[3]);
}
bool Vec4::operator!=(const Vec4 &v) const {
	return (!(*this == v));
}
Vec4 Vec4::operator+(const Vec4 &v) const {
	return (Vec4(
		x + v[0],
		y + v[1],
		z + v[2],
		w + v[3]));
}
Vec4 &Vec4::operator+=(const Vec4 &v) {
	x += v[0];
	y += v[1];
	z += v[2];
	w += v[3];
	return (*this);
}
Vec4 Vec4::operator-(const Vec4 &v) const {
	return (Vec4(
		x - v[0],
		y - v[1],
		z - v[2],
		w - v[3]));
}
Vec4 &Vec4::operator-=(const Vec4 &v) {
	x -= v[0];
	y -= v[1];
	z -= v[2];
	w -= v[3];
	return (*this);
}
Vec4 Vec4::operator*(const Vec4 &v) const {
	return (Vec4(
		x * v[0],
		y * v[1],
		z * v[2],
		w * v[3]));
}
Vec4 &Vec4::operator*=(const Vec4 &v) {
	x *= v[0];
	y *= v[1];
	z *= v[2];
	w *= v[3];
	return (*this);
}
Vec4 Vec4::operator/(const Vec4 &v) const {
	return (Vec4(
		x / v[0],
		y / v[1],
		z / v[2],
		w / v[3]));
}
Vec4 &Vec4::operator/=(const Vec4 &v) {
	x /= v[0];
	y /= v[1];
	z /= v[2];
	w /= v[3];
	return (*this);
}
Vec4 Vec4::operator+(float v) const {
	return (Vec4(
		x + v,
		y + v,
		z + v,
		w + v));
}
Vec4 &Vec4::operator+=(float v) {
	x += v;
	y += v;
	z += v;
	w += v;
	return (*this);
}
Vec4 Vec4::operator-(float v) const {
	return (Vec4(
		x - v,
		y - v,
		z - v,
		w - v));
}
Vec4 &Vec4::operator-=(float v) {
	x -= v;
	y -= v;
	z -= v;
	w -= v;
	return (*this);
}
Vec4 Vec4::operator*(float v) const {
	return (Vec4(
		x * v,
		y * v,
		z * v,
		w * v));
}
Vec4 &Vec4::operator*=(float v) {
	x *= v;
	y *= v;
	z *= v;
	w *= v;
	return (*this);
}
Vec4 Vec4::operator/(float v) const {
	return (Vec4(
		x / v,
		y / v,
		z / v,
		w / v));
}
Vec4 &Vec4::operator/=(float v) {
	x /= v;
	y /= v;
	z /= v;
	w /= v;
	return (*this);
}
Vec4 Vec4::cross(const Vec4 &v) const {
	return (Vec4(
		y * v[2] - z * v[1],
		z * v[0] - x * v[2],
		x * v[1] - y * v[0],
		1.0f));
}
float Vec4::dot(const Vec4 &v) const {
	return (
		x * v[0] +
		y * v[1] +
		z * v[2] +
		w * v[3]);
}
float Vec4::length() const {
	float	len =
		x * x +
		y * y +
		z * z +
		w * w;
	if (len == 0.0f)
		return (0.0f);
	return (sqrtf(len));
}
Vec4 &Vec4::normalise() {
	float	len = length();
	if (len != 0.0f)
	{
		len = 1.0f / len;
		x *= len;
		y *= len;
		z *= len;
		w *= len;
	}
	return (*this);
}
Vec4 &Vec4::negate() {
	x = -x;
	y = -y;
	z = -z;
	w = -w;
	return (*this);
}
Vec3 &Vec4::xyz() {
	return (*reinterpret_cast<Vec3*>(this));
}
const Vec3 &Vec4::xyz() const {
	return (*reinterpret_cast<const Vec3*>(this));
}
Vec2 &Vec4::xy() {
	return (*reinterpret_cast<Vec2*>(this));
}
const Vec2 &Vec4::xy() const {
	return (*reinterpret_cast<const Vec2*>(this));
}
Vec4::operator const float *() const {
	return (&x);
}
Vec4::operator float *() {
	return (&x);
}
