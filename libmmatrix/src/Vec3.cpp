#include "mmatrix.hpp"
#include <cmath>
#include <cstring>

Vec3::Vec3() {
	x = 0;
	y = 0;
	z = 0;
}
Vec3::Vec3(float v) {
	x = v;
	y = v;
	z = v;
}
Vec3::Vec3(float x, float y, float z) {
	this->x = x;
	this->y = y;
	this->z = z;
}
Vec3::Vec3(const Vec2 &v, float z) {
	x = v[0];
	y = v[1];
	this->z = z;
}
Vec3::Vec3(const Vec3 &v) {
	x = v[0];
	y = v[1];
	z = v[2];
}
Vec3::Vec3(const Vec3i &v) : Vec3(static_cast<float>(v[0]),
	static_cast<float>(v[1]), static_cast<float>(v[2])) {}
float &Vec3::operator[](const unsigned int i) {
	return ((&x)[i]);
}
const float	&Vec3::operator[](const unsigned int i) const {
	return ((&x)[i]);
}
Vec3 &Vec3::operator=(const Vec3 &v) {
	memcpy(this, &v, sizeof(Vec3));
	return (*this);
}
bool Vec3::operator==(const Vec3 &v) const {
	return (
		x == v[0] &&
		y == v[1] &&
		z == v[2]);
}
bool Vec3::operator!=(const Vec3 &v) const {
	return (!(*this == v));
}
Vec3 Vec3::operator+(const Vec3 &v) const {
	return (Vec3(
		x + v[0],
		y + v[1],
		z + v[2]));
}
Vec3 &Vec3::operator+=(const Vec3 &v) {
	x += v[0];
	y += v[1];
	z += v[2];
	return (*this);
}
Vec3 Vec3::operator-(const Vec3 &v) const {
	return (Vec3(
		x - v[0],
		y - v[1],
		z - v[2]));
}
Vec3 &Vec3::operator-=(const Vec3 &v) {
	x -= v[0];
	y -= v[1];
	z -= v[2];
	return (*this);
}
Vec3 Vec3::operator*(const Vec3 &v) const {
	return (Vec3(
		x * v[0],
		y * v[1],
		z * v[2]));
}
Vec3 &Vec3::operator*=(const Vec3 &v) {
	x *= v[0];
	y *= v[1];
	z *= v[2];
	return (*this);
}
Vec3 Vec3::operator/(const Vec3 &v) const {
	return (Vec3(
		x / v[0],
		y / v[1],
		z / v[2]));
}
Vec3 &Vec3::operator/=(const Vec3 &v) {
	x /= v[0];
	y /= v[1];
	z /= v[2];
	return (*this);
}
Vec3 Vec3::operator+(float v) const {
	return (Vec3(
		x + v,
		y + v,
		z + v));
}
Vec3 &Vec3::operator+=(float v) {
	x += v;
	y += v;
	z += v;
	return (*this);
}
Vec3 Vec3::operator-(float v) const {
	return (Vec3(
		x - v,
		y - v,
		z - v));
}
Vec3 &Vec3::operator-=(float v) {
	x -= v;
	y -= v;
	z -= v;
	return (*this);
}
Vec3 Vec3::operator*(float v) const {
	return (Vec3(
		x * v,
		y * v,
		z * v));
}
Vec3 &Vec3::operator*=(float v) {
	x *= v;
	y *= v;
	z *= v;
	return (*this);
}
Vec3 Vec3::operator/(float v) const {
	return (Vec3(
		x / v,
		y / v,
		z / v));
}
Vec3 &Vec3::operator/=(float v) {
	x /= v;
	y /= v;
	z /= v;
	return (*this);
}
Vec3 Vec3::cross(const Vec3 &v) const {
	return (Vec3(
		y * v[2] - z * v[1],
		z * v[0] - x * v[2],
		x * v[1] - y * v[0]));
}
float Vec3::dot(const Vec3 &v) const {
	return (
		x * v[0] +
		y * v[1] +
		z * v[2]);
}
float Vec3::length(void) const {
	float	len =
		x * x +
		y * y +
		z * z;
	if (len == 0.0f)
		return (0.0f);
	return (sqrtf(len));
}
Vec3 &Vec3::normalise(void) {
	float	len = length();
	if (len != 0.0f)
	{
		len = 1.0f / len;
		x *= len;
		y *= len;
		z *= len;
	}
	return (*this);
}
Vec3 &Vec3::negate() {
	x = -x;
	y = -y;
	z = -z;
	return (*this);
}
Vec3::operator const float *() const {
	return (&x);
}
Vec3::operator float *() {
	return (&x);
}
