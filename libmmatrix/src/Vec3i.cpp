#include "mmatrix.hpp"
#include <cmath>
#include <cstring>

Vec3i::Vec3i() {
	x = 0;
	y = 0;
	z = 0;
}
Vec3i::Vec3i(int v) {
	x = v;
	y = v;
	z = v;
}
Vec3i::Vec3i(int x, int y, int z) {
	this->x = x;
	this->y = y;
	this->z = z;
}
Vec3i::Vec3i(const Vec2i &v, int z) {
	x = v[0];
	y = v[1];
	this->z = z;
}
Vec3i::Vec3i(const Vec3 &v) : Vec3i(static_cast<int>(v[0]),
	static_cast<int>(v[1]), static_cast<int>(v[2])) {}
Vec3i::Vec3i(const Vec3i &v) {
	x = v[0];
	y = v[1];
	z = v[2];
}
int &Vec3i::operator[](const unsigned int i) {
	return ((&x)[i]);
}
const int	&Vec3i::operator[](const unsigned int i) const {
	return ((&x)[i]);
}
Vec3i &Vec3i::operator=(const Vec3i &v) {
	memcpy(this, &v, sizeof(Vec3i));
	return (*this);
}
bool Vec3i::operator==(const Vec3i &v) const {
	return (
		x == v[0] &&
		y == v[1] &&
		z == v[2]);
}
bool Vec3i::operator!=(const Vec3i &v) const {
	return (!(*this == v));
}
Vec3i Vec3i::operator+(const Vec3i &v) const {
	return (Vec3i(
		x + v[0],
		y + v[1],
		z + v[2]));
}
Vec3i &Vec3i::operator+=(const Vec3i &v) {
	x += v[0];
	y += v[1];
	z += v[2];
	return (*this);
}
Vec3i Vec3i::operator-(const Vec3i &v) const {
	return (Vec3i(
		x - v[0],
		y - v[1],
		z - v[2]));
}
Vec3i &Vec3i::operator-=(const Vec3i &v) {
	x -= v[0];
	y -= v[1];
	z -= v[2];
	return (*this);
}
Vec3i Vec3i::operator*(const Vec3i &v) const {
	return (Vec3i(
		x * v[0],
		y * v[1],
		z * v[2]));
}
Vec3i &Vec3i::operator*=(const Vec3i &v) {
	x *= v[0];
	y *= v[1];
	z *= v[2];
	return (*this);
}
Vec3i Vec3i::operator/(const Vec3i &v) const {
	return (Vec3i(
		x / v[0],
		y / v[1],
		z / v[2]));
}
Vec3i &Vec3i::operator/=(const Vec3i &v) {
	x /= v[0];
	y /= v[1];
	z /= v[2];
	return (*this);
}
Vec3i Vec3i::operator+(int v) const {
	return (Vec3i(
		x + v,
		y + v,
		z + v));
}
Vec3i &Vec3i::operator+=(int v) {
	x += v;
	y += v;
	z += v;
	return (*this);
}
Vec3i Vec3i::operator-(int v) const {
	return (Vec3i(
		x - v,
		y - v,
		z - v));
}
Vec3i &Vec3i::operator-=(int v) {
	x -= v;
	y -= v;
	z -= v;
	return (*this);
}
Vec3i Vec3i::operator*(int v) const {
	return (Vec3i(
		x * v,
		y * v,
		z * v));
}
Vec3i &Vec3i::operator*=(int v) {
	x *= v;
	y *= v;
	z *= v;
	return (*this);
}
Vec3i Vec3i::operator/(int v) const {
	return (Vec3i(
		x / v,
		y / v,
		z / v));
}
Vec3i &Vec3i::operator/=(int v) {
	x /= v;
	y /= v;
	z /= v;
	return (*this);
}
Vec3i &Vec3i::negate() {
	x = -x;
	y = -y;
	z = -z;
	return (*this);
}
Vec3i::operator const int *() const {
	return (&x);
}
Vec3i::operator int *() {
	return (&x);
}
