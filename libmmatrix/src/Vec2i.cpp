#include "mmatrix.hpp"
#include <cmath>
#include <cstring>

Vec2i::Vec2i() {
	x = 0;
	y = 0;
}
Vec2i::Vec2i(const int v) {
	x = v;
	y = v;
}
Vec2i::Vec2i(const int x, const int y) {
	this->x = x;
	this->y = y;
}
Vec2i::Vec2i(const Vec2 &v) : Vec2i(static_cast<int>(v[0]), static_cast<int>(v[1])) {}
Vec2i::Vec2i(const Vec2i &v) {
	x = v[0];
	y = v[1];
}
int &Vec2i::operator[](const unsigned int i) {
	return ((&x)[i]);
}
const int	&Vec2i::operator[](const unsigned int i) const {
	return ((&x)[i]);
}
Vec2i  &Vec2i::operator=(const Vec2i &v) {
	memcpy(this, &v, sizeof(Vec2i));
	return (*this);
}
bool  Vec2i::operator==(const Vec2i &v) const {
	return (
		x == v[0] &&
		y == v[1]);
}
bool  Vec2i::operator!=(const Vec2i &v) const {
	return (!(*this == v));
}
Vec2i Vec2i::operator+(const Vec2i &v) const {
	return (Vec2i(x + v[0], y + v[1]));
}
Vec2i  &Vec2i::operator+=(const Vec2i &v) {
	x += v[0];
	y += v[1];
	return (*this);
}
Vec2i Vec2i::operator-(const Vec2i &v) const {
	return (Vec2i(x - v[0], y - v[1]));
}
Vec2i  &Vec2i::operator-=(const Vec2i &v) {
	x -= v[0];
	y -= v[1];
	return (*this);
}
Vec2i Vec2i::operator*(const Vec2i &v) const {
	return (Vec2i(x * v[0], y * v[1]));
}
Vec2i  &Vec2i::operator*=(const Vec2i &v) {
	x *= v[0];
	y *= v[1];
	return (*this);
}
Vec2i Vec2i::operator/(const Vec2i &v) const {
	return (Vec2i(x / v[0], y / v[1]));
}
Vec2i  &Vec2i::operator/=(const Vec2i &v) {
	x /= v[0];
	y /= v[1];
	return (*this);
}
Vec2i Vec2i::operator+(int v) const {
	return (Vec2i(x + v, y + v));
}
Vec2i  &Vec2i::operator+=(int v) {
	x += v;
	y += v;
	return (*this);
}
Vec2i Vec2i::operator-(int v) const {
	return (Vec2i(x - v, y - v));
}
Vec2i  &Vec2i::operator-=(int v) {
	x -= v;
	y -= v;
	return (*this);
}
Vec2i Vec2i::operator*(int v) const {
	return (Vec2i(x * v, y * v));
}
Vec2i  &Vec2i::operator*=(int v) {
	x *= v;
	y *= v;
	return (*this);
}
Vec2i Vec2i::operator/(int v) const {
	return (Vec2i(x / v, y / v));
}
Vec2i  &Vec2i::operator/=(int v) {
	x /= v;
	y /= v;
	return (*this);
}
Vec2i &Vec2i::negate() {
	x = -x;
	y = -y;
	return (*this);
}
Vec2i::operator const int *() const {
	return (&x);
}
Vec2i::operator int *() {
	return (&x);
}
