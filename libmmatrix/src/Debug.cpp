/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Debug.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrouthie <jrouthie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/21 00:06:33 by jrouthie          #+#    #+#             */
/*   Updated: 2018/05/28 13:56:59 by jrouthie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mmatrix.hpp"


std::ostream& operator<<(std::ostream& os, const Vec2& rhs) {
	os << rhs[0] << ":" << rhs[1];
	return (os);
}
std::ostream& operator<<(std::ostream& os, const Vec3& rhs) {
	os << rhs[0] << ":" << rhs[1] << ":" << rhs[2];
	return (os);
}
std::ostream& operator<<(std::ostream& os, const Vec4& rhs) {
	os << rhs[0] << ":" << rhs[1] << ":" << rhs[2] << ":" << rhs[3];
	return (os);
}
std::ostream& operator<<(std::ostream& os, const Quat& rhs) {
	os << rhs[0] << ":" << rhs[1] << ":" << rhs[2] << ":" << rhs[3];
	return (os);
}
std::ostream& operator<<(std::ostream& os, const Mat4x4& rhs) {
    for (int x = 0; x < 4; ++x)
    {
        os << rhs[x] << std::endl;
    }
	return (os);
}
std::ostream& operator<<(std::ostream& os, const Vec2i& rhs) {
	os << rhs[0] << ":" << rhs[1];
	return (os);
}
std::ostream& operator<<(std::ostream& os, const Vec3i& rhs) {
	os << rhs[0] << ":" << rhs[1] << ":" << rhs[2];
	return (os);
}
std::ostream& operator<<(std::ostream& os, const Vec4i& rhs) {
	os << rhs[0] << ":" << rhs[1] << ":" << rhs[2] << ":" << rhs[3];
	return (os);
}