// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Server.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: fperruch <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/03/12 17:41:48 by fperruch          #+#    #+#             //
//   Updated: 2018/10/08 14:56:22 by fperruch         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <Server.hpp>
#include <iostream>
#include <Client.hpp>
#include "SceneGame.hpp"
#include "MapGenerator.hpp"
#include <fcntl.h>
#include <Character.hpp>

Server::Server() {
	_map = NULL;
	_mapChange = false;
	pthread_mutex_init(&_mapMutex, NULL);
	pthread_mutex_init(&_connMutex, NULL);
	pthread_mutex_init(&_msgsMutex, NULL);
	_strPort.clear();
	_strIp.clear();
	_msgs.clear();
	_conns.clear();
	_level = 1;
	_stop = false;
	_savPlayer = NULL;
	_savedSeed = false;
}

Server::Server(Server const& src) {
	*this = src;
}

Server::~Server() {
	kickClients();
	deleteMap();
	if (_savPlayer) {
		delete (_savPlayer);
		_savPlayer = NULL;
	}
	pthread_mutex_destroy(&_connMutex);
	pthread_mutex_destroy(&_mapMutex);
	pthread_mutex_destroy(&_msgsMutex);
}

void 	Server::kickClients() {
	t_seMsg	msg;

	msg.cmd = STOP;
	pthread_mutex_lock(&_connMutex);
	for (size_t i = 0; i < _conns.size(); i++) {
		if (_conns[i].connected && _conns[i].fd > 0)
			write(_conns[i].fd, &msg, sizeof(t_seMsg));
		_conns[i].msgs = NULL;
	}
	_conns.clear();
	pthread_mutex_unlock(&_connMutex);
	pthread_mutex_lock(&_msgsMutex);
	_msgs.clear();
	pthread_mutex_unlock(&_msgsMutex);
}

void	Server::deleteMap() {
	if (_map) {
		delete _map;
		_map = NULL;
	}
}

Server& Server::operator=(Server const& rhs) {
	_connMutex = rhs._connMutex;
	_map = rhs._map;
	return (*this);
}

void    Server::sendToAll(eCmd cmd, int playerId, int val, Vec2 pos, Vec2 dir, size_t nbPlayers, size_t nbIAs) {
	t_seMsg	msg;

	msg.cmd = cmd;
	msg.val = val;
	msg.pos = pos;
	msg.playerId = playerId;
	msg.nbPlayers = nbPlayers;
	msg.nbIAs = nbIAs;
	msg.dir = dir;
	sendToAll(msg);
}

void	Server::sendToAll(const t_seMsg &msg) {
	pthread_mutex_lock(&_connMutex);
	for (size_t i = 0; i < _conns.size(); i++) {
		if (_conns[i].fd > 0 && _conns[i].connected)
			write(_conns[i].fd, &msg, sizeof(t_seMsg));
	}
	pthread_mutex_unlock(&_connMutex);
}

void	Server::sendToOne(const t_conn &conn, eCmd cmd, int playerId, int val, Vec2 pos, Vec2 dir) {
	t_seMsg	msg;

	msg.cmd = cmd;
	msg.val = val;
	msg.pos = pos;
	msg.dir = dir;
	msg.playerId = playerId;
	sendToOne(conn, msg);
}

void    Server::sendToOne(const t_conn &conn, const t_seMsg &msg) {
	if (conn.fd > 0)
		write(conn.fd, &msg, sizeof(t_seMsg));
}

void    *serverComLoop(void *connPtr) {
	t_conn      *conn;
	t_seMsg		msg;

	if (!connPtr) {
		std::cerr << "No data in server recv loop thread" << std::endl;
		pthread_exit(NULL);
	}
	conn = (t_conn *)connPtr;
	bzero(&msg, sizeof(t_seMsg));
	if (!conn->mutex) {
		std::cerr << "Mutex null" << std::endl;
		pthread_exit(NULL);
	}
	while (read(conn->fd, &msg, sizeof(t_seMsg)) >= 0) {
		if (msg.cmd == QUIT)
			break ;
		if (conn->msgs) {
			pthread_mutex_lock(conn->mutex);
			conn->msgs->push_back(msg);
			pthread_mutex_unlock(conn->mutex);
		}
		bzero(&msg, sizeof(t_seMsg));
	}
	conn->connected = false;
	close(conn->fd);
	bzero(&msg, sizeof(t_seMsg));
	pthread_detach(pthread_self());
	pthread_exit(NULL);
}

void    Server::changeMapClient(size_t nbPlayers, size_t nbIAs, int map) {
	std::vector<Character*>	players;
    _mapChange = true;
	if (_map) {
		players = _map->getPlayers();
		for (size_t i = 0; i < players.size(); i++)
			if (players[i]) {
				sendToAll(SCORE, i, players[i]->getScore());
			}
	}
	sendToAll(SETMAP, 0, (int)map, NULL, 0, nbPlayers, nbIAs);
}

void    Server::changeMapServ(size_t nbPlayers, size_t nbIAs, int map) {
	Character   *player = NULL;

	if (!_mapChange)
		return ;
	pthread_mutex_lock(&_mapMutex);
	if (((_map && map != RANDMULTI) || !_map) && _savPlayer)
		player = new Character(*(_savPlayer));
	deleteMap();
	_map = MapGenerator::genMap(nbPlayers, nbIAs, map, player);
	pthread_mutex_unlock(&_mapMutex);
}

void	Server::inputHandler(const t_seMsg &msg) {
	Character	*player = NULL;

	if (_map && (player = _map->getPlayer(msg.playerId)) && (size_t)msg.playerId < _map->getPlayers().size() && !_mapChange) {
		if (msg.cmd == POSITION)
			player->setPosition(msg.pos, msg.dir, msg.val);
		if (msg.cmd == PUTBOMB && _map) {
			player->putBombAt(msg.pos, _map);
			sendToAll(msg);
		}
		if (msg.cmd == TRIGBOMB) {
			player->detonate(_map);
			sendToAll(msg);
		}
		if (msg.cmd == SCORE)
			player->setScore(msg.val);
	}
	if (msg.cmd == NEXTLVL) {
		if (_map && _nbPlayers == 1 && _map->getPlayer(0) && _level % BOSSRATIO) {
			if (_savPlayer) {
				delete (_savPlayer);
				_savPlayer = NULL;
			}
			_savPlayer = new Character(*(_map->getPlayer(0)));
		}
		_level = msg.val;
		if (!_savedSeed)
			setRandSeed();
		if ((_nbPlayers == 1 && (_level % BOSSRATIO || !_level)) || _nbPlayers > 1)
			changeMapClient(_nbPlayers, _level, _mapName);
		else
			changeMapClient(_nbPlayers, _level / BOSSRATIO, RANDBOSS);
	}
	if (msg.cmd == LOADED) {
		if ((_nbPlayers == 1 && (_level % BOSSRATIO || !_level)) || _nbPlayers > 1)
			changeMapServ(_nbPlayers, _level, _mapName);
		else if (_nbPlayers == 1)
			changeMapServ(_nbPlayers, _level / BOSSRATIO, RANDBOSS);
		_mapChange = false;
	}
	if (msg.cmd == TIMERAND)
		_timeRand = msg.val;
}

void	Server::sendInputs() {
	bool		dataSend;
	std::string	str;

	dataSend = false;
	pthread_mutex_lock(&_msgsMutex);
	if (_msgs.size() > 0) {
		for (size_t i = 0; i < _msgs.size(); i++) {
			if (_msgs[i].cmd != NOCMD)
				inputHandler(_msgs[i]);
			dataSend = true;
		}
		if (dataSend)
			_msgs.clear();
	}
	pthread_mutex_unlock(&_msgsMutex);
}

bool	isNear(Vec2 pos, Vec2 pos2) {
	float	delta = 0.5f;
	return (pos[0] > pos2[0] - delta && pos[0] < pos2[0] + delta && pos[1] > pos2[1] - delta && pos[1] < pos2[1] + delta);
}

void    Server::sendCharPos() {
	std::vector<Character*> chars;
	std::vector<IA*>		IAs;
	std::string				str;
	Character				*IAChar = NULL;
	std::vector<t_seMsg>	iaMsgs;

	pthread_mutex_lock(&_mapMutex);
	if (!_map || _mapChange) {
		pthread_mutex_unlock(&_mapMutex);
		return ;
	}
	chars = _map->getPlayers();
	for (size_t i = 0; i < chars.size(); i++) {
		if (!chars[i]->isDead())
			sendToAll(POSITION, i, chars[i]->getAnimate(), chars[i]->getPos(), chars[i]->getDir());
		else
			sendToAll(DIE, i);
	}
	IAs = _map->getIAs();
	for (size_t i = 0; i < IAs.size(); i++) {
		if (IAs[i]->isDead())
			sendToAll(DIE, i + IAINDEXDEC);
		else if ((IAChar = IAs[i]->getChara())) {
			sendToAll(POSITION, i + IAINDEXDEC, IAChar->getAnimate(), IAChar->getPos(), IAChar->getDir());
			for (size_t j = 0; j < chars.size(); j++) {
				if (IAs[i]->getType() == GHOSTLIKE && isNear(chars[j]->getPos(), IAChar->getPos()) && !chars[j]->isDead()) {
					chars[j]->exploded(_map, 0, NULL);
					sendToAll(DIE, j);
				}
			}
			iaMsgs = IAs[i]->getMsgs();
			for (size_t j = 0; j < iaMsgs.size(); j++) {
				iaMsgs[j].playerId = i + IAINDEXDEC;
				sendToAll(iaMsgs[j]);
				IAs[i]->clearMsgs();
			}
		}
	}
	pthread_mutex_unlock(&_mapMutex);
}

void	Server::playIAs() {
	std::vector<IA*>	IAs;

	pthread_mutex_lock(&_mapMutex);
	if (!_map) {
		pthread_mutex_unlock(&_mapMutex);
		return ;
	}
	IAs = _map->getIAs();
	for (size_t i = 0; i < IAs.size(); i++)
		IAs[i]->play(_map);
	pthread_mutex_unlock(&_mapMutex);
}

void    Server::mainLoop() {
	while (nbPlayersConn() > 0) {
		sendInputs();
		playIAs();
		sendCharPos();
		pthread_mutex_lock(&_mapMutex);
		if (_map)
			_map->update();
		pthread_mutex_unlock(&_mapMutex);
		if (SceneGame::instance->client.stopped)
			break ;
		if (_map && _map->playersAreDeads() && !_mapChange) {
			sleep(1);
			changeMapClient(_map->getNbPlayers(), _map->getNbIAs(), _map->getName());
		}
	}
}

void    Server::waitConnexions(size_t nbPlayer, size_t nbIAs, int soc, eMapName map)
{
	t_conn      conn;
	t_sockaddr  addr;
	socklen_t   structSize;
	t_seMsg     msg;
	bool        replaced;
	size_t      nb;
	int			flags;
	int			flsave;

	flags = fcntl(soc, F_GETFL);
	flsave = flags;
	flags = flags | O_NONBLOCK;
	fcntl(soc, F_SETFL, flags);
	if (!_savedSeed)
		_timeRand = time(NULL);
	else
		_savedSeed = false;
	while ((nb = nbPlayersConn()) < nbPlayer) {
		conn.fd = -1;
		replaced = false;
		if (_stop)
			return ;
		if ((conn.fd = accept(soc, (struct sockaddr*)&addr, &structSize)) < 0)
		{
			if (errno == EWOULDBLOCK)
				continue ;
			std::cerr << "server: fail to accept > " << strerror(errno) << std::endl;
			throw (std::exception());
		}
		fcntl(conn.fd, F_SETFL, flsave);
		conn.connected = true;
		conn.msgs = &_msgs;
		conn.playerId = -1;
		pthread_mutex_lock(&_connMutex);
		for (size_t i = 0; i < _conns.size(); i++)
			if (!_conns[i].connected) {
				_conns[i] = conn;
				replaced = true;
				break ;
			}
		if (!replaced) {
			_conns.push_back(conn);
			nb++;
		}
		pthread_mutex_unlock(&_connMutex);
		sendToAll(NBPLAYERS, 0, nb, 0, 0, nbPlayer);
	}
    fcntl(soc, F_SETFL, flsave);
	sendToAll(TIMERAND, 0, _timeRand);
	pthread_mutex_lock(&_connMutex);
	for (size_t i = 0; i < _conns.size(); i++)
	{
		_conns[i].playerId = i;
		_conns[i].mutex = &_msgsMutex;
		pthread_create(&_conns[i].threadId, NULL, &serverComLoop, &_conns[i]);
		sendToOne(_conns[i], SETPLAYER, _conns[i].playerId);
	}
	pthread_mutex_unlock(&_connMutex);
	changeMapClient(nbPlayer, nbIAs, map);
	sendToAll(START);
}

int     Server::initServ(size_t nbPlayer) {
	int         soc;
	int         port;
	t_sockaddr  addr;
	time_t      timeoutStart;

	if ((soc = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		std::cerr << "server: fail to get socket > " << strerror(errno) << std::endl;
		throw (std::exception());
	}
	port = 42420;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(port);
	addr.sin_family = AF_INET;
	timeoutStart = time(NULL);
	while (bind(soc, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
		port++;
		addr.sin_port = htons(port);
		if (time(NULL) - timeoutStart > TIMEOUTVAL) {
			close(soc);
			std::cerr << " server connection timeout : " << strerror(errno) << std::endl;
			throw (std::exception());
		}
	}
	if (listen(soc, nbPlayer) < 0) {
		std::cerr << "server: fail to listen > " << strerror(errno) << std::endl;
		throw (std::exception());
	}
	_strPort = std::to_string(port);
	SceneGame::instance->client.port = port;
	SceneGame::instance->client.setServerReady();
	return (soc);
}

void	Server::run(size_t nbPlayer, size_t nbIAs, eMapName map, t_player player, long seed) {
	int	soc;

	_stop = false;
	if (_savPlayer) {
		delete (_savPlayer);
		_savPlayer = NULL;
	}

	if (player.saved)
		_savPlayer = new Character(player);

	_level = nbIAs;
	_nbPlayers = nbPlayer;
	_mapName = map;
	if (seed > -1) {
		_timeRand = seed;
		_savedSeed = true;
	}
	try {
		soc = initServ(nbPlayer);
		waitConnexions(nbPlayer, nbIAs, soc, map);
	}
	catch (std::exception e) {
		std::cerr << e.what() << std::endl;
		return ;
	}
	if (!_stop)
		mainLoop();
	reset();
	close(soc);
}

size_t	Server::nbPlayersConn() {
	size_t	nb = 0;

	pthread_mutex_lock(&_connMutex);
	for (size_t i = 0; i < _conns.size(); i++)
		if (_conns[i].connected)
			nb++;
	pthread_mutex_unlock(&_connMutex);
	return (nb);
}

void	Server::reset() {
	_strIp.clear();
	_strPort.clear();
	kickClients();
	pthread_mutex_lock(&_msgsMutex);
	_msgs.clear();
	pthread_mutex_unlock(&_msgsMutex);
	pthread_mutex_lock(&_connMutex);
	_conns.clear();
	pthread_mutex_unlock(&_connMutex);
	pthread_mutex_lock(&_mapMutex);
	deleteMap();
	pthread_mutex_unlock(&_mapMutex);
}

void	Server::setRandSeed() {
	_timeRand = time(NULL);
	sendToAll(TIMERAND, 0, _timeRand);
}

long	Server::getRandSeed() {
	return (_timeRand);
}

void	Server::stop() {
	_stop = true;
}
