#include <iostream>
#include "Models.hpp"
#include "Except.hpp"
#include "FastMath.hpp"
#include "TextureManager.hpp"
#include "Except.hpp"
#include "debug.h"

#include <fstream>

Material::Material() :
	_name(), _Ns(0), _Ka(0), _Kd(0), _Ks(0), _Ke(0), _Ni(0), _d(0), _illum(0),
	_map_Kd(NULL), _map_Ks(NULL), _map_Ka(NULL)
{
}

Material::~Material()
{
	if (_map_Kd)
		TextureManager::getManager().unloadTexture(*_map_Kd);
	if (_map_Ks)
		TextureManager::getManager().unloadTexture(*_map_Ks);
	if (_map_Ka)
		TextureManager::getManager().unloadTexture(*_map_Ka);
}


static inline int face_count(const std::string &buf)
{
	char sp = 1;
	int wc = 0;
	size_t c = 2;

	while (c < buf.size())
	{
		if (buf[c] == '\t' || buf[c] == ' ' || buf[c] == '\n' || buf[c] == '\r')
			sp = 1;
		else
		{
			if (sp)
				wc++;
			sp = 0;
		}
		c++;
	}
	if (wc < 3)
		throw Except("invalide face");
	return (wc - 2);
}

static inline Vec3 pr_atod3(const char *line)
{
	int c = 0;

	return (Vec3(
		FastMath::pr_atod(line, &c),
		FastMath::pr_atod(line, &c),
		FastMath::pr_atod(line, &c)));
}

Material *Models::getMaterial(const std::string &name)
{
	for (size_t i = 0; i < _mat.size(); ++i)
	{
		if (name == _mat[i]->_name)
			return (_mat[i]);
	}
	return (NULL);
}

void Models::parseMtl(const std::string &file)
{
	std::string path;
	std::string line;
	size_t p;
	size_t line_c = 0;

	p = file.find_last_of("/");
	path = (p == std::string::npos) ? "./" : file.substr(0, p + 1);
	DEBUG(std::cout << "parseMtl(" << file << ")\n";)

	std::ifstream fd; 
	fd.open(file); 
	if (!fd)
		throw Except("invalide .mtl file");
	Material *mat = NULL;
	while (std::getline(fd, line))
	{
		const char *ll = line.c_str();
		++line_c;
		if (ll[0] == '#' || line.size() < 4)
			;
		else if (!line.compare(0, 6, "newmtl"))
		{
			if (mat)
				_mat.push_back(mat);
			mat = new Material();
			mat->_name = line.substr(7);
		}
		else if (ll[0] == 'N')
		{
			if (ll[1] == 's')
			{
				if (!mat)
					throw Except("newmtl missing");
				mat->_Ns = FastMath::pr_atod(ll + 3);
			}
			else if (ll[1] == 'i')
			{
				if (!mat)
					throw Except("newmtl missing");
				mat->_Ni = pr_atod3(ll + 3);
			}
		}
		else if (ll[0] == 'd')
		{
			if (!mat)
				throw Except("newmtl missing");
			mat->_d = FastMath::pr_atod(ll + 2);
		}
		else if (ll[0] == 'K')
		{
			if (ll[1] == 'a')
			{
				if (!mat)
					throw Except("newmtl missing");
				mat->_Ka = pr_atod3(ll + 3);
			}
			if (ll[1] == 'd')
			{
				if (!mat)
					throw Except("newmtl missing");
				mat->_Kd = pr_atod3(ll + 3);
			}
			if (ll[1] == 's')
			{
				if (!mat)
					throw Except("newmtl missing");
				mat->_Ks = pr_atod3(ll + 3);
			}
			if (ll[1] == 'e')
			{
				if (!mat)
					throw Except("newmtl missing");
				mat->_Ke = pr_atod3(ll + 3);
			}
		}
		else if(!line.compare(0, 5, "illum"))
		{
			if (!mat)
				throw Except("newmtl missing");
			mat->_illum = FastMath::pr_atoi(ll + 5);
		}
		else if(!line.compare(0, 6, "map_Kd"))
		{
			if (!mat)
				throw Except("newmtl missing");
			mat->_map_Kd = &TextureManager::getManager().loadTexture(path + line.substr(7));
		}
		else if(!line.compare(0, 6, "map_Ks"))
		{
			if (!mat)
				throw Except("newmtl missing");
			mat->_map_Ks = &TextureManager::getManager().loadTexture(path + line.substr(7));
		}
		else if(!line.compare(0, 6, "map_Ka"))
		{
			if (!mat)
				throw Except("newmtl missing");
			mat->_map_Ka = &TextureManager::getManager().loadTexture(path + line.substr(7));
		}
		else
			std::cout << "unknow("<<line_c<<"):(" << line << ")\n";
	}
	if (mat)
		_mat.push_back(mat);
}

static inline int readPidOne(const char *ll, int *i, unsigned int *d)
{
	if (ll[*i] == '/')
	{
		(*i)++;
		return (1);
	}
	else if (!FastMath::isDigit(ll[*i]))
		return (0);
	*d = FastMath::pr_atoi(ll, i) - 1;
	if (ll[*i] == '/')
	{
		(*i)++;
		return (1);
	}
	return (0);
}


static inline bool readPid(const char *ll, int *i, unsigned int pid[3])
{
	while (ll[*i] == ' ')
		(*i)++;
	if (readPidOne(ll, i, pid + 0))
	{
		if (readPidOne(ll, i, pid + 1))
			readPidOne(ll, i, pid + 2);
		return (true);
	}
	return (false);
}

void Models::parseObj(const std::string &file)
{
	MeshObject *curObj = NULL;
	std::string path;
	std::string line;
	size_t p;
	size_t line_c = 0;
	size_t ver_c = 0;
	size_t tex_c = 0;
	size_t nor_c = 0;
	size_t f_c = 0;
	p = file.find_last_of("/");
	path = (p == std::string::npos) ? "./" : file.substr(0, p + 1);
	DEBUG(std::cout << "parseObj(" << file << ")\n";)

	std::ifstream fd; 
	fd.open(file);
	if (!fd)
		throw Except("invalide .obj file");
	while (std::getline(fd, line))
	{
		++line_c;
		if (line.size() < 4 || line[0] == '#')
			;
		else if (line[0] == 'v')
		{
			if (line[1] == 'n')
				nor_c++;
			else if (line[1] == 't')
				tex_c++;
			else
				ver_c++;
		}
		else if (line[0] == 'f')
			f_c += face_count(line) * 3;
		else if (!line.compare(0, 6, "mtllib"))
			parseMtl(path + line.substr(7));
		else if (line[0] == 'o')
			_name = line.substr(2);
		// else
		// 	std::cout << "unknow("<<line_c<<"):(" << line << ")\n";
	}
	Mesh *msh = new Mesh(f_c, tex_c, nor_c);
	curObj = new MeshObject();
	size_t mc = 0;
	// fd.seekg(0, std::ios::beg);
	fd.close();
	fd.open(file);
	line_c = 0;
	{
		Vec3 *ver_b = ver_c ? new Vec3[ver_c] : NULL;
		Vec2 *tex_b = tex_c ? new Vec2[tex_c] : NULL;
		Vec3 *nor_b = nor_c ? new Vec3[nor_c] : NULL;
		size_t ver_i = 0;
		size_t tex_i = 0;
		size_t nor_i = 0;

		while (std::getline(fd, line))
		{
			const char *ll = line.c_str();
			if (ll[0] == 'v')
			{
				int i;

				if (ll[1] == 'n')
				{
					i = 2;
					nor_b[nor_i] = Vec3(FastMath::pr_atod(ll, &i), FastMath::pr_atod(ll, &i), FastMath::pr_atod(ll, &i));
					nor_i++;
				}
				else if (ll[1] == 't')
				{
					i = 2;
					tex_b[tex_i] = Vec2(FastMath::pr_atod(ll, &i), FastMath::pr_atod(ll, &i));
					tex_i++;
				}
				else
				{
					i = 1;
					ver_b[ver_i] = Vec3(FastMath::pr_atod(ll, &i), FastMath::pr_atod(ll, &i), FastMath::pr_atod(ll, &i));
					ver_i++;
				}
			}
			else if (ll[0] == 'f')
			{
				int i = 2;
				unsigned int pid[4][3];
				if (readPid(ll, &i, pid[0]))//TODO a verifier la cause
				if (readPid(ll, &i, pid[1]))
				if (readPid(ll, &i, pid[2]))
				{
					msh->_verV[mc] = ver_b[pid[0][0]];
					msh->_verV[mc + 1] = ver_b[pid[2][0]];
					msh->_verV[mc + 2] = ver_b[pid[1][0]];
					if (tex_i)
					{
						msh->_texV[mc] = tex_b[pid[0][1]];
						msh->_texV[mc + 1] = tex_b[pid[2][1]];
						msh->_texV[mc + 2] = tex_b[pid[1][1]];
					}
					if (nor_i)
					{
						msh->_norV[mc] = nor_b[pid[0][2]];
						msh->_norV[mc + 1] = nor_b[pid[2][2]];
						msh->_norV[mc + 2] = nor_b[pid[1][2]];
					}
					mc += 3;					
					if (readPid(ll, &i, pid[3]))
					{
						msh->_verV[mc] = ver_b[pid[0][0]];
						msh->_verV[mc + 1] = ver_b[pid[3][0]];
						msh->_verV[mc + 2] = ver_b[pid[2][0]];
						if (tex_i)
						{
							msh->_texV[mc] = tex_b[pid[0][1]];
							msh->_texV[mc + 1] = tex_b[pid[3][1]];
							msh->_texV[mc + 2] = tex_b[pid[2][1]];
						}
						if (nor_i)
						{
							msh->_norV[mc] = nor_b[pid[0][2]];
							msh->_norV[mc + 1] = nor_b[pid[3][2]];
							msh->_norV[mc + 2] = nor_b[pid[2][2]];
						}
						mc += 3;					
					}
				}
			}
			else if (ll[0] == 'o')
			{
				if (curObj->_start)
				{
					curObj->_size = mc - curObj->_start;
					msh->_obj.push_back(curObj);
					curObj = new MeshObject();
				}
				curObj->_name = line.substr(2);
				DEBUG(std::cout << "obj : " << curObj->_name << "\n";)
				curObj->_start = mc;
			}
			else if (!line.compare(0, 6, "usemtl"))
			{
				std::string name = line.substr(7);
				if (curObj->_mat)
				{
					curObj->_size = mc - curObj->_start;
					msh->_obj.push_back(curObj);
					std::string obName = curObj->_name + "." + name;
					DEBUG(std::cout << "obj : " << obName << "\n";)
					curObj = new MeshObject();
					curObj->_name = obName;
				}
				curObj->_mat = getMaterial(name);
				curObj->_start = mc;
			}
			line_c++;
		}
		curObj->_size = mc - curObj->_start;
		msh->_obj.push_back(curObj);
		if (ver_b)
			delete[] ver_b;
		if (tex_b)
			delete[] tex_b;
		if (nor_b)
			delete[] nor_b;
	}
	_meshs.push_back(msh);
	fd.close();
	updateGl();
}

Mesh::Mesh(size_t vs, bool ts, bool ns)
{
	_vc = vs;
	_verV = vs ? new Vec3[vs] : NULL;
	_texV = ts ? new Vec2[vs] : NULL;
	_norV = ns ? new Vec3[vs] : NULL;
}

Mesh::~Mesh() {
	for (size_t i = 0; i < _obj.size(); ++i)
		delete _obj[i];
	if (_verV)
		delete[] _verV;
	if (_texV)
		delete[] _texV;
	if (_norV)
		delete[] _norV;
}

Models::Models() : _name() {
	_vao[0] = 0;
	_vbo[0] = 0;
	_vbo[1] = 0;
	_vbo[2] = 0;
}
Models::Models(const std::string &file) : Models() {
	loadModel(file);
}

Models::Models(const Models& src) {
	*this = src;
}

Models::~Models() {
	for (size_t i = 0; i < _meshs.size(); ++i)
		delete _meshs[i];
	_meshs.clear();
	for (size_t i = 0; i < _mat.size(); ++i)
		delete _mat[i];
	_mat.clear();
	if (_vao[0])
		glDeleteVertexArrays(1, _vao);
	if (_vbo[0])
		glDeleteBuffers(1, _vbo);
	if (_vbo[1])
		glDeleteBuffers(1, _vbo + 1);
	if (_vbo[2])
		glDeleteBuffers(1, _vbo + 2);
}

Models& Models::operator=(const Models& rhs) {
	(void)rhs;
	return (*this);
}
void Models::addMesh(Mesh *mesh) {
	_meshs.push_back(mesh);
}
void Models::loadModel(const std::string &file) {
	std::string ext;

	size_t p = file.find_last_of(".");
	if (p == std::string::npos)
		throw Except("not recognize file");
	ext = file.substr(p);
	if (ext == ".obj")
		parseObj(file);
	else
		throw Except("not recognize file");
}
void Models::setScale(unsigned int mesh, float scale)
{
	if (mesh > _meshs.size())
		throw Except("Models::setScale id > size");
	Mesh &msh = *_meshs[mesh];
	Vec3 min = Vec3(0);
	Vec3 max = Vec3(0);
	for (size_t i = 0; i < msh._vc; ++i)
	{
		Vec3 &v = msh._verV[i];
		for (int j = 0; j < 3; ++j)
		{
			if (v[j] < min[j])
				min[j] = v[j];
			if (v[j] > max[j])
				max[j] = v[j];
		}
	}
	Vec3 size = Vec3(max - min);
	float big = std::max(std::max(size[0], size[1]), size[2]);
	if (scale == 0.0f)
		scale = 1.0f / big;
	else
		scale = scale / big;
	for (size_t i = 0; i < msh._vc; ++i)
	{
		msh._verV[i] *= scale;
	}
	/*
	for (size_t i = 0; i < msh._obj.size(); ++i)
	{
		MeshObject &obj = *msh._obj[i];
		obj._model.scale(scale);
		obj._model[3][3] = 1.0f;
	}*/
	updateGl();
}
void Models::setScale(unsigned int mesh, const Vec3 &scale)
{
	if (mesh > _meshs.size())
		throw Except("Models::setScale id > size");
	Mesh &msh = *_meshs[mesh];
	Vec3 min = Vec3(0);
	Vec3 max = Vec3(0);
	for (size_t i = 0; i < msh._vc; ++i)
	{
		Vec3 &v = msh._verV[i];
		for (int j = 0; j < 3; ++j)
		{
			if (v[j] < min[j])
				min[j] = v[j];
			if (v[j] > max[j])
				max[j] = v[j];
		}
	}
	Vec3 size = Vec3(max - min);
	float big = std::max(std::max(size[0], size[1]), size[2]);
	Vec3 sc;
	if (scale == 0.0f)
		sc = Vec3(1.0f) / big;
	else
		sc = scale / big;
	for (size_t i = 0; i < msh._vc; ++i)
	{
		msh._verV[i] *= sc;
	}
	updateGl();
}
void Models::translate(unsigned int mesh, const Vec3 &pos)
{
	if (mesh > _meshs.size())
		throw Except("Models::setScale id > size");
	Mesh &msh = *_meshs[mesh];
	for (size_t i = 0; i < msh._vc; ++i)
	{
		msh._verV[i] += pos;
	}
	updateGl();
}
void Models::setMeshObjectOrigin(unsigned int obj, const Vec3 &v)
{
	Mesh &msh = *_meshs[0];
	if (obj > msh._obj.size())
		throw Except("Models::setScale id > size");
	MeshObject &ob = *msh._obj[obj];
	Vec3 min = Vec3(std::numeric_limits<float>::max());
	Vec3 max = Vec3(-std::numeric_limits<float>::max());
	for (size_t i = 0; i < ob._size; ++i)
	{
		Vec3 &v = msh._verV[ob._start + i];
		for (int j = 0; j < 3; ++j)
		{
			if (v[j] < min[j])
				min[j] = v[j];
			if (v[j] > max[j])
				max[j] = v[j];
		}
		// std::cout << "pos : " << v << "\n";
	}
	Vec3 size = Vec3(max - min);
	Vec3 origin = min + size * v;
	// std::cout << "min : " << min << ", max : " << max << "\n";
	// std::cout << "size : " << size << ", origin : " << origin << "\n";
	for (size_t i = 0; i < ob._size; ++i)
	{
		msh._verV[ob._start + i] -= origin;
	}
	ob._model.translate_in_place(origin);
	updateGl();
}
void Models::updateGl()
{
	if (_meshs.empty())
		return ;
	Mesh &msh = *_meshs[0];
	if (!_vao[0])
		glGenVertexArrays(1, _vao);
	if (!_vbo[0])
		glGenBuffers(1, _vbo);
	if (!_vbo[1] && msh._texV)
		glGenBuffers(1, _vbo + 1);
	if (!_vbo[2] && msh._norV)
		glGenBuffers(1, _vbo + 2);

	glBindVertexArray(_vao[0]);
	glBindBuffer(GL_ARRAY_BUFFER, _vbo[0]);
	glBufferData(GL_ARRAY_BUFFER,
		(GLsizeiptr)(sizeof(Vec3) * (size_t)msh._vc),
		msh._verV, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);
	if (msh._texV)
	{
		glBindBuffer(GL_ARRAY_BUFFER, _vbo[1]);
		glBufferData(GL_ARRAY_BUFFER,
			(GLsizeiptr)(sizeof(Vec2) * (size_t)msh._vc),
			msh._texV, GL_STATIC_DRAW);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(1);
	}
	if (msh._norV)
	{
		glBindBuffer(GL_ARRAY_BUFFER, _vbo[2]);
		glBufferData(GL_ARRAY_BUFFER,
			(GLsizeiptr)(sizeof(Vec3) * (size_t)msh._vc),
			msh._norV, GL_STATIC_DRAW);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(2);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}
void Models::render(const ProgramGlsl &prog)
{
	if (_meshs.empty())
		return ;
	// std::cout << _meshs[0]->_obj[0]->_name <<"\n";
	// _meshs[0]->_obj[0]->_model.rotate(Vec3(0, 0, 1), 0.01);
	// _meshs[0]->_obj[1]->_model.rotate(Vec3(0, 0, 1), 0.01);
	// _meshs[0]->_obj[2]->_model.rotate(Vec3(0, 1, 0), 0.05);
	// _meshs[0]->_obj[3]->_model.rotate(Vec3(0, 1, 0), -0.05);
	GLint idModel = prog.getUniformLocation("modelPart");
	glBindVertexArray(_vao[0]);
	for (size_t i = 0; i < _meshs.size(); ++i)
	{
		Mesh &msh = *_meshs[i];
		for (size_t j = 0; j < msh._obj.size(); ++j)
		{
			MeshObject &obj = *msh._obj[j];
			if (obj._mat)
			{
				Material &mat = *obj._mat;
				if (mat._map_Kd)
					mat._map_Kd->bind();
			}

			if (idModel != -1)
				glUniformMatrix4fv(idModel, 1, GL_FALSE, obj._model);	
			glDrawArrays(GL_TRIANGLES, obj._start, obj._size);
		}
		
	}
	/*for (size_t i = 0; i < _mat.size(); ++i)
	{
		const Material &cur = *_mat[i];
		if (cur._map_Kd)
			cur._map_Kd->bind();
		glDrawArrays(GL_TRIANGLES, cur._start, cur._size);
	}*/
	glBindVertexArray(0);
}
Mesh &Models::getMesh(int id)
{
	return (*_meshs[id]);
}

/*

void	mesh_get_center(t_mesh *msh, float center[3], float *scale)
{
	float			mm[2][3];
	unsigned int	i;
	int				j;

	if (!msh->size)
		return ;
	ft_memcpy(mm[0], msh->vertice, sizeof(float) * 3);
	ft_memcpy(mm[1], msh->vertice, sizeof(float) * 3);
	i = 0;
	while (i < msh->size)
	{
		j = -1;
		while (++j < 3)
			if (mm[0][j] < msh->vertice[i * 3 + j])
				mm[0][j] = msh->vertice[i * 3 + j];
			else if (mm[1][j] > msh->vertice[i * 3 + j])
				mm[1][j] = msh->vertice[i * 3 + j];
		i++;
	}
	center[0] = (mm[0][0] + mm[1][0]) / 2.0f;
	center[1] = (mm[0][1] + mm[1][1]) / 2.0f;
	center[2] = (mm[0][2] + mm[1][2]) / 2.0f;
	if (scale)
		*scale = MAX(ABS(mm[0][0] - mm[1][0]), MAX(ABS(mm[0][1] - mm[1][1]),
			ABS(mm[0][2] - mm[1][2])));
}*/


ModelsHuman::ModelsHuman() : Models("res/bomber/WhiteBomber.obj")
{
	setScale(0, 0);
	setMeshObjectOrigin(2, Vec3(0, 0.5, 0.5));
	setMeshObjectOrigin(3, Vec3(1, 0.5, 0.5));
	setMeshObjectOrigin(4, Vec3(0.5, 1, 0.25));
	setMeshObjectOrigin(5, Vec3(0.5, 1, 0.25));
	Mesh &mesh = getMesh(0);
	int size = mesh._obj.size();
	_defaultModel = new Mat4x4[size];
	for (int i = 0; i < size; ++i)
	{
		_defaultModel[i] = mesh._obj[i]->_model;
	}
}
void ModelsHuman::anim(float e)
{
	e = e * 4.0f;
	Mesh &mesh = getMesh(0);
	mesh._obj[2]->_model = Mat4x4(_defaultModel[2])
		.rotate_y(TORADIANS(sin(e) * 80))
		.rotate_z(TORADIANS(-45));
	mesh._obj[3]->_model = Mat4x4(_defaultModel[3])
		.rotate_y(TORADIANS(sin(e) * 80))
		.rotate_z(TORADIANS(45));
	mesh._obj[4]->_model = Mat4x4(_defaultModel[4])
		.rotate_x(TORADIANS(sin(e) * 45));
	mesh._obj[5]->_model = Mat4x4(_defaultModel[5])
		.rotate_x(TORADIANS(-sin(e) * 45));
}


ModelsIa::ModelsIa() : Models("res/bomber/IABomb.obj")
{
	setScale(0, 0.4);
	// setMeshObjectOrigin(2, Vec3(0, 0.5, 0.5));
	// setMeshObjectOrigin(3, Vec3(1, 0.5, 0.5));
	// setMeshObjectOrigin(4, Vec3(0.5, 1, 0.25));
	// setMeshObjectOrigin(5, Vec3(0.5, 1, 0.25));
	Mesh &mesh = getMesh(0);
	int size = mesh._obj.size();
	_defaultModel = new Mat4x4[size];
	for (int i = 0; i < size; ++i)
	{
		_defaultModel[i] = mesh._obj[i]->_model.rotate_y(TORADIANS(180));
	}
}
void ModelsIa::anim(float e)
{
	(void)e;
	// e = e * 4.0f;
	Mesh &mesh = getMesh(0);
	int size = mesh._obj.size();
	for (int i = 0; i < size; ++i)
	mesh._obj[i]->_model = Mat4x4(_defaultModel[i])
		.translate_in_place(Vec3(0, sin(e) * 0.25f + 0.5f, 0));
	/*mesh._obj[3]->_model = Mat4x4(_defaultModel[3])
		.rotate_y(TORADIANS(sin(e) * 80))
		.rotate_z(TORADIANS(45));
	mesh._obj[4]->_model = Mat4x4(_defaultModel[4])
		.rotate_x(TORADIANS(sin(e) * 45));
	mesh._obj[5]->_model = Mat4x4(_defaultModel[5])
		.rotate_x(TORADIANS(-sin(e) * 45));*/
}