#include "SceneGame.hpp"
#include "SceneJoin.hpp"
#include "Core.hpp"
#include "SceneMainMenu.hpp"
#include "SceneWaitingPlayers.hpp"

std::string SceneJoin::ip;
std::string SceneJoin::port;

static void joinCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	int		port;

	if (key == MenuKeyEnter && !SceneJoin::ip.empty() && !SceneJoin::port.empty()) {
        Core *core = reinterpret_cast<Core*>(userdata);
        SceneManager &man = core->getSceneManager();
		Scene *sceJoin = man.getScene("join");
		Scene *sceWaitingPlayers = new SceneWaitingPlayers(*core);

		try {
			port = std::stoi(SceneJoin::port);
		}
		catch (std::exception e) {
			std::cerr << e.what() << std::endl;
		}

	    SceneGame::instance->client.joinGame(SceneJoin::ip, port);

		man.addScene(sceWaitingPlayers);
	    man.setActiveScene(sceWaitingPlayers);
		man.removeScene(sceJoin);
	   	delete sceJoin;
	}
}

static void exitCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = (*core).getSceneManager();

		Scene *sceJoin = man.getScene("join");
		Scene *sceMain = new SceneMainMenu(*core);

		man.removeScene(sceJoin);
		man.addScene(sceMain);
		man.setActiveScene(sceMain);
		delete sceJoin;
	}
}

SceneJoin::SceneJoin(Core &core) {
	(void)core;

	_menu.addModule(new MenuButton("Server ip : ", joinCallback, &core));
	_menu.addModule(new MenuButton("Server port : ", joinCallback, &core));
	_menu.addModule(new MenuButton("Join server", joinCallback, &core));
	_menu.addModule(new MenuButton("Back to Menu", exitCallback, &core));
	ip.clear();
	port.clear();
}

SceneJoin::~SceneJoin() {
}

void SceneJoin::eventChar(Core &core, unsigned int key) {
	IMenuModule	*module;
	MenuButton	*button;
	std::string str;

	(void)core;

	if (key != '.' && (key < '0' || key > '9'))
		return;

	if (key != '.')
		key -= 48;

	module = _menu.getActiveModule();
	if ((button = dynamic_cast<MenuButton*>(module))) {
		str = button->getText();
		if (!str.compare(0, 9, "Server ip")) {
			if (key == '.') {
				button->setText(str +  '.');
				ip += '.';
			}
			else {
				button->setText(str +  std::to_string(key));
				ip += std::to_string(key);
			}
		}
		else if (!str.compare(0, 11, "Server port")) {
			if (key == '.') {
				button->setText(str + '.');
				port += '.';
			}
			else {
				button->setText(str + std::to_string(key));
				port += std::to_string(key);
			}
		}
	}
}

void SceneJoin::eventResize(Core &core, int width, int height) {
	_menu.resize(core);
	(void)width;
	(void)height;
}

void SceneJoin::eventKeyPress(Core &core, int key, int mods) {
	(void)mods;

	if (key == GLFW_KEY_ESCAPE)
	{
		SceneManager &man = core.getSceneManager();
		Scene *sceJoin = man.getScene("join");
		Scene *sceMain = new SceneMainMenu(core);
		man.removeScene(sceJoin);
		man.addScene(sceMain);
		man.setActiveScene(sceMain);
		delete sceJoin;
	}

	if (key == GLFW_KEY_BACKSPACE) {
		IMenuModule	*module;
		MenuButton	*button;
		std::string str;

		module = _menu.getActiveModule();
		if ((button = dynamic_cast<MenuButton*>(module))) {
			str = button->getText();
			if (!str.compare(0, 9, "Server ip") && !ip.empty()) {
				str.pop_back();
				button->setText(str);
				ip.pop_back();
			}
			else if (!str.compare(0, 11, "Server port") && !port.empty()) {
				str.pop_back();
				button->setText(str);
				port.pop_back();
			}
		}
	}

	_menu.keyPress(core, key);
}

void SceneJoin::eventButtonPress(Core &core, int key, int mods) {
	(void)mods;
	if (key == GLFW_MOUSE_BUTTON_LEFT)
	{
		const Window &win = core.getWindow();
		_menu.buttonPress(core, Vec2(
			(win.getMouseX() / win.getWidth() - 0.5) * 2,
			(win.getMouseY() / win.getHeight() - 0.5) * -2));
	}
}

void SceneJoin::update(Core &core) {
	(void)core;

	if (SceneGame::instance->getMap() != NULL && !SceneGame::instance->client.stopped) {
		SceneManager &man = core.getSceneManager();
		Scene *sceGame = man.getScene("game");
		Scene *sceJoin = man.getScene("join");

		if (!sceGame) {
			sceGame = SceneGame::instance;
			man.addScene(sceGame);
		}
		man.setActiveScene(sceGame);
		man.removeScene(sceJoin);
		delete sceJoin;
	}

	_menu.update(core);
}

void SceneJoin::render(Core &core) {
	SceneGame::instance->renderBackground();
	glClear(GL_DEPTH_BUFFER_BIT);
	_menu.render(core);
}

const std::string &SceneJoin::getName() {
	static const std::string name("join");
	return (name);
}
