#include "AudioManager.hpp"

AudioManager::AudioManager() {
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) == -1) {
		std::cout <<  Mix_GetError() << std::endl;
		return ;
	}

	srand(time(NULL));

	_musics.push_back(Mix_LoadMUS("sounds/musics/01-Welcome_Player.wav"));
	_musics.push_back(Mix_LoadMUS("sounds/musics/02-Royal_Entrance.wav"));
	_musics.push_back(Mix_LoadMUS("sounds/musics/03-Throne_Room.wav"));
	_musics.push_back(Mix_LoadMUS("sounds/musics/04-Factory_Time.wav"));
	_musics.push_back(Mix_LoadMUS("sounds/musics/05-Battle_Intro.wav"));
	_musics.push_back(Mix_LoadMUS("sounds/musics/06-Battle.wav"));
	_musics.push_back(Mix_LoadMUS("sounds/musics/07-Haunted_Forest.wav"));
	_musics.push_back(Mix_LoadMUS("sounds/musics/08-Witchs_Hut.wav"));
	_musics.push_back(Mix_LoadMUS("sounds/musics/09-Winter_Village.wav"));
	_musics.push_back(Mix_LoadMUS("sounds/musics/10-We_Can_Do_It.wav"));
	_musics.push_back(Mix_LoadMUS("sounds/musics/11-The_Plateau_at_Night.wav"));
	_musics.push_back(Mix_LoadMUS("sounds/musics/12-Miniboss_Fight.wav"));
	_musics.push_back(Mix_LoadMUS("sounds/musics/13-Eerie_Mausoleum.wav"));
	_musics.push_back(Mix_LoadMUS("sounds/musics/14-Inside_the_Tower.wav"));
	_musics.push_back(Mix_LoadMUS("sounds/musics/15-Final_Sacrifice.wav"));
	_musics.push_back(Mix_LoadMUS("sounds/musics/16-End_Credits.wav"));

	_sounds.push_back(Mix_LoadWAV("sounds/fxs/explosion1.wav"));
	_sounds.push_back(Mix_LoadWAV("sounds/fxs/death1.wav"));

	// Max sounds allowed to be played simultaneously.
	Mix_AllocateChannels(20);
}

AudioManager::~AudioManager() {
	std::for_each(_musics.begin(), _musics.end(), Mix_FreeMusic);
	std::for_each(_sounds.begin(), _sounds.end(), Mix_FreeChunk);
	Mix_CloseAudio();
}

void	AudioManager::playMusic(t_musics name) {
	Mix_PlayMusic(_musics[name], -1);
	Mix_VolumeMusic(_musicVolume * 1.28);
}

void	AudioManager::playRandomMusic() {
	Mix_PlayMusic(_musics[rand() % (_musics.size() - 1) + 1], -1);
	Mix_VolumeMusic(_musicVolume);
}

void	AudioManager::playSound(t_sounds name) {
	Mix_PlayChannel(-1, _sounds[name], 0);
	Mix_Volume(-1, _effectVolume * 1.28);
}

void	AudioManager::stopSounds() {
	Mix_HaltMusic();
	Mix_HaltChannel(-1);
}

// SDL mixer volume bounds : 0 - 128, our bounds : 0 - 100.

int		AudioManager::setEffectVolume(int newVolume) {
	if (newVolume < 0)
		_effectVolume = 0;
	else if (newVolume > 100)
		_effectVolume = 100;
	else
		_effectVolume = newVolume;
	return (_effectVolume);
}

int		AudioManager::setMusicVolume(int newVolume) {
	if (newVolume < 0)
		_musicVolume = 0;
	else if (newVolume > 100)
		_musicVolume = 100;
	else
		_musicVolume = newVolume;
	Mix_VolumeMusic(_musicVolume);
	return (_musicVolume);
}

int		AudioManager::effectVolumeUp() {
	_effectVolume = (_effectVolume <= 90) ? _effectVolume + 10 : 100;
	Mix_Volume(-1, _effectVolume * 1.28);
	return (_effectVolume);
}

int		AudioManager::musicVolumeUp() {
	_musicVolume = (_musicVolume <= 90) ? _musicVolume + 10 : 100;
	Mix_VolumeMusic(_musicVolume * 1.28);
	return (_musicVolume);
}

int			AudioManager::effectVolumeDown() {
	_effectVolume = (_effectVolume >= 10) ? _effectVolume - 10 : 0;
	Mix_Volume(-1, _effectVolume * 1.28);
	return (_effectVolume);
}

int			AudioManager::musicVolumeDown() {
	_musicVolume = (_musicVolume >= 10) ? _musicVolume - 10 : 0;
	Mix_VolumeMusic(_musicVolume * 1.28);
	return (_musicVolume);
}
