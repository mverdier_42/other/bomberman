#include <iostream>
#include "Map.hpp"
#include "SceneGame.hpp"
#include <IA.hpp>
#include <Character.hpp>

Map::Map() {
}

Map::Map(Vec2i size, eMapName mapName) : _mapName(mapName), _size(size) {
	// Fill _map with empty objects.
	_map = new AMapObject*[INT(MAP_X * MAP_Y)];

	for (size_t i = 0; i < UINT(MAP_X * MAP_Y); i++) {
		_map[i] = &_empty;
	}

	_mapChanged = false;

	//std::cout <<"map construct(" << this << ")\n";
}

Map::Map(Map const& src) {
	*this = src;
}

Map::~Map() {
	for (size_t i = 0; i < _players.size(); i++) {
		delete _players[i];
	}
	_players.clear();
	for (size_t i = 0; i < _IAs.size(); i++) {
        delete _IAs[i];
    }
    _IAs.clear();
	for (ssize_t i = 0; i < MAP_X * MAP_Y; i++) {
		if (_map[i] != &_empty)
			delete _map[i];
	}
	delete _map;
}

Map& Map::operator=(Map const& rhs) {
	(void)rhs;
	return (*this);
}

void	Map::update() {
	// Do something here.
	Vec2	plPos;
	Vec2	iaPos;
	/*if (this != SceneGame::instance->getMap() && playersAreDeads() && !_mapChanged) {
		std::cout << "Change map." << std::endl;
		SceneGame::instance->server->ChangeMap(MULTI1);
		_mapChanged = true;
	}*/

	for (size_t i = 0 ; i < _players.size(); i++) {
		if (_players[i])
			_players[i]->update(this);
	}

	for (size_t i = 0 ; i < UINT(MAP_X * MAP_Y); i++) {
		if (_map && _map[i])
			_map[i]->update(this);
	}
	for (size_t i = 0 ; i < _IAs.size(); i++) {
		if (_IAs[i])
			_IAs[i]->update(this);
    }
}

void	Map::explode(const Vec2i pos, int radius, bool drill, Character *owner) {
	AMapObject	*obj;
	int		x = pos[0];
	int		y = pos[1];
	OBJTYPE type;
	int		hp;

	Vec2i d[] = {Vec2i(0, 1), Vec2i(0, -1), Vec2i(-1, 0), Vec2i(1, 0)};
	int de[] = {0, 0, 0, 0};

	if (x < 0 || y < 0 || x >= MAP_X || y >= MAP_Y) {
		std::cout << "Error: pos out of map" << std::endl;
		return ;
		//to put: except
	}

	for(int r = 0; r < 4; r++) {
		for (int i = 0; i < radius && x + i * d[r].x >= 0 && x + i * d[r].x < MAP_X
		&& y + i * d[r].y >= 0 && y + i * d[r].y < MAP_Y; i++, de[r]++) {
			obj = _map[x + i * d[r].x + (y + i * d[r].y) * MAP_X];
			type = obj->getObjType();
			hp = obj->getHp();
			if (obj->exploded(this, drill, owner) == true) {
				if (type == WALL && hp == 1)
					de[r]++;
				break;
			} else {
				for (int j = 0; j < INT(_players.size()); j++) {
					if (x + i * d[r].x == _players[j]->getX() && y + i * d[r].y == _players[j]->getY() && !_players[j]->isDead()) {
						_players[j]->exploded(this, drill, owner);
					}
				}
				Character	*chara;
				for (int j = 0; j < INT(_IAs.size()); j++) {
					chara = _IAs[j]->getChara();
                    if (chara && x + i * d[r].x == chara->getX() && y + i * d[r].y == chara->getY() && !chara->isDead())
						chara->exploded(this, drill, owner);
                }
			}
		}
	}
	//TODO debug
/*/	std::cout << "expode : " << pos[0] << " " << pos[1] << "\n";
	for(int r = 0; r < 4; r++)
	std::cout << de[r] << "\n";*/
	if (SceneGame::instance)
		SceneGame::instance->addExplosion(pos, de[0] - 1, de[1] - 1, de[2] - 1, de[3] - 1);
}

bool	Map::putObject(AMapObject* object) {
	if (object->getObjType() == BOMB && _map[object->getX() + object->getY() * INT(MAP_X)]->getObjType() == POWERUP)
		dynamic_cast<Bomb *>(object)->setPow(dynamic_cast<PowerUp *>(_map[object->getX() + object->getY() * INT(MAP_X)]));
	_map[object->getX() + object->getY() * INT(MAP_X)] = object;
	return true;
}

bool	Map::removeObject(Vec2 pos) {
	AMapObject	*obj = getObj(pos[0], pos[1]);

	return (removeObject(obj));
}

bool	Map::removeObject(AMapObject* object) {
	if (!_map[object->getX() + object->getY() * INT(MAP_X)]->isEmpty() ) {
		_map[object->getX() + object->getY() * INT(MAP_X)] = &_empty;
		return true;
	}
	return false;
}

void	Map::addPlayer(Character *player) {
	player->setTp(_players.size());
	_players.push_back(player);
}

void	Map::removePlayer(int tpos) {
	if (tpos == -1 || tpos >= INT(_players.size()))
		std::cout << "ERROR removeplayer: i out of range !!!!" << std::endl;
	else {
		_players[tpos]->setTp(-1);
		_players.erase(_players.begin() + tpos);
		for (size_t i = UINT(tpos) ; i < _players.size(); i++) {
			_players[i]->tpMinus();
		}
	}
}

bool	Map::playersAreDeads() {
	for (size_t i = 0; i < _players.size(); i++) {
		if (_players[i] && !_players[i]->isDead())
			return (false);
	}

	return (true);
}

Character *Map::getPlayer(unsigned int tpos) {
	if (tpos >= _players.size() || tpos == (unsigned int)-1)
		return (NULL);
	return (_players[tpos]);
}

std::vector<Character *> &Map::getPlayers() {
	return _players;
}

AMapObject	*Map::getObj(Vec2 vec) {
	return (getObj(vec[0], vec[1]));
}

AMapObject	*Map::getObj(int x, int y) {
	if (x < 0 || y < 0 || x >= MAP_X || y >= MAP_Y) {
		return (&_full);
	} else {
		return (_map[INT(x + y * MAP_X)]);
	}
}
const Vec2i &Map::getSize() const {
	return (_size);
}

void	Map::addIA(IA *ia) {
	if (ia)
		_IAs.push_back(ia);
}

std::vector<IA*>	&Map::getIAs() {
	return (_IAs);
}

IA	*Map::getIA(unsigned int tpos) {
	if (tpos == std::numeric_limits<unsigned int>::max() || tpos >= _IAs.size())
		return (NULL);
	return (_IAs[tpos]);
}

eMapName	Map::getName() const {
	return (_mapName);
}

size_t		Map::getNbPlayers() const {
	return (_players.size());
}

size_t		Map::getNbIAs() const {
	return (_IAs.size());
}

bool		Map::victory() const {
	for (size_t i = 0; i < _IAs.size(); i++) {
		if (!_IAs[i]->isDead())
			return (false);
	}
	return (true);
}

void		Map::checkDeads() const {
	for (size_t i = 0; i < _IAs.size(); i++)
		if (_IAs[i]->isDead())
			SceneGame::instance->client.sendToServ(DIE, i + IAINDEXDEC);
	for (size_t i = 0; i < _players.size(); i++)
		if (_players[i]->isDead())
			SceneGame::instance->client.sendToServ(DIE, i);
}
