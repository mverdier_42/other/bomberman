#include <iostream>
#include "TextureManager.hpp"
#include "Except.hpp"

TextureManager *TextureManager::_manager = NULL;


TextureContainer::TextureContainer() {};
TextureContainer::~TextureContainer() {};

TextureContainer::TextureContainer(const std::string &file) :
	_file(file), _texture(file), _ref(1) {
}
bool TextureContainer::operator==(const std::string &rhs) {
	return (_file == rhs);
}
bool TextureContainer::operator==(const Texture &rhs) {
	return (_texture == rhs);
}



TextureManager::TextureManager() {
	if (_manager)
		throw Except("TextureManager already init");
	_manager = this;
}

TextureManager::~TextureManager() {
	_manager = NULL;
}

TextureManager &TextureManager::getManager()
{
	if (!_manager)
		throw Except("TextureManager not init");
	return (*_manager);
}

Texture &TextureManager::loadTexture(const std::string &file) {
	std::list<TextureContainer*>::iterator it = searchTexture(file);
	if (it == _list.end())
	{
		_list.push_back(new TextureContainer(file));
		return (_list.back()->_texture);
	}
	return ((*it)->_texture);
}
void TextureManager::unloadTexture(const std::string &file) {
	std::list<TextureContainer*>::iterator it = searchTexture(file);
	if (it == _list.end())
		throw Except("texture invalide");
	if (--(*it)->_ref <= 0)
	{
		delete *it;
		_list.erase(it);
	}
}
void TextureManager::unloadTexture(const Texture &texture) {
	std::list<TextureContainer*>::iterator it = searchTexture(texture);
	if (it == _list.end())
		throw Except("texture invalide");
	if (--(*it)->_ref <= 0)
	{
		delete *it;
		_list.erase(it);
	}
}


std::list<TextureContainer*>::iterator TextureManager::searchTexture(
	const std::string &file)
{
	std::list<TextureContainer*>::iterator it;
	for (it = _list.begin(); it != _list.end() && !(**it == file); ++it)
		;
	return (it);
}
std::list<TextureContainer*>::iterator TextureManager::searchTexture(
	const Texture &texture)
{
	std::list<TextureContainer*>::iterator it;
	for (it = _list.begin(); it != _list.end() && !(**it == texture); ++it)
		;
	return (it);
}