#include "SceneHost.hpp"
#include "SceneGame.hpp"
#include "Core.hpp"
#include "SceneMainMenu.hpp"
#include "SceneWaitingPlayers.hpp"

int SceneHost::nbPlayers;
int SceneHost::nbIAs;
bool	SceneHost::validated = false;

static void startCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
/*		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = core->getSceneManager();
		Scene *sceHost = man.getScene("host");
		Scene *sceWaitingPlayers = new SceneWaitingPlayers(*core, SceneHost::nbPlayers, SceneHost::nbIAs, SceneHost::mapID);
		man.removeScene(sceHost);
		man.addScene(sceWaitingPlayers);
		man.setActiveScene(sceWaitingPlayers);
		delete sceHost;*/
		SceneGame::instance->client.newGame(SceneHost::nbPlayers, SceneHost::nbIAs, RANDMULTI);
		SceneHost::validated = true;
	}
}

static void exitCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = (*core).getSceneManager();
		Scene *sceHost = man.getScene("host");
		Scene *sceMain = new SceneMainMenu(*core);
		man.removeScene(sceHost);
		man.addScene(sceMain);
		man.setActiveScene(sceMain);
		delete sceHost;
	}
}

SceneHost::SceneHost(Core &core) {
	_menu.addModule(new MenuButton("Players : 2", startCallback, &core));
	_menu.addModule(new MenuButton("IAs : 0", startCallback, &core));
	_menu.addModule(new MenuButton("Start", startCallback, &core));
	_menu.addModule(new MenuButton("Back to Menu", exitCallback, &core));

	nbPlayers = 2;
	nbIAs = 0;
	validated = false;
}

SceneHost::~SceneHost() {
	validated = false;
}

void SceneHost::eventResize(Core &core, int width, int height) {
	_menu.resize(core);
	(void)width;
	(void)height;
}

void SceneHost::eventKeyPress(Core &core, int key, int mods) {
	(void)mods;
	if (key == GLFW_KEY_ESCAPE)
	{
		SceneManager &man = core.getSceneManager();
		Scene *sceHost = man.getScene("host");
		Scene *sceMain = new SceneMainMenu(core);
		man.removeScene(sceHost);
		man.addScene(sceMain);
		man.setActiveScene(sceMain);
		delete sceHost;
	}

	if (key == GLFW_KEY_RIGHT) {
		IMenuModule	*module;
		MenuButton	*button;
		std::string str;

		module = _menu.getActiveModule();
		if ((button = dynamic_cast<MenuButton*>(module))) {
			str = button->getText();
			if (!str.compare(0, 7, "Players") && nbPlayers < 4 - nbIAs) {
				nbPlayers++;
				button->setText(str.replace(str.find(":") + 2, std::string::npos, std::to_string(nbPlayers)));
			}
			else if (!str.compare(0, 3, "IAs") && nbIAs < 4 - nbPlayers) {
				nbIAs++;
				button->setText(str.replace(str.find(":") + 2, std::string::npos, std::to_string(nbIAs)));
			}
		}
	}

	if (key == GLFW_KEY_LEFT) {
		IMenuModule	*module;
		MenuButton	*button;
		std::string str;

		module = _menu.getActiveModule();
		if ((button = dynamic_cast<MenuButton*>(module))) {
			str = button->getText();
			if (!str.compare(0, 7, "Players") && nbPlayers > 1) {
				nbPlayers--;
				button->setText(str.replace(str.find(":") + 2, std::string::npos, std::to_string(nbPlayers)));
			}
			else if (!str.compare(0, 3, "IAs") && nbIAs > 0) {
				nbIAs--;
				button->setText(str.replace(str.find(":") + 2, std::string::npos, std::to_string(nbIAs)));
			}
		}
	}
	_menu.keyPress(core, key);
}

void SceneHost::eventButtonPress(Core &core, int key, int mods) {
	(void)mods;
	if (key == GLFW_MOUSE_BUTTON_LEFT)
	{
		const Window &win = core.getWindow();
		_menu.buttonPress(core, Vec2(
			(win.getMouseX() / win.getWidth() - 0.5) * 2,
			(win.getMouseY() / win.getHeight() - 0.5) * -2));
	}
}

void SceneHost::update(Core &core) {
	_menu.update(core);

	if (SceneGame::instance && SceneGame::instance->client.serverIsReady() && validated) {
		validated = false;
        SceneManager &man = core.getSceneManager();
        Scene *sceHost = man.getScene("host");
        Scene *sceWaitingPlayers = new SceneWaitingPlayers(core, SceneHost::nbPlayers);
        man.removeScene(sceHost);
        man.addScene(sceWaitingPlayers);
        man.setActiveScene(sceWaitingPlayers);
        delete sceHost;
	}
}

void SceneHost::render(Core &core) {
	SceneGame::instance->renderBackground();
	glClear(GL_DEPTH_BUFFER_BIT);
	_menu.render(core);
}

const std::string &SceneHost::getName() {
	static const std::string name("host");
	return (name);
}
