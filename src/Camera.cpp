#include "Camera.hpp"

Camera::Camera(const Vec3 &origin, const Vec3 &pos, const Vec3 &euler) :
	_origin(origin), _pos(pos), _euler(euler), _update(true)
{

}
Camera::Camera(const Camera& src)
{
	*this = src;
}
const Mat4x4 &Camera::getCam()
{
	if (_update)
	{
		_update = false;
		_cam = Mat4x4::Translate(_origin)
			.rotate_x(_euler[0]) 
			.rotate_y(_euler[1]) *
			Mat4x4::Translate(_pos);
	}
	return (_cam);
}
const Vec3 &Camera::getPos() const
{
	return (_pos);
}
const Vec3 &Camera::getOrigin() const
{
	return (_origin);
}
const Vec3 &Camera::getEuler() const
{
	return (_euler);
}
Vec3 &Camera::getPos()
{
	_update = true;
	return (_pos);
}
Vec3 &Camera::getOrigin()
{
	_update = true;
	return (_origin);
}
Vec3 &Camera::getEuler()
{
	_update = true;
	return (_euler);
}
void Camera::setCam(const Mat4x4 &cam)
{
	_cam = cam;
}
void Camera::setPos(const Vec3 &pos)
{
	_update = true;
	_pos = pos;
}
void Camera::setOrigin(const Vec3 &origin)
{
	_update = true;
	_origin = origin;
}
void Camera::setEuler(const Vec3 &euler)
{
	_update = true;
	_euler = euler;
}