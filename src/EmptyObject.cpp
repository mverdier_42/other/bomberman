#include "EmptyObject.hpp"
#include "Map.hpp"

EmptyObject::EmptyObject() {
	_hp = -1;
}

EmptyObject::EmptyObject(const EmptyObject& src) {
	*this = src;
}

EmptyObject::~EmptyObject() {
}

EmptyObject& EmptyObject::operator=(const EmptyObject& rhs) {
	(void)rhs;
	return (*this);
}

bool	EmptyObject::exploded(Map *map, bool drill, Character *owner) {
	(void)map;
	(void)drill;
	(void)owner;
	return false;
}

bool	EmptyObject::isCollide() {
	return (false);
}

void	EmptyObject::update(Map *map) {
	(void)map;
}

bool	EmptyObject::isEmpty() const{
	return true;
}
OBJTYPE			EmptyObject::getObjType() const {
    return      EMPTY;
}