#include "Map.hpp"
#include <math.h>

bool	AMapObject::isEmpty() const{
	return (false);
}

int	AMapObject::getHp() const{
	return (_hp);
}

int AMapObject::getX() const {
    return (INT(round(POS_X)));
}
int AMapObject::getY() const {
    return (INT(round(POS_Y)));
}

float AMapObject::getFY() const {
    return (POS_Y);
}

float AMapObject::getFX() const {
    return (POS_X);
}
const Vec2 &AMapObject::getPos() const {
    return _pos;
}

OBJTYPE			AMapObject::getObjType() const {
    return      AMAPOBJECT;
}

