#include "SceneGame.hpp"
#include "SceneWaitingPlayers.hpp"
#include "Core.hpp"
#include "SceneMainMenu.hpp"
#include <ifaddrs.h>

static void exitCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = (*core).getSceneManager();

		Scene *sceWaitingPlayers = man.getScene("waiting players");
		Scene *sceGame = man.getScene("game");
		Scene *sceMain = new SceneMainMenu(*core);

		SceneGame::instance->client.disconnect();

		if (sceGame)
			man.removeScene(sceGame);
		man.removeScene(sceWaitingPlayers);
		man.addScene(sceMain);
		man.setActiveScene(sceMain);
		delete sceWaitingPlayers;
	}
}

SceneWaitingPlayers::SceneWaitingPlayers(Core &core) {
	_host = false;
	_connected = false;
	_nbPlayers = -1;
	_nbPlayersConn = -1;

	_menu.addModule(new MenuButton("Waiting for players"));
	_menu.addModule(new MenuButton("Back to Menu", exitCallback, &core));
}

std::string getStrIp() {
    struct ifaddrs      *ifap = NULL;
    struct sockaddr_in  *sin = NULL;
    char                *addr = NULL;
	std::string			ip;

    getifaddrs(&ifap);
    while (ifap) {
        if (!std::string("en0").compare(ifap->ifa_name))
            sin = (struct sockaddr_in*)ifap->ifa_addr;
        ifap = ifap->ifa_next;
    }
    try {
        if (sin && (addr = inet_ntoa(sin->sin_addr))) {
            ip.clear();
            ip = std::string(addr);
		}
    }
    catch (std::exception &e) {
		std::cerr << "error in 'put ip and port' " << e.what() << std::endl;
	}
	return (ip);
}

SceneWaitingPlayers::SceneWaitingPlayers(Core &core, int nbPlayers) {
	_host = true;
	_connected = true;

	while (Client::port < 0)
		usleep(10);

	_nbPlayers = nbPlayers;
	_nbPlayersConn = SceneGame::instance->client.getNbPlayersConn();
	_menu.addModule(new MenuButton("Server ip : " + getStrIp()));
	_menu.addModule(new MenuButton("Server port : " + std::to_string(Client::port)));
	_menu.addModule(new MenuButton("Players connected : " + std::to_string(_nbPlayersConn) + " / " + std::to_string(_nbPlayers)));
	_menu.addModule(new MenuButton("Back to Menu", exitCallback, &core));
}

SceneWaitingPlayers::~SceneWaitingPlayers() {
}

void SceneWaitingPlayers::eventResize(Core &core, int width, int height) {
	_menu.resize(core);
	(void)width;
	(void)height;
}

void SceneWaitingPlayers::eventKeyPress(Core &core, int key, int mods) {
	(void)mods;

	_menu.keyPress(core, key);
}

void SceneWaitingPlayers::eventButtonPress(Core &core, int key, int mods) {
	(void)mods;
	if (key == GLFW_MOUSE_BUTTON_LEFT)
	{
		const Window &win = core.getWindow();
		_menu.buttonPress(core, Vec2(
			(win.getMouseX() / win.getWidth() - 0.5) * 2,
			(win.getMouseY() / win.getHeight() - 0.5) * -2));
	}
}

void SceneWaitingPlayers::update(Core &core) {
	(void)core;

	if (!_host && (SceneGame::instance->client.error || Client::stopped)) {
		exitCallback(*_menu.getActiveModule(), MenuKeyEnter, &core);
		return ;
	}

	if (_host && SceneGame::instance->client.getNbPlayersConn() != _nbPlayersConn) {
		std::vector<IMenuModule*> *modules = _menu.getModules();
		_nbPlayersConn = SceneGame::instance->client.getNbPlayersConn();

		for (size_t i = 0; i < modules->size(); i++) {
			MenuButton *button;
			std::string str;

			if ((button = dynamic_cast<MenuButton*>((*modules)[i]))) {
				str = button->getText();
				if (!str.compare(0, 7, "Players"))
					button->setText("Players connected : " + std::to_string(_nbPlayersConn) + " / " + std::to_string(_nbPlayers));
			}
		}
	}
	if (SceneGame::instance->getMap() != NULL) {
		SceneManager &man = core.getSceneManager();
		Scene *sceGame = man.getScene("game");
		Scene *sceWaitingPlayers = man.getScene("waiting players");

		if (!sceGame) {
			sceGame = SceneGame::instance;
			man.addScene(sceGame);
		}
		man.setActiveScene(sceGame);
		man.removeScene(sceWaitingPlayers);
		delete sceWaitingPlayers;
	}
	else if (!_host && !_connected) {
		std::vector<IMenuModule*> *modules = _menu.getModules();
		MenuButton *button = dynamic_cast<MenuButton*>((*modules)[0]);
		std::string str = button->getText();
		eClientStatus status = SceneGame::instance->client.getStatus();

		switch (status) {
		case CONNECTED :
			_connected = true;
			_nbPlayersConn = SceneGame::instance->client.getNbPlayersConn();
			button->setText("Players connected : " + std::to_string(_nbPlayersConn) + " / " + std::to_string(SceneGame::instance->client.getNbPlayers()));
			break;
		case WAITING :
			button->setText("Waiting connection");
			break;
		case IDLE :
			button->setText("Unknown status");
			break;
		case TIMEOUT :
			button->setText("Connection timeout");
			break;
		}
	}
	else if (!_host && _connected && SceneGame::instance->client.getNbPlayersConn() != _nbPlayersConn) {
		std::vector<IMenuModule*> *modules = _menu.getModules();
		MenuButton *button = dynamic_cast<MenuButton*>((*modules)[0]);
		std::string str = button->getText();
		_nbPlayersConn = SceneGame::instance->client.getNbPlayersConn();

		button->setText(str.replace(str.find("/") - 2, 1, std::to_string(_nbPlayersConn)));
	}

	_menu.update(core);
}

void SceneWaitingPlayers::render(Core &core) {
	SceneGame::instance->renderBackground();
	glClear(GL_DEPTH_BUFFER_BIT);
	_menu.render(core);
}

const std::string &SceneWaitingPlayers::getName() {
	static const std::string name("waiting players");
	return (name);
}
