/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SceneGame.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrouthie <jrouthie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/30 01:50:57 by jrouthie          #+#    #+#             */
/*   Updated: 2018/10/13 15:50:07 by jrouthie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cmath>
#include "Core.hpp"
#include "SceneGame.hpp"
#include "SceneMainMenu.hpp"
#include "SceneGame.hpp"
#include <Client.hpp>
#include "RenderData.hpp"
#include <IA.hpp>
#include <Character.hpp>

SceneGame *SceneGame::instance = NULL;

const nlohmann::json SceneGame::_defaultConfig = R"(
	{
		"Move up" : "W",
		"Move left" : "A",
		"Move down" : "S",
		"Move right" : "D",
		"Bomb" : " ",
		"Activate" : "E",
		"Effects volume" : 50,
		"Music volume" : 20,
		"Screen width" : 720,
		"Screen height" : 480,
		"Full screen" : false
	}
)"_json;

const nlohmann::json SceneGame::_defaultSave = R"(
	{
		"Seed" : -1,
		"Level" : 1,
		"Speed" : 5,
		"MaxBombs" : 1,
		"BombPow" : 2,
		"DrillBomb" : false,
		"BombCtrl" : false,
		"Score" : 0
	}
)"_json;

SceneGame::SceneGame(Core &core, Map *map) : core(core), _map(map),
		_particle(&TextureManager::getManager().loadTexture("res/flame.bmp"),
			new ProgramGlsl(Shader("particle_g.glsl"), Shader("particle_v.glsl"), Shader("particle_f.glsl")), 10000),
		_power_speed("res/tex/power_speed.png"),
		_power_bomb("res/tex/power_bomb.png"),
		_power_power("res/tex/power_power.png"),
		_power_bombWall("res/tex/power_bombWall.png"),
		_power_remote("res/tex/power_remote.png"),
		_power_life("res/tex/power_life.png"),
		_wall("res/tex/wall.png"),
		_grass("res/tex/grass.png"),
		_uiTime("res/tex/uiTime.png"),
		_trapDoor("res/tex/trapdoor.png"),
		_trapDoorOpen("res/tex/trapdooropen.png"),
		_background("res/tex/menBackground.png"),
		_oldElapsed(0),
		_cam(Vec3(0, -1, 7), Vec3(0), Vec3(TORADIANS(-70), 0, 0)),
		_freeCam(false)
	{
	(void)_fileSave;
	(void)_save;
	(void)_freeCam;

	if (instance)
		throw Except("SceneGame already exist");

	bzero(&_gameSave, sizeof(t_gameSave));
    bzero(&_gameInputs, sizeof(t_gameInputs));
	_level = 0;
	_resetTimer = false;
	_savPlayer.saved = false;
	instance = this;
	_key = "s&uFd$42ds@FGx3S ;.gfsW7";
	audioManager.playMusic(MAIN);

	// _model.loadModel("res/bomber/WhiteBomber.obj");
	// _model.setScale(0, 0);
	_prog = new ProgramGlsl(Shader("model_v.glsl"), Shader("model_f.glsl"));
	// glEnable(GL_CULL_FACE);
	// glDisable(GL_CULL_FACE);

	loadGameConfigs();
	loadGameSave();
	pthread_mutex_init(&_mutex, NULL);

	gotMapDesc = false;
	core.getWindow().setWindowOption(_gameInputs.screenWidth, _gameInputs.screenHeight, _gameInputs.fullScreen);
}
void SceneGame::eventClose(Core &core)
{
	(void)core;
    client.disconnect();
}
SceneGame::~SceneGame()
{
    client.disconnect();
	pthread_mutex_lock(&_mutex);
	if (_map) {
		delete _map;
		_map = NULL;
	}
	pthread_mutex_unlock(&_mutex);
	instance = NULL;
	pthread_mutex_destroy(&_mutex);
	audioManager.stopSounds();
}

void SceneGame::eventResize(Core &core, int width, int height)
{
	(void)core;
	(void)width;
	(void)height;
	_matProjection = Mat4x4::Perspective(TORADIANS(70.0f),
		static_cast<float>(width) / static_cast<float>(height), 0.001f, 1000.0f);
}

void SceneGame::eventKeyPress(Core &core, int key, int mods)
{
	int         playerId;
	Character	*player;
	(void)core;
	(void)key;
	(void)mods;

	pthread_mutex_lock(&_mutex);
	if (!_map) {
		pthread_mutex_unlock(&_mutex);
		return ;
	}
	playerId = client.getPlayerId();
	player = _map->getPlayer(playerId);
	if (key == GLFW_KEY_ESCAPE) {
		SceneManager &man = core.getSceneManager();
		Scene *sceMain = new SceneMainMenu(core);
		man.addScene(sceMain);
		man.setActiveScene(sceMain);
		if (player)
			player->unmove();
	}
	if (!player)
		return ;
	if (key == GLFW_KEY_UP || key == _gameInputs.moveUp[0])
		player->move(Vec2(0, 1));

	if (key == GLFW_KEY_DOWN || key == _gameInputs.moveDown[0])
		player->move(Vec2(0, -1));

	if (key == GLFW_KEY_LEFT || key == _gameInputs.moveLeft[0])
		player->move(Vec2(-1, 0));

	if (key == GLFW_KEY_RIGHT || key == _gameInputs.moveRight[0])
		player->move(Vec2(1, 0));

	if (key == GLFW_KEY_KP_0 || key == _gameInputs.bomb[0])
		player->putBomb();

	if (key == _gameInputs.activate[0]) {
		client.sendToServ(TRIGBOMB);
		player->detonate(_map);
	}
	pthread_mutex_unlock(&_mutex);

	if (key == GLFW_KEY_O)
		audioManager.playSound(EXP);

	if (key == GLFW_KEY_P)
		audioManager.playSound(DEATH);
}

void SceneGame::eventKeyRelease(Core &core, int key, int mods)
{
	int         playerId;
	(void)core;
	(void)key;
	(void)mods;

	pthread_mutex_lock(&_mutex);
	if (!_map) {
		pthread_mutex_unlock(&_mutex);
		return ;
	}
	playerId = client.getPlayerId();
	Character *player = _map->getPlayer(playerId);
	if (!player)
	{
		pthread_mutex_unlock(&_mutex);
		return ;
	}
	if (key == GLFW_KEY_UP || key == _gameInputs.moveUp[0])
		player->unmove(Vec2(0, 1));

	if (key == GLFW_KEY_DOWN || key == _gameInputs.moveDown[0])
		player->unmove(Vec2(0, -1));

	if (key == GLFW_KEY_LEFT || key == _gameInputs.moveLeft[0])
		player->unmove(Vec2(-1, 0));

	if (key == GLFW_KEY_RIGHT || key == _gameInputs.moveRight[0])
		player->unmove(Vec2(1, 0));

	if (key == GLFW_KEY_KP_0 || key == _gameInputs.bomb[0] || key == _gameInputs.activate[0])
		player->unputBomb();
	pthread_mutex_unlock(&_mutex);
}

void SceneGame::eventButtonPress(Core &core, int key, int mods)
{
	(void)core;
	(void)key;
	(void)mods;
}

void SceneGame::eventMouseMove(Core &core, double x, double y)
{
	Window &win = core.getWindow();

	static float xx;
	static float yy;
	if (glfwGetMouseButton(win.getGLFW(), GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
	{
		Vec3 a = _cam.getEuler() + Vec3(y - yy, x - xx, 0) * 0.01;
		a.x = std::min(std::max(a.x, -2.8f), 0.0f);
		_cam.getEuler() = a;
	}
	xx = x;
	yy = y;
}

void SceneGame::update(Core &core)
{
	Window &win = core.getWindow();
	if (_resetTimer) {
		glfwSetTime(0);
		_resetTimer = false;
	}
	if (glfwGetMouseButton(win.getGLFW(), GLFW_MOUSE_BUTTON_LEFT) != GLFW_PRESS &&
		_cam.getEuler() != Vec3(TORADIANS(-70), 0, 0))
	{
		Vec3 dir(Vec3(TORADIANS(-70), 0, 0) - _cam.getEuler());
		float len = dir.length();
		_cam.getEuler() += dir.normalise() * std::min(len, 0.1f);
	}

	(void)core;
	pthread_mutex_lock(&_mutex);
	if (_map) {
		int			id;
		Character	*player;

		id = client.getPlayerId();
		player = _map->getPlayer(id);
		if (player)
			client.sendToServ(POSITION, player->getAnimate(), player->getPos(), player->getDir());
	}
	pthread_mutex_unlock(&_mutex);
	if (Client::stopped) {
		SceneManager &man = core.getSceneManager();
		Scene *sceMain;
		Scene *sceGame = man.getScene("game");

		client.disconnect();
		if (sceGame)
            man.removeScene(sceGame);
		sceMain = new SceneMainMenu(core);
		man.addScene(sceMain);
		man.setActiveScene(sceMain);
	}
	pthread_mutex_lock(&_mutex);
	if (_map) {
        _map->update();
		_map->checkDeads();
	}
	pthread_mutex_unlock(&_mutex);
}

float getRand()
{
	int m2 = RAND_MAX / 2;
	return (static_cast<float>(std::rand() - m2) / static_cast<float>(m2));
}

void SceneGame::render(Core &core)
{
	pthread_mutex_lock(&_mutex);
	if (!_map) {
		pthread_mutex_unlock(&_mutex);
		return ;
	}
	double e = core.getWindow().getElapsed();
	double delta = std::max(0.0, e - _oldElapsed);
	_oldElapsed = e;
	Vec4 color = Vec4(0);

	_prog->bind();
	glUniform1i(_prog->getUniformLocation("uni_tex"), 0);
	GLint id = _prog->getUniformLocation("projection");
	if (id != -1)
		glUniformMatrix4fv(id, 1, GL_FALSE, _matProjection);

	Character *player = _map->getPlayer(client.getPlayerId());
	if (player)
	{
		const Vec2 &pos = player->getPos();
		_cam.setPos(Vec3(-pos[0] - 0.5f, 0, -pos[1] - 0.5f));
	}
	Mat4x4 cam = _cam.getCam();
	if (false)//tremblement
	{
		float ee = e * 50;
		cam = cam * Mat4x4::Translate(Vec3(
			std::cos(ee) * 0.03f,
			std::cos(ee + TORADIANS(120)) * 0.03f,
			std::cos(ee + TORADIANS(240)) * 0.03f));
		cam.rotate(Vec3(0, 1, 0), std::cos(ee) * 0.01f);
	}

	id = _prog->getUniformLocation("view");
	if (id != -1)
		glUniformMatrix4fv(id, 1, GL_FALSE, cam);


	// mat = Mat4x4::Identity();

	color = Vec4(1, 1, 1, 0);
	glUniform4fv(_prog->getUniformLocation("uni_color"), 1, color);
	id = _prog->getUniformLocation("model");
	std::vector<Character*> &play = _map->getPlayers();
	for (size_t i = 0; i < (size_t)play.size(); i++) {
		if (!play[i]->isDead()) {
			RenderDataCharacter data = RenderDataCharacter(*play[i]);
			_model.anim(play[i]->getWalkAnim(delta));
			// _model.anim(e);//TODO e == temp depuis debut anim
			if (id != -1)
				glUniformMatrix4fv(id, 1, GL_FALSE, data._model);
			_model.render(*_prog);
		}
	}
	std::vector<IA*> IAs = _map->getIAs();
    for (size_t i = 0; i < (size_t)IAs.size(); i++) {
		if (!IAs[i]->isDead()) {
			Character &cara = *IAs[i]->getChara();
            RenderDataCharacter data = RenderDataCharacter(cara);
			// _model.anim(e);//TODO e == temp depuis debut anim
            if (id != -1)
                glUniformMatrix4fv(id, 1, GL_FALSE, data._model);
			if (IAs[i]->getType() == GHOSTLIKE)
			{
				Texture::unbind();
				color = Vec4(1, 1, 1, 0);
				glUniform4fv(_prog->getUniformLocation("uni_color"), 1, color);
				_modelIa.anim(cara.getWalkAnim(delta));
				_modelIa.render(*_prog);
			}
			else
			{
				// color = Vec4(1, 1, 1, 0);
				// glUniform4fv(_prog->getUniformLocation("uni_color"), 1, color);
				_model.anim(cara.getWalkAnim(delta));
				_model.render(*_prog);
			}
		}
    }

	Vec2i mapSize = _map->getSize();

	for (int x = -1; x <= mapSize[0]; ++x)
	for (int y = -1; y <= mapSize[1]; ++y)
	{
		AMapObject *obj = _map->getObj(x, y);
		Models *model = NULL;
		if (!obj)
		{
            model = &_modelCube;
            RenderData data = RenderData(Vec2(x, y));
            glUniformMatrix4fv(id, 1, GL_FALSE, data._model);
			color = Vec4(0.10, 0.10, 0.10, 0.0);
			_wall.bind();
		}
		else
		{
            OBJTYPE type = obj->getObjType();
			if (type == WALL)
			{
				model = &_modelCube;
				RenderData data = RenderData(Vec2(x, y));
				glUniformMatrix4fv(id, 1, GL_FALSE, data._model);
				if (obj->getHp() > 2)
					color = Vec4(0.24, 0.18, 0.14, 0.0);
				else if (obj->getHp() == 2)
					color = Vec4(0.38, 0.3, 0.23, 0.0);
				else if (obj->getHp() == 1)
					color = Vec4(0.8, 0.65, 0.53, 0.0);
				else if (obj->getHp() < 0)
					color = Vec4(0.31, 0.31, 0.31, 0.0);
				_wall.bind();
			}
			else
			{
				RenderData data = RenderData(Vec3(x, 0, y));
				glUniformMatrix4fv(id, 1, GL_FALSE, data._model);
				if ((x + y) & 1)
					color = Vec4(0.9, 0.9, 0.9, 0);
				else
					color = Vec4(1, 1, 1, 0);
				_grass.bind();
				glUniform4fv(_prog->getUniformLocation("uni_color"), 1, color);
				_modelFloor.render(*_prog);
				if (type == BOMB)
				{
					model = &_modelBomb;
					RenderData data = RenderData(Vec3(x + 0.5f, 0, y + 0.5f));
					glUniformMatrix4fv(id, 1, GL_FALSE, data._model);
					if (dynamic_cast<Bomb*>(obj)->getDrill() == true)
						color = Vec4(0.2, 0.2, 1, 0);
					else
						color = Vec4(1, 0.2, 0.2, 0);
					//if (static_cast<Bomb&>(obj).getControl() == true)
					glUniform4fv(_prog->getUniformLocation("uni_color"), 1, color);
					model->render(*_prog);
				}
				if (type == POWERUP || (type == BOMB && dynamic_cast<Bomb *>(obj)->getPow()))
				{
					PowerUp *up;

					model = &_modelPower;
					if (type == POWERUP)
						up = dynamic_cast<PowerUp*>(obj);
					else
						up = dynamic_cast<PowerUp*>(dynamic_cast<Bomb *>(obj)->getPow());
					Effect effect = up->getEffect();
					if (effect == NOEFFECT)
						glBindTexture(GL_TEXTURE_2D, 0);
					else if (effect == EXIT)
					{
						if (type == BOMB && model) {
							glUniform4fv(_prog->getUniformLocation("uni_color"), 1, color);
							model->render(*_prog);
						}
						RenderData data = RenderData(Vec3(x, 0.05, y));
						glUniformMatrix4fv(id, 1, GL_FALSE, data._model);
						if (_map->victory())
							_trapDoorOpen.bind();
						else
							_trapDoor.bind();
						color = Vec4(1, 1, 1, 0);
						glUniform4fv(_prog->getUniformLocation("uni_color"), 1, color);
						_modelFloor.render(*_prog);
						continue;
					}
					else if (effect == SPEED)
						_power_speed.bind();
					else if (effect == BNB)
						_power_bomb.bind();
					else if (effect == BPOW)
						_power_power.bind();
					else if (effect == DBOMB)
						_power_bombWall.bind();
					else if (effect == CBOMB)
						_power_remote.bind();
					/*RenderData data = RenderData(Vec3(x, -1, y));
					glUniformMatrix4fv(id, 1, GL_FALSE, data._model);
					color = Vec4(0.1, 0.1, 0.1, 1);
					_modelCube.render(*_prog);*/

					Mat4x4 model = RenderData(Vec3(x + 0.5f, 0.5f, y + 0.5f))._model;

					// model = model * Mat4x4::Identity().rotate_x(TORADIANS(80));
					model.rotate_x(TORADIANS(30));
					model.rotate_y(TORADIANS(e * 180));
					glUniformMatrix4fv(id, 1, GL_FALSE, model);
					color = Vec4(1, 1, 1, 0);
					// color = Vec4(0.1, 0.1, 0.8, 0);
				}
			}
		}
		if (model)
		{
			glUniform4fv(_prog->getUniformLocation("uni_color"), 1, color);
			model->render(*_prog);
		}
	}

	{
		RenderData data = RenderData(Vec2(-50));
		glUniformMatrix4fv(id, 1, GL_FALSE, data._model);
		color = Vec4(1, 1, 1, 0);
		glUniform4fv(_prog->getUniformLocation("uni_color"), 1, color);
		_grass.bind();
		_modelBackground.render(*_prog);
	}

	glUseProgram(0);

	_particle.update(delta);
	_particle.render(_matProjection, cam);


	Renderer &rd = core.getRenderer();
	Window &win = core.getWindow();
	int width = win.getWidth();
	int height = win.getHeight();
	const Vec2 ps(1.0f / width, 1.0f / height);
	if (player)
	{
		const Vec2 startP(1.0 - ps[0] * 150, 1 - ps[1] * 74);
		_power_power.bind();
		renderUiStat(core, startP, ps, std::to_string(player->getBombPow() - 1));
		_power_bomb.bind();
		renderUiStat(core, startP - Vec2(0, ps[1] * 70), ps, std::to_string(player->getMaxBombs()));
		_power_life.bind();
		renderUiStat(core, startP - Vec2(0, ps[1] * 140), ps, std::to_string(player->getHp()));
	}
	pthread_mutex_unlock(&_mutex);
	{
		_uiTime.bind();
		const Vec2 uiSize(200, 80);
		const Vec2 uiRenderSize(ps * uiSize);
		const Vec2 uiRenderPos(-uiRenderSize[0] * 0.5f, 1.0f - ps[1] * 10 - uiRenderSize[1]);
		rd.renderTexture(Vec4(uiRenderPos, uiRenderSize), Vec4(1));
		std::string strTime = std::to_string(e);
		int pp = strTime.find('.');
		if (pp >= 0 && static_cast<int>(strTime.size()) > pp + 3)
			strTime = /*std::string(std::max(0, 3 - pp), ' ') + */strTime.substr(0, pp + 3);
		Vec2 ss = rd.getTextSize(strTime);
		Vec2 txtPos(ss * -0.5f + Vec2(0, 1.0f - ps[1] * 15));
		rd.renderText(strTime, txtPos);
	}

	if (glGetError() != GL_NO_ERROR) throw Except("glerror");

}
void SceneGame::renderUiStat(Core &core, const Vec2 &pos, const Vec2 &ps, const std::string &str)
{
	Renderer &rd = core.getRenderer();
	rd.renderTexture(Vec4(pos, ps * 64), Vec4(1));
	rd.renderText(str, pos + ps * Vec2(69, 37));
}
const std::string &SceneGame::getName()
{
	static const std::string name("game");
	return (name);
}

void SceneGame::addExplosion(const Vec2i &pos, unsigned int up, unsigned int down,
	unsigned int left, unsigned int right)
{
	Vec3 pp(pos[0] + 0.5f, 0.5f, pos[1] + 0.5f);
	_particle.addParticles(pp, 1.0f, 1.0f, 6);
	Vec3 t;
	t = pp;
	for (unsigned int i = 0; i < up * 2; ++i)
		_particle.addParticles(t += Vec3(0, 0, 0.5f), 1.0f, 1.0f, 6);
	t = pp;
	for (unsigned int i = 0; i < down * 2; ++i)
		_particle.addParticles(t += Vec3(0, 0, -0.5f), 1.0f, 1.0f, 6);
	t = pp;
	for (unsigned int i = 0; i < left * 2; ++i)
		_particle.addParticles(t += Vec3(-0.5f, 0, 0), 1.0f, 1.0f, 6);
	t = pp;
	for (unsigned int i = 0; i < right * 2; ++i)
		_particle.addParticles(t += Vec3(0.5f, 0, 0), 1.0f, 1.0f, 6);
}

void SceneGame::setMap(Map *map) {
	pthread_mutex_lock(&_mutex);
	if (map) {
		//audioManager.stopSounds();
		if (_map)
			delete _map;
		_map = NULL;
		_map = map;
	}
	else if (_map) {
		delete (_map);
		_map = NULL;
		pthread_mutex_unlock(&_mutex);
		return ;
	}
	pthread_mutex_unlock(&_mutex);
}

void	SceneGame::saveGame() {
	_gameSave.seed = client.timeRand;
	_gameSave.level = _level;
	if (_map) {
		Character *player = _map->getPlayer(0);
		if (player) {
			_gameSave.score = player->getScore();
			_gameSave.player = player->toStruct();
		}
	}
	setGameSave();
}

void SceneGame::loadGameConfigs() {
	_fileConfig.open("config.json", std::fstream::in);

	if (!_fileConfig.is_open())
		_fileConfig.open("config.json", std::fstream::out | std::fstream::trunc);

	try {
		_fileConfig >> _config;
	}
	catch (nlohmann::detail::parse_error & e) {
		_fileConfig << std::setw(4) << _defaultConfig << std::endl;
		std::cout << "config.json was unusable or unexistant and now has default configs." << std::endl;
		_fileConfig.close();
	}

	_fileConfig.close();
	initGameInputs();
}

void SceneGame::loadGameSave() {
	std::string tmp;

	_fileSave.open("save.json", std::fstream::in);

	try {
		_fileSave >> tmp;
		_save = nlohmann::json::parse(decryptSave(tmp));
	}
	catch (nlohmann::detail::parse_error & e) {
		if (_fileSave.is_open())
			_fileSave.close();
		_fileSave.open("save.json", std::fstream::out | std::fstream::trunc);
		_fileSave << encryptSave(_defaultSave.dump()) << std::endl;
		std::cout << "save.json was unusable or unexistant and now has default informations." << std::endl;
		_fileSave.close();
	}

	_fileSave.close();
	initGameSave();
}

t_gameInputs SceneGame::getGameInputs() const {
	return (_gameInputs);
}

t_gameSave SceneGame::getGameSave() const {
	return (_gameSave);
}

void SceneGame::setGameInputs(t_gameInputs gameInputs) {
	_gameInputs = gameInputs;

	_config["Move up"] = _gameInputs.moveUp;
	_config["Move down"] = _gameInputs.moveDown;
	_config["Move left"] = _gameInputs.moveLeft;
	_config["Move right"] = _gameInputs.moveRight;
	_config["Bomb"] = _gameInputs.bomb;
	_config["Activate"] = _gameInputs.activate;
	_config["Music volume"] = _gameInputs.musicVolume;
	_config["Effects volume"] = _gameInputs.effectVolume;
	_config["Screen width"] = _gameInputs.screenWidth;
	_config["Screen height"] = _gameInputs.screenHeight;
	_config["Full screen"] = _gameInputs.fullScreen;

	_fileConfig.open("config.json", std::fstream::out);
	_fileConfig << _config.dump(4) << std::endl;
	_fileConfig.close();
}

void SceneGame::setGameSave() {
	_save["Seed"] = _gameSave.seed;
	_save["Level"] = _gameSave.level;
	_save["Speed"] = _gameSave.player.speed;
	_save["MaxBombs"] = _gameSave.player.maxBombs;
	_save["BombPow"] = _gameSave.player.bombPow;
	_save["DrillBomb"] = _gameSave.player.drillBomb;
	_save["BombCtrl"] = _gameSave.player.bombCtrl;
	_save["Score"] = _gameSave.score;

	_fileSave.open("save.json", std::fstream::out);
	_fileSave << encryptSave(_save.dump()) << std::endl;
	_fileSave.close();
}

void SceneGame::initGameInputs() {
	std::string verif;

	try {
		verif = _config["Move up"];
		if (verif.length() == 1)
			_gameInputs.moveUp = verif;
		else
			_gameInputs.moveUp = "W";

		verif = _config["Move down"];
		if (verif.length() == 1)
			_gameInputs.moveDown = verif;
		else
			_gameInputs.moveDown = "S";

		verif = _config["Move left"];
		if (verif.length() == 1)
			_gameInputs.moveLeft = verif;
		else
			_gameInputs.moveLeft = "A";

		verif = _config["Move right"];
		if (verif.length() == 1)
			_gameInputs.moveRight = verif;
		else
			_gameInputs.moveRight = "D";

		verif = _config["Bomb"];
		if (verif.length() == 1)
			_gameInputs.bomb = verif;
		else
			_gameInputs.bomb = " ";

		verif = _config["Activate"];
		if (verif.length() == 1)
			_gameInputs.activate = verif;
		else
			_gameInputs.activate = "E";

		_gameInputs.musicVolume = audioManager.setMusicVolume(_config["Music volume"]);
		_gameInputs.effectVolume = audioManager.setEffectVolume(_config["Effects volume"]);
		_gameInputs.screenWidth = _config["Screen width"];
		_gameInputs.screenHeight = _config["Screen height"];
		_gameInputs.fullScreen = _config["Full screen"];
	}
	catch (nlohmann::json::exception & e) {
		_fileConfig.open("config.json", std::fstream::out | std::fstream::trunc);
		_fileConfig << std::setw(4) << _defaultConfig << std::endl;
		_fileConfig.close();
		std::cout << "config.json was unusable and now has default configs." << std::endl;
		loadGameConfigs();
	}
}

void SceneGame::initGameSave() {
	try {
		_gameSave.seed = (long)_save["Seed"];
		_gameSave.level = (int)_save["Level"];
		_gameSave.player.speed = (int)_save["Speed"];
		_gameSave.player.maxBombs = (int)_save["MaxBombs"];
		_gameSave.player.bombPow = (int)_save["BombPow"];
		_gameSave.player.drillBomb = (bool)_save["DrillBomb"];
		_gameSave.player.bombCtrl = (bool)_save["BombCtrl"];
		_gameSave.score = (int)_save["Score"];

		if (_gameSave.seed > -1)
			_gameSave.player.saved = true;
		else
			_gameSave.player.saved = false;
	}
	catch (nlohmann::json::exception & e) {
		_fileSave.open("save.json", std::fstream::out | std::fstream::trunc);
		_fileSave << encryptSave(_defaultSave.dump()) << std::endl;
		_fileSave.close();
		std::cout << "save.json was unusable and now has default infos. " << std::endl;
		loadGameSave();
	}
}

Map	*SceneGame::getMap() {
	return (_map);
}

void	SceneGame::nextLevel() {
	if (_map && _map->getPlayer(0) && _level % BOSSRATIO)
		_savPlayer = _map->getPlayer(0)->toStruct();

	_level++;
	client.sendToServ(NEXTLVL, _level);
	audioManager.stopSounds();
	audioManager.playRandomMusic();
}

void	SceneGame::renderBackground() {
	if (client.stopped || !getMap())
	{
		glClear(GL_DEPTH_BUFFER_BIT);
		_background.bind();
		core.getRenderer().renderTexture(Vec4(-1, -1, 2, 2), Vec4(1));
	}
}

void	SceneGame::resetGameTime() {
	_resetTimer = true;
}

void    SceneGame::loadMap(size_t nbPlayers, size_t nbIAs, eMapName name) {
	Character	*player = NULL;

	if (_savPlayer.saved)
		player = new Character(_savPlayer, true);
	
	resetGameTime();
	if (name == RANDSOLO)
		_level = nbIAs;
	else if (name == RANDBOSS)
		_level = nbIAs * BOSSRATIO;
	setMap(MapGenerator::genMap(nbPlayers, nbIAs, name, player, true));
	client.sendToServ(LOADED);
}

int		SceneGame::getLevel() {
	return _level;
}

std::string	SceneGame::encryptSave(std::string src) {
	size_t		len = src.length();
	size_t		klen = _key.length();

	for (size_t i = 0; i < len; i++) {
		src[i] = src[i] + _key[i % klen];
	}

	return (src);
}

std::string	SceneGame::decryptSave(std::string src) {
	size_t		len = src.length();
	size_t		klen = _key.length();

	for (size_t i = 0; i < len; i++) {
		src[i] = src[i] - _key[i % klen];
	}

	return (src);
}

void	SceneGame::loadPlayer(t_player player) {
	_savPlayer = player;
	_savPlayer.saved = true;
}

void	SceneGame::unloadPlayer() {
	_savPlayer.saved = false;
}
