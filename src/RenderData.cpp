#include <iostream>
#include <cmath>
#include "RenderData.hpp"

RenderData::RenderData(const Mat4x4 &model) : _model(model) {}
RenderData::RenderData(const Vec3 &v) : RenderData(
	Mat4x4::Translate(v)) {}
RenderData::RenderData(const Vec2 &v) : RenderData(Vec3(v[0], 0, v[1])) {}
RenderData::RenderData(const AMapObject &obj) : RenderData(obj.getPos()) {}

RenderDataCharacter::RenderDataCharacter(const Mat4x4 &model, float speed) : RenderData(model),
	_speed(speed) {}
RenderDataCharacter::RenderDataCharacter(const Vec2 &pos, const Vec2 &dir, float speed) :
	RenderDataCharacter(
		Mat4x4::Translate(Vec3(pos[0] + 0.5f, 0, pos[1] + 0.5f)) *
			Mat4x4::Identity().rotate_y(std::atan2(-dir[0], dir[1])),
		speed) {}
RenderDataCharacter::RenderDataCharacter(const Character &ch) :
	RenderDataCharacter(ch.getPos(), ch.getDir(), ch.getSpeed()) {}
