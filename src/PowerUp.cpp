#include "PowerUp.hpp"
#include "Map.hpp"
#include "Character.hpp"
#include <sys/time.h>

PowerUp::PowerUp() {
	_pos = Vec2(-1, -1);
	_effect = NOEFFECT;
	_value = 0;
	_time = -1;
	if (_time > 0) {
		_ltt = new timeval;
		_ntt = new timeval;
		gettimeofday(_ltt, NULL);
	}
	_tab_pos = -1;
	_parent = NULL;
}

PowerUp::PowerUp(const Vec2 pos, Effect eff, int value, int ttime, Character *parent) {
	_pos = pos;
	_effect = eff;
	_value = value;
	_time = ttime;
	if (_time > 0) {
		_ltt = new timeval;
		_ntt = new timeval;
		gettimeofday(_ltt, NULL);
	} else {
		_ltt = NULL;
		_ntt = NULL;
	}
	_tab_pos = -1;
	_parent = parent;
}

PowerUp::PowerUp(PowerUp const& src) {
	*this = src;
}

PowerUp::~PowerUp() {
	if (_ltt)
		delete _ltt;
	if (_ntt)
		delete _ntt;
}

PowerUp& PowerUp::operator=(PowerUp const& rhs) {
	(void)rhs;
	return (*this);
}

bool		PowerUp::exploded(Map *map, bool drill, Character *owner) {

	(void)map;
	(void)drill;
	(void)owner;

	return false;
}

bool	PowerUp::isCollide() {
	return (false);
}

void	PowerUp::update(Map *map) {
	(void)map;
}

void PowerUp::updateTime() {
	if (_time > 0) {
		gettimeofday(_ntt, NULL);
		double df = static_cast<double>((_ntt->tv_sec + _ntt->tv_usec / SECOND) - (_ltt->tv_sec + _ltt->tv_usec / SECOND));
		if (df > (double)_time) {
			_time = -1;
			if (_parent != NULL && _tab_pos != -1) {
				_parent->removePow(_tab_pos);
				delete(this);
			}
		}
	}
}

int		PowerUp::getTp() {
	return _tab_pos;
}

void	PowerUp::setTp(int pos) {
	_tab_pos = pos;
}
void	PowerUp::tpMinus() {
	_tab_pos--;
	if (_tab_pos < 0) {
		std::cout << "tpminus ERRORRRR there might be something horribly wrong with your code." << std::endl;
		_tab_pos = 0;
	}
}

Effect		PowerUp::getEffect() {
	return _effect;
}

int		PowerUp::getValue() {
	return _value;
}

Character 	*PowerUp::getParent() {
	return _parent;
}

void		PowerUp::setParent(Character *parent) {
	_parent = parent;
}

OBJTYPE			PowerUp::getObjType() const {
    return      POWERUP;
}
