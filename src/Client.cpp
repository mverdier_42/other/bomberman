// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Client.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: fperruch <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/03/12 17:21:43 by fperruch          #+#    #+#             //
//   Updated: 2018/10/08 14:49:52 by fperruch         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <Client.hpp>
#include "SceneGame.hpp"

int		Client::port = -1;
bool	Client::error = false;
bool	Client::stopped = false;
bool	Client::mapChanged = false;
long	Client::timeRand;

Client::Client() {
	_socket = 0;
	_sG = NULL;
	_playerIndex = 0;
	_threadIdServ = NULL;
	_threadIdCli = NULL;
	_sG = SceneGame::instance;
	_serv = NULL;
	_serverReady = false;
}

Client::Client(Client const& src) {
	*this = src;
}

Client::~Client() {
	disconnect();
	if (_serv)
		delete (_serv);
}

Client& Client::operator=(Client const& rhs) {
	_socket = rhs._socket;
	_sG = rhs._sG;
	return (*this);
}

void	Client::setMap(const t_seMsg &msg) {
	if (msg.cmd == SETMAP && msg.val >= 0 && _sG)
		_sG->loadMap(msg.nbPlayers, msg.nbIAs, static_cast<eMapName>(msg.val));
}


void	Client::setPlayer(const t_seMsg &msg) {
	if (msg.cmd == SETPLAYER && msg.val >= 0)
		setPlayerId(msg.playerId);
}

void	Client::syncPos(const t_seMsg &msg) {
	Character	*player = NULL;
	Vec2		dir;
	Vec2		pos;
	Map			*map;

	if (msg.cmd == POSITION) {
		if (_sG && (map = _sG->getMap())) {
			if (msg.playerId >= IAINDEXDEC) {
				IA *ia = map->getIA(msg.playerId - IAINDEXDEC);
				if (ia)
					player = ia->getChara();
			}
			else
				player = map->getPlayer(msg.playerId);
			if (player && msg.playerId != _playerIndex) {
				player->setPosition(msg.pos, msg.dir, msg.val);
			}
		}
	}
}

void	Client::plActions(t_seMsg &msg) {
	Character	*player = NULL;
	Map			*map = NULL;

	if (_sG && (map = _sG->getMap())) {
		if (msg.playerId >= IAINDEXDEC) {
			IA *ia = map->getIA(msg.playerId - IAINDEXDEC);
			if (ia)
				player = ia->getChara();
		}
		else
			player = map->getPlayer(msg.playerId);
		if (player) {
			if (msg.cmd == PUTBOMB && msg.playerId != _playerIndex) 
				player->putBombAt(msg.pos, map);
			if (msg.cmd == TRIGBOMB && msg.playerId != _playerIndex)
				player->detonate(_sG->getMap());
			if (msg.cmd == DIE && !player->isDead()) {
				player->exploded(map, 0, NULL);
			}
			if (msg.cmd == SCORE && msg.playerId == _playerIndex) {
				int	score = player->getScore();
				if (score < msg.val) {
					player->setScore(msg.val);
					score = msg.val;
				}
			}
		}
	}
}

void    Client::clientCommLoop() {
	t_seMsg	msg;
	time_t	t;
	bool	serverInactiv;
	bool	connected = false;

	bzero(&msg, sizeof(t_seMsg));
	t = time(NULL);
	while (read(_socket, &msg, sizeof(t_seMsg)) >= 0) {
		if (msg.cmd == START)
			startGame();
		if (msg.cmd == TIMERAND)
            timeRand = msg.val;
		if (msg.cmd == SERVRDY)
			_serverReady = true;
		setMap(msg);
		setPlayer(msg);
		syncPos(msg);
		plActions(msg);
		if (msg.cmd == NBPLAYERS) {
			_nbPlayersConn = msg.val;
			_nbPlayers = msg.nbPlayers;
			bzero(&msg, sizeof(t_seMsg));
			continue ;
		}
		if (msg.cmd == STOP || ((serverInactiv = time(NULL) - t > TIMEOUTVAL) && connected)) {
			if (serverInactiv)
				std::cout << "Server connexion lost" << std::endl;
			SceneGame::instance->client.disconnect();
			break ;
		}
		t = time(NULL);
		bzero(&msg, sizeof(t_seMsg));
		connected = true;
	}
}

void	Client::sendToServ(eCmd cmd, int val, Vec2 pos, Vec2 dir) {
	t_seMsg	msg;

	if (_socket > 0) {
		msg.cmd = cmd;
		msg.val = val;
		msg.pos = pos;
		msg.dir = dir;
		msg.playerId = _playerIndex;
		sendToServ(msg);
	}
}

void    Client::sendToServ(const t_seMsg &msg) {
	if (_socket > 0)
		write(_socket, &msg, sizeof(msg));
}

void	Client::connectClient(const std::string &ip, time_t timeoutStart) {
	t_sockaddr	addr;

    memset(&addr, '\0', sizeof(addr));
    addr.sin_addr.s_addr = inet_addr(ip.c_str());
    addr.sin_port = htons(port);
    addr.sin_family = AF_INET;
	if (timeoutStart < 0)
		timeoutStart = time(NULL);
	_status = WAITING;
	if (connect(_socket, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
		if (time(NULL) - timeoutStart > TIMEOUTVAL) {
			std::cerr << "client connection timeout : " << strerror(errno) << std::endl;
			_status = TIMEOUT;
            throw (std::exception());
        }
		sleep(1);
		connectClient(ip, timeoutStart);
    }
	_status = CONNECTED;
}

void    Client::run(std::string ip) {
	stopped = false;
	_serverReady = false;
	if (!_sG)
		_sG = SceneGame::instance;
	try {
		if ((_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
			std::cerr << "client: fail to get socket > " << strerror(errno) << std::endl;
			throw (std::exception());
		}
		while (port < 0)
			usleep(10);
		connectClient(ip, -1);
	}
	catch (std::exception e) {
		std::cerr << e.what() << std::endl;
		error = true;
		return ;
	}
	_pause = true;
	clientCommLoop();
	stopped = true;
}

void    *clientThread(void *data) {
	t_game  *game = NULL;

	if (data) {
		game = (t_game*)data;
		SceneGame::instance->client.run(game->ip);
	}
	pthread_detach(pthread_self());
	pthread_exit(NULL);
}

void	*serverThread(void *data) {
	t_game	*game = NULL;
	Server	*serv = NULL;

	if (data) {
		game = (t_game*)data;
		serv = game->serv;
		if (serv)
			serv->run(game->nbPlayer, game->nbIAs, game->map, game->player, game->seed);
	}
	pthread_detach(pthread_self());
	pthread_exit(NULL);
}

void	Client::newGame(size_t nbPlayer, size_t nbIAs, eMapName map, t_player player, long seed) {
	error = false;
	if (_socket > 0)
		disconnect();
	player.saved = true;
	SceneGame::instance->loadPlayer(player);
	_game.nbPlayer = nbPlayer;
	_game.nbIAs = nbIAs;
	_game.map = map;
	_game.seed = seed;
	_game.player = player;
	_game.ip = "127.0.0.1";
	if (!_serv)
		_serv = new Server();
	_game.serv = _serv;
	pthread_create(&_threadIdCli, NULL, &clientThread, &_game);
	pthread_create(&_threadIdServ, NULL, &serverThread, &_game);
}

void	Client::newGame(size_t nbPlayer, size_t nbIAs, eMapName map) {
	t_player	player;

	error = false;
	if (_socket > 0)
		disconnect();
	SceneGame::instance->unloadPlayer();
	player.saved = false;
	_game.nbPlayer = nbPlayer;
	_game.nbIAs = nbIAs;
	_game.map = map;
	_game.seed = -1;
	_game.player = player;
	_game.ip = "127.0.0.1";
	if (!_serv)
		_serv = new Server();
	_game.serv = _serv;
	pthread_create(&_threadIdCli, NULL, &clientThread, &_game);
	pthread_create(&_threadIdServ, NULL, &serverThread, &_game);
}

void	Client::joinGame(std::string ip, int daPort) {
	error = false;
	_game.ip = ip;
	port = daPort;
	pthread_create(&_threadIdCli, NULL, &clientThread, &_game);
}

void	Client::disconnect() {
	sendToServ(QUIT, 0);
	stopped = true;
	if (_serv)
		_serv->stop();
	close(_socket);
    _socket = -1;
	port = -1;
	_playerIndex = 0;
	bzero(&_game, sizeof(t_game));
	_pause = false;
	_nbPlayersConn = 0;
	_nbPlayers = 0;
	_status = IDLE;
	sleep(1);
}

void	Client::startGame() {
	_pause = false;
}

void	Client::setPlayerId(int id) {
	if (id < 0)
		id = 0;
	_playerIndex = id;
}

int		Client::getPlayerId() {
	return (_playerIndex);
}

int		Client::getNbPlayersConn() {
	return (_nbPlayersConn);
}

int		Client::getNbPlayers() {
	return (_nbPlayers);
}

eClientStatus	Client::getStatus() {
	return (_status);
}

bool	Client::serverIsReady() const {
	return (_serverReady);
}

void	Client::setServerReady() {
	_serverReady = true;
}
