#include <string>
#include "SceneKeyboard.hpp"
#include "Core.hpp"
#include "SceneMainMenu.hpp"
#include "SceneOptions.hpp"
#include "SceneGame.hpp"

static void exitCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = core->getSceneManager();
		Scene *sceOptions = new SceneOptions(*core);
		Scene *sceKeyboard = man.getScene("keyboard");
		man.removeScene(sceKeyboard);
		man.addScene(sceOptions);
		man.setActiveScene(sceOptions);
		delete sceKeyboard;
	}
}

SceneKeyboard::SceneKeyboard(Core &core) {
	_gameInputs = SceneGame::instance->getGameInputs();

	_menu.addModule(new MenuButton("Up : " + _gameInputs.moveUp));
	_menu.addModule(new MenuButton("Down : " + _gameInputs.moveDown));
	_menu.addModule(new MenuButton("Left : " + _gameInputs.moveLeft));
	_menu.addModule(new MenuButton("Right : " + _gameInputs.moveRight));
	_menu.addModule(new MenuButton("Bomb : " + _gameInputs.bomb));
	_menu.addModule(new MenuButton("Activate : " + _gameInputs.activate));
	_menu.addModule(new MenuButton("Back to Options", exitCallback, &core));
}

SceneKeyboard::~SceneKeyboard() {
	SceneGame::instance->setGameInputs(_gameInputs);
}

void SceneKeyboard::eventChar(Core &core, unsigned int key) {
	IMenuModule	*module;
	MenuButton	*button;
	std::string str;
	std::string keystr;

	(void)core;

	if (key < 32 || key > 126)
		return;

	if (key >= 97 && key <= 122)
		key -= 32;

	keystr = std::string(1, key);

	module = _menu.getActiveModule();
	if ((button = dynamic_cast<MenuButton*>(module))) {
		str = button->getText();
		if (!str.compare(0, 2, "Up") && !checkDuplicates(keystr, "Up"))
			_gameInputs.moveUp = keystr;
		else if (!str.compare(0, 4, "Down") && !checkDuplicates(keystr, "Down"))
			_gameInputs.moveDown = keystr;
		else if (!str.compare(0, 4, "Left") && !checkDuplicates(keystr, "Left"))
			_gameInputs.moveLeft = keystr;
		else if (!str.compare(0, 5, "Right") && !checkDuplicates(keystr, "Right"))
			_gameInputs.moveRight = keystr;
		else if (!str.compare(0, 4, "Bomb") && !checkDuplicates(keystr, "Bomb"))
			_gameInputs.bomb = keystr;
		else if (!str.compare(0, 8, "Activate") && !checkDuplicates(keystr, "Activate"))
			_gameInputs.activate = keystr;
		button->setText(str.replace(str.length() - 1, 1, keystr));
	}
}

void SceneKeyboard::eventResize(Core &core, int width, int height) {
	_menu.resize(core);
	(void)width;
	(void)height;
}

void SceneKeyboard::eventKeyPress(Core &core, int key, int mods) {
	(void)mods;
	if (key == GLFW_KEY_ESCAPE)
	{
		SceneManager &man = core.getSceneManager();
		Scene *sceOptions = new SceneOptions(core);
		Scene *sceKeyboard = man.getScene("keyboard");
		man.removeScene(sceKeyboard);
		man.addScene(sceOptions);
		man.setActiveScene(sceOptions);
		delete sceKeyboard;
	}

	_menu.keyPress(core, key);
}

void SceneKeyboard::eventButtonPress(Core &core, int key, int mods) {
	(void)mods;
	if (key == GLFW_MOUSE_BUTTON_LEFT)
	{
		const Window &win = core.getWindow();
		_menu.buttonPress(core, Vec2(
			(win.getMouseX() / win.getWidth() - 0.5) * 2,
			(win.getMouseY() / win.getHeight() - 0.5) * -2));
	}
}

void SceneKeyboard::update(Core &core) {
	(void)core;
	_menu.update(core);
}

void SceneKeyboard::render(Core &core) {
	SceneGame::instance->renderBackground();
	glClear(GL_DEPTH_BUFFER_BIT);
	_menu.render(core);
}

bool SceneKeyboard::checkDuplicates(std::string keystr, std::string module) {
	if (module.compare("Up") && _gameInputs.moveUp == keystr)
		swapKeys(module, "Up", keystr);
	else if (module.compare("Down") && _gameInputs.moveDown == keystr)
		swapKeys(module, "Down", keystr);
	else if (module.compare("Left") && _gameInputs.moveLeft == keystr)
		swapKeys(module, "Left", keystr);
	else if (module.compare("Right") && _gameInputs.moveRight == keystr)
		swapKeys(module, "Right", keystr);
	else if (module.compare("Bomb") && _gameInputs.bomb == keystr)
		swapKeys(module, "Bomb", keystr);
	else if (module.compare("Activate") && _gameInputs.activate == keystr)
		swapKeys(module, "Activate", keystr);
	else
		return (false);
	return (true);
}

void SceneKeyboard::swapKeys(std::string src, std::string dup, std::string keystr) {
	std::string keyTmp;

	if (!src.compare("Up")) {
		keyTmp = _gameInputs.moveUp;
		_gameInputs.moveUp = keystr;
	}
	if (!src.compare("Down")) {
		keyTmp = _gameInputs.moveDown;
		_gameInputs.moveDown = keystr;
	}
	if (!src.compare("Left")) {
		keyTmp = _gameInputs.moveLeft;
		_gameInputs.moveLeft = keystr;
	}
	if (!src.compare("Right")) {
		keyTmp = _gameInputs.moveRight;
		_gameInputs.moveRight = keystr;
	}
	if (!src.compare("Bomb")) {
		keyTmp = _gameInputs.bomb;
		_gameInputs.bomb = keystr;
	}
	if (!src.compare("Activate")) {
		keyTmp = _gameInputs.activate;
		_gameInputs.activate = keystr;
	}

	if (!dup.compare("Up"))
		_gameInputs.moveUp = keyTmp;
	if (!dup.compare("Down"))
		_gameInputs.moveDown = keyTmp;
	if (!dup.compare("Left"))
		_gameInputs.moveLeft = keyTmp;
	if (!dup.compare("Right"))
		_gameInputs.moveRight = keyTmp;
	if (!dup.compare("Bomb"))
		_gameInputs.bomb = keyTmp;
	if (!dup.compare("Activate"))
		_gameInputs.activate = keyTmp;

	updateText(dup, keyTmp);
}

void SceneKeyboard::updateText(std::string module, std::string keystr) {
	std::vector<IMenuModule*> *modules = _menu.getModules();
	for (size_t i = 0; i < modules->size(); i++) {
		MenuButton	*button;
		std::string	str;

		if ((button = dynamic_cast<MenuButton*>((*modules)[i])) && !button->getText().compare(0, module.length(), module)) {
			str = button->getText();
			button->setText(str.replace(str.length() - 1, 1, keystr));
		}
	}
}

const std::string &SceneKeyboard::getName() {
	static const std::string name("keyboard");
	return (name);
}
