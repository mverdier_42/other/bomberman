#include "Bomb.hpp"
#include "Map.hpp"
#include "SceneGame.hpp"
#include <sys/time.h>

Bomb::Bomb() {
	_owner = NULL;
	_pos = Vec2(-1, -1);
	_radius = 3;
	_hp = 120;
	_canTick = true;
	_drillBomb = false;
	_controlBomb = false;
	_client = false;
	_tab_pos = -1;
		_ltt = new timeval;
		_ntt = new timeval;
		gettimeofday(_ltt, NULL);
}

Bomb::Bomb(Character *owner, const Vec2 pos, PowerUp *poww, int radius, bool canTick, bool drillbomb, bool controlbomb) {
	_owner = owner;
	_pos = pos;
	_radius = radius;
	_hp = 120;
	_canTick = canTick;
	_drillBomb = drillbomb;
	_controlBomb = controlbomb;
	_tab_pos = -1;
	_pow = poww;
	_client = owner->isClient();
	_ltt = new timeval;
	_ntt = new timeval;
	gettimeofday(_ltt, NULL);
}

Bomb::Bomb(Bomb const& src) {
	*this = src;
}

Bomb::~Bomb() {
	if (_pow != NULL)
		delete(_pow);
	delete _ltt;
	delete _ntt;
}

Bomb& Bomb::operator=(Bomb const& rhs) {
	(void)rhs;
	return (*this);
}

bool 	Bomb::exploded(Map *map, bool drill, Character *owner) {

	(void)owner;
	if (_owner) {
		_owner->lessBomb();
		_owner->removeBomb(_tab_pos);
	}

	if (_client)
		SceneGame::instance->audioManager.playSound(EXP);
	
	if (_pow)
		map->putObject(_pow);
	else
		map->removeObject(this);

	_pow = NULL;
	delete(this);
	try {
		map->explode(Vec2i(_pos), _radius, _drillBomb, _owner);
	}
	catch (std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	return !drill;
}

bool	Bomb::isCollide() {
	return (true);
}

void	Bomb::update(Map *map) {
	if (_canTick == true) {
		gettimeofday(_ntt, NULL);
		double df = static_cast<double>( (_ntt->tv_sec + _ntt->tv_usec / SECOND) - (_ltt->tv_sec + _ltt->tv_usec / SECOND) );
		if (df > 2.0) {
			exploded(map, _drillBomb, NULL);
		}
	}
}

OBJTYPE			Bomb::getObjType() const {
    return      BOMB;
}

bool	Bomb::getControl() const{
	return _controlBomb;
}

bool	Bomb::getDrill() const{
	return _drillBomb;
}
void	Bomb::setDrill(bool drill) {
	_drillBomb = drill;
}

int		Bomb::getTp() {
	return _tab_pos;
}

void	Bomb::setTp(int pos) {
	_tab_pos = pos;
}

void	Bomb::tpMinus() {
	_tab_pos--;
	if (_tab_pos < 0) {
		std::cout << "tpMinus:ERROR: there might be something horribly wrong with your code." << std::endl;
		_tab_pos = 0;
	}
}
void	Bomb::setPlayer(Character *player) {
	_owner = player;
}

int		Bomb::getRadius() const {
	return (_radius);
}

void	Bomb::setPow(PowerUp *poww) {
	_pow = poww;
}

PowerUp *Bomb::getPow(void) {
	return _pow;
}
