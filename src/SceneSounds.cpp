#include <string>
#include "SceneSounds.hpp"
#include "Core.hpp"
#include "SceneMainMenu.hpp"
#include "SceneOptions.hpp"
#include "SceneGame.hpp"

static void exitCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = core->getSceneManager();
		Scene *sceOptions = new SceneOptions(*core);
		Scene *sceSounds = man.getScene("sounds");
		man.removeScene(sceSounds);
		man.addScene(sceOptions);
		man.setActiveScene(sceOptions);
		delete sceSounds;
	}
}

SceneSounds::SceneSounds(Core &core) {
	_gameInputs = SceneGame::instance->getGameInputs();

	_menu.addModule(new MenuButton("Music volume : " + std::to_string(_gameInputs.musicVolume) + "%"));
	_menu.addModule(new MenuButton("Effects volume : " + std::to_string(_gameInputs.effectVolume) + "%"));
	_menu.addModule(new MenuButton("Back to Options", exitCallback, &core));
}

SceneSounds::~SceneSounds() {
	SceneGame::instance->setGameInputs(_gameInputs);
}

void SceneSounds::eventChar(Core &core, unsigned int key) {
	(void)core;
	(void)key;
}

void SceneSounds::eventResize(Core &core, int width, int height) {
	_menu.resize(core);
	(void)width;
	(void)height;
}

void SceneSounds::eventKeyPress(Core &core, int key, int mods) {
	(void)mods;
	if (key == GLFW_KEY_ESCAPE)
	{
		SceneManager &man = core.getSceneManager();
		Scene *sceOptions = new SceneOptions(core);
		Scene *sceSounds = man.getScene("sounds");
		man.removeScene(sceSounds);
		man.addScene(sceOptions);
		man.setActiveScene(sceOptions);
		delete sceSounds;
	}

	if (key == GLFW_KEY_RIGHT) {
		IMenuModule	*module;
		MenuButton	*button;
		std::string str;

		module = _menu.getActiveModule();
		if ((button = dynamic_cast<MenuButton*>(module))) {
			str = button->getText();
			if (!str.compare(0, 5, "Music")) {
				_gameInputs.musicVolume = SceneGame::instance->audioManager.musicVolumeUp();
				button->setText(str.replace(str.find(":") + 2, std::string::npos, std::to_string(_gameInputs.musicVolume) + "%"));
			}
			else if (!str.compare(0, 7, "Effects")) {
				_gameInputs.effectVolume = SceneGame::instance->audioManager.effectVolumeUp();
				button->setText(str.replace(str.find(":") + 2, std::string::npos, std::to_string(_gameInputs.effectVolume) + "%"));
			}
		}
	}

	if (key == GLFW_KEY_LEFT) {
		IMenuModule	*module;
		MenuButton	*button;
		std::string str;

		module = _menu.getActiveModule();
		if ((button = dynamic_cast<MenuButton*>(module))) {
			str = button->getText();
			if (!str.compare(0, 5, "Music")) {
				_gameInputs.musicVolume = SceneGame::instance->audioManager.musicVolumeDown();
				button->setText(str.replace(str.find(":") + 2, std::string::npos, std::to_string(_gameInputs.musicVolume) + "%"));
			}
			if (!str.compare(0, 7, "Effects")) {
				_gameInputs.effectVolume = SceneGame::instance->audioManager.effectVolumeDown();
				button->setText(str.replace(str.find(":") + 2, std::string::npos, std::to_string(_gameInputs.effectVolume) + "%"));
			}
		}
	}

	_menu.keyPress(core, key);
}

void SceneSounds::eventButtonPress(Core &core, int key, int mods) {
	(void)mods;
	if (key == GLFW_MOUSE_BUTTON_LEFT)
	{
		const Window &win = core.getWindow();
		_menu.buttonPress(core, Vec2(
			(win.getMouseX() / win.getWidth() - 0.5) * 2,
			(win.getMouseY() / win.getHeight() - 0.5) * -2));
	}
}

void SceneSounds::update(Core &core) {
	(void)core;
	_menu.update(core);
}

void SceneSounds::render(Core &core) {
	SceneGame::instance->renderBackground();
	glClear(GL_DEPTH_BUFFER_BIT);
	_menu.render(core);
}

const std::string &SceneSounds::getName() {
	static const std::string name("sounds");
	return (name);
}
