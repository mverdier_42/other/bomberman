// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   IA.cpp                                             :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: fperruch <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/05/08 18:16:09 by fperruch          #+#    #+#             //
//   Updated: 2018/06/25 18:05:01 by fperruch         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "IA.hpp"
#include <Character.hpp>
#include <Bomb.hpp>

void	IA::init() {
	_dirs.push_back(Vec2(1, 0));
	_dirs.push_back(Vec2(0, 1));
	_dirs.push_back(Vec2(0, -1));
	_dirs.push_back(Vec2(-1, 0));
	_goTo = Vec2(-1, -1);
	_goSafe = false;
	_putBomb = false;
	_pause = false;
	_closeToPl = false;
	_oldPos = Vec2(0, 0);
}

IA::IA() {
	_chara = NULL;
	_nbMov = 0;
	_dir = Vec2(1, 0);
	_type = GHOSTLIKE;
	init();
}

IA::IA(Character *chara, eIAType type) {
	_chara = NULL;
	_chara = chara;
	if (_chara)
		_chara->setIA(type);
	_nbMov = 0;
	_dir = Vec2(1, 0);
	_type = type;
	init();
}

IA::IA(const IA& src) {
	*this = src;
}

IA::~IA() {
	if (_chara)
		delete (_chara);
	_chara = NULL;
}

IA& IA::operator=(const IA& rhs) {
	_chara = rhs._chara;
	_type = rhs._type;
	_dir = rhs._dir;
	_dirs = rhs._dirs;
	return (*this);
}

float	getDist(Vec2 v1, Vec2 v2) {
	return (sqrt(pow(v1[0] - v2[0], 2) + pow(v1[1] - v2[1], 2)));
}

t_tile	fillTile(Vec2 pos, float dFrom, float dTo) {
	t_tile	tile;

	tile.pos = pos;
	tile.dFrom = dFrom;
	tile.dTo = dTo;
	tile.val = dFrom + dTo;
	return (tile);
}

void	addToVecSort(std::vector<t_tile> &list, t_tile tile) {
	if (!list.size()) {
		list.push_back(tile);
		return ;
	}
	for (size_t i = 0; i < list.size(); i++) {
		if (list[i].val > tile.val) {
			list.insert(list.begin() + i, tile);
			return ;
		}
	}
	list.push_back(tile);
}

void	printVec(const std::vector<t_tile> &list) {
	for (size_t i = 0; i < list.size(); i++)
		std::cout << "pos " << list[i].pos << " dFrom " << list[i].dFrom << " dTo " << list[i].dTo << " val " << list[i].val << std::endl;
}

void    printVec(const std::vector<Vec2> &list) {
	if (!list.size())
		std::cout << "==EMPTY VEC==" << std::endl;
	for (size_t i = 0; i < list.size(); i++)
		std::cout << list[i] << std::endl;
}

bool    vecContains(const std::vector<t_tile> &list, const Vec2 &pos) {
	for (size_t i = 0; i < list.size(); i++) {
		if (list[i].pos == pos)
			return (true);
	}
	return (false);
}

bool    isSafe(const Vec2 &pos, Map *map, std::vector<Bomb*> &bombs) {
    AMapObject          *obj = NULL;
    Vec2                bpos;
    int                 range;

    if (!map)
        return (true);
    if (!bombs.size())
        for (int x = 0; x < map->getSize().x; x++) {
            for (int y = 0; y < map->getSize().y; y++) {
                obj = map->getObj(x, y);
                if (obj && obj->getObjType() == BOMB)
                    bombs.push_back(dynamic_cast<Bomb*>(obj));
            }
        }
    if (bombs.size() > 0) {
        for (size_t i = 0; i < bombs.size(); i++) {
            if (bombs[i]) {
                bpos = bombs[i]->getPos();
                range = bombs[i]->getRadius() - 1;
                if ((pos[0] >= bpos[0] - range && pos[0] <= bpos[0] + range && pos[1] == bpos[1]) || (pos[1] >= bpos[1] - range && pos[1] <= bpos[1] + range && pos[0] == bpos[0]))
                    return (false);
            }
        }
    }
    return (true);
}

bool	IA::isAdj(Vec2 orig, Vec2 check) {
	Vec2	diff = orig - check;

	for (size_t i = 0; i < _dirs.size(); i++)
		if (diff == _dirs[i])
			return (true);
	return (false);
}

t_tile	getTile(const std::vector<t_tile> &list, Vec2 pos) {
	for (size_t i = 0; i < list.size(); i++)
		if (list[i].pos == pos)
			return (list[i]);
	return (t_tile());
}

std::vector<t_tile>	IA::getPath(const std::vector<t_tile> &list, Vec2 start, Vec2 end) {
	std::vector<t_tile>	path;
	float				curDFrom = 0;
	Vec2				lastPos;
	t_tile				tmp;

	tmp = getTile(list, end);
	path.push_back(tmp);
	curDFrom = tmp.dFrom - 1;
	lastPos = tmp.pos;
	if (curDFrom <= 0)
		return (path);
	for (size_t i = list.size() - 1; (int)i >= 0; i--) {
		if (list[i].dFrom == curDFrom && isAdj(lastPos, list[i].pos)) {
			path.insert(path.begin(), list[i]);
			curDFrom--;
			lastPos = list[i].pos;
		}
		if (list[i].pos == start)
			break ;
	}
	return (path);
}

Vec2	getClosest(const std::vector<t_tile> &list) {
	float	dToMin = 0;
	Vec2	out;

	for (size_t i = 0; i < list.size(); i++)
		if (list[i].dTo > dToMin)
			dToMin = list[i].dTo;
	for (size_t i = 0; i < list.size(); i++)
		if (list[i].dTo < dToMin) {
			dToMin = list[i].dTo;
			out = list[i].pos;
		}
	return (out);
}

Vec2	IA::searchPath(Map *map, Vec2 from, Vec2 to) {
	std::vector<t_tile>	open;
	std::vector<t_tile>	close;
	t_tile				current;
	t_tile				tmp;
	Vec2				pos;
	AMapObject			*obj = NULL;
	std::vector<Bomb*>	bombs;

	if (!map)
		return (Vec2(0, 0));
	current = fillTile(from, 0, getDist(from, to));
	addToVecSort(open, current);
	while (open.size() > 0) {
		current = open.front();
		addToVecSort(close, current);
		open.erase(open.begin());
		if (isAdj(current.pos, to)) {
			tmp = fillTile(to, current.dFrom + 1, getDist(current.pos, to));
            addToVecSort(close, tmp);
			break ;
		}
		if (current.pos == to)
			break ;
		pos = current.pos + Vec2(1, 0);
		if ((obj = map->getObj(pos)) && !obj->isCollide() && !vecContains(open, pos) && !vecContains(close, pos)) {
			tmp = fillTile(pos, current.dFrom + 1, getDist(pos, to));
			addToVecSort(open, tmp);
		}
		pos = current.pos + Vec2(-1, 0);
		if ((obj = map->getObj(pos)) && !obj->isCollide() && !vecContains(open, pos) && !vecContains(close, pos)) {
			tmp = fillTile(pos, current.dFrom + 1, getDist(pos, to));
			addToVecSort(open, tmp);
		}
		pos = current.pos + Vec2(0, 1);
		if ((obj = map->getObj(pos)) && !obj->isCollide() && !vecContains(open, pos) && !vecContains(close, pos)) {
			tmp = fillTile(pos, current.dFrom + 1, getDist(pos, to));
			addToVecSort(open, tmp);
		}
		pos = current.pos + Vec2(0, -1);
		if ((obj = map->getObj(pos)) && !obj->isCollide() && !vecContains(open, pos) && !vecContains(close, pos)) {
			tmp = fillTile(pos, current.dFrom + 1, getDist(pos, to));
			addToVecSort(open, tmp);
		}
	}
	if (!vecContains(close, to) && !_goSafe && !_putBomb) {
		_putBomb = true;
		_goTo = getClosest(close);
		return (_chara ? _chara->getDir() : Vec2());
	}
	close = getPath(close, from, to);
	if (close.size() > 1)
		return (close[1].pos - close[0].pos);
	Vec2 dir = to - from;
	return (dir);
}

void	IA::addMsg(eCmd cmd, int val, Vec2 pos) {
	t_seMsg	msg;

	msg.cmd = cmd;
	msg.val = val;
	msg.pos = pos;
	_msgs.push_back(msg);
}

Vec2	IA::getSafePlace(Map *map, std::vector<Bomb*> &bombs, Vec2 pos) {
	std::vector<t_tile>	way;
	Vec2				vec;
	AMapObject			*obj = NULL;
	Vec2				bpos;

	if (map) {
		way.push_back(fillTile(pos, 0, 0));
		for (size_t i = 0; i < way.size(); i++) {
			vec = way[i].pos + _dirs[0];
			if ((obj = map->getObj(vec)) && !obj->isCollide() && !vecContains(way, vec))
				addToVecSort(way, fillTile(vec, way[i].dFrom + 1, 0));
			vec = way[i].pos + _dirs[1];
			if ((obj = map->getObj(vec)) && !obj->isCollide() && !vecContains(way, vec))
				addToVecSort(way, fillTile(vec, way[i].dFrom + 1, 0));
			vec = way[i].pos + _dirs[2];
			if ((obj = map->getObj(vec)) && !obj->isCollide() && !vecContains(way, vec))
				addToVecSort(way, fillTile(vec, way[i].dFrom + 1, 0));
			vec = way[i].pos + _dirs[3];
			if ((obj = map->getObj(vec)) && !obj->isCollide() && !vecContains(way, vec))
				addToVecSort(way, fillTile(vec, way[i].dFrom + 1, 0));
			for (size_t j = 0; j < way.size(); j++) {
				if (isSafe(way[j].pos, map, bombs))
					return (way[j].pos);
			}
		}
	}
	return (pos);
}

Vec2    IA::getSafePlace(Map *map, Vec2 &danger, Vec2 pos) {
	std::vector<t_tile> way;
    Vec2                vec;
    AMapObject          *obj = NULL;
    Vec2                bpos;

    if (map) {
        way.push_back(fillTile(pos, 0, 0));
        for (size_t i = 0; i < way.size(); i++) {
            vec = way[i].pos + _dirs[0];
            if ((obj = map->getObj(vec)) && !obj->isCollide() && !vecContains(way, vec))
                addToVecSort(way, fillTile(vec, way[i].dFrom + 1, 0));
            vec = way[i].pos + _dirs[1];
            if ((obj = map->getObj(vec)) && !obj->isCollide() && !vecContains(way, vec))
                addToVecSort(way, fillTile(vec, way[i].dFrom + 1, 0));
            vec = way[i].pos + _dirs[2];
            if ((obj = map->getObj(vec)) && !obj->isCollide() && !vecContains(way, vec))
                addToVecSort(way, fillTile(vec, way[i].dFrom + 1, 0));
            vec = way[i].pos + _dirs[3];
            if ((obj = map->getObj(vec)) && !obj->isCollide() && !vecContains(way, vec))
                addToVecSort(way, fillTile(vec, way[i].dFrom + 1, 0));
            for (size_t j = 0; j < way.size(); j++) {
				if (way[j].pos[0] != danger[0] && way[j].pos[1] != danger[1])
                    return (way[j].pos);
            }
        }
    }
    return (pos);
}

void	IA::playerLike(Map *map) {
	std::vector<Character*>	players;
	Vec2	pos;
	Vec2	to;
	Vec2	dir;
	Vec2	truepos;
	float	dist;
	float	tmpdist;
	std::vector<Bomb*>	bombs;

	if (map && _chara) {
		players = map->getPlayers();
		pos = _chara->getPos();
		truepos = pos;
		pos[0] = round(pos[0]);
		pos[1] = round(pos[1]);
		if (!_putBomb && !_goSafe && !_closeToPl) {
			for (size_t i = 0; i < players.size(); i++) {
				tmpdist = getDist(pos, players[i]->getPos());
				if ((!i || tmpdist < dist) && !players[i]->isDead()) {
					dist = tmpdist;
					to = players[i]->getPos();
				}
			}
			if (map->getName() == RANDMULTI) {
				std::vector<IA*>	IAs = map->getIAs();
				for (size_t i = 0; i < IAs.size(); i++) {
					if (IAs[i] != this) {
						tmpdist = getDist(pos, IAs[i]->getPos());
						if (tmpdist < dist && !IAs[i]->isDead()) {
							dist = tmpdist;
							to = IAs[i]->getPos();
						}
					}
				}
			}
			to[0] = round(to[0]);
			to[1] = round(to[1]);
			if (isAdj(pos, to) || pos == to) {
				_closeToPl = true;
				_dangerPos = pos;
				_chara->putBombAt(pos, map);
				addMsg(PUTBOMB, 1, pos);
				getSafePlace(map, pos, pos);
				_goTo[0] = round(_goTo[0]);
				_goTo[1] = round(_goTo[1]);
			}
		}
		else {
			if (_putBomb && pos == _goTo) {
				_putBomb = false;
				_goSafe = true;
				_chara->putBombAt(pos, map);
				addMsg(PUTBOMB, 1, pos);
				_goTo = getSafePlace(map, pos, pos);
			}
			else if (_closeToPl && pos == _goTo) {
				_closeToPl = false;
				_goTo = getSafePlace(map, _dangerPos, pos);
				_goSafe = true;
			}
			else if (_goSafe && pos == _goTo) {
				if (!_chara->getBombs().size())
					_goSafe = false;
				else {
					_chara->detonate(map);
					addMsg(TRIGBOMB, 0, pos);
					if (!isSafe(pos, map, bombs)) {
						_goSafe = true;
						_putBomb = false;
						_closeToPl = false;
						_goTo = getSafePlace(map, bombs, pos);
						dir = searchPath(map, pos, to);
						_chara->unmove();
						_chara->move(dir);
						_oldPos = truepos;
					}
				}
				_dangerPos = Vec2(-1, -1);
			}
			to = _goTo;
		}
		to[0] = round(to[0]);
		to[1] = round(to[1]);
		if (pos != to)
			dir = searchPath(map, pos, to);
		else
			dir = to - truepos;
		_chara->unmove();
		_chara->move(dir);
		_oldPos = truepos;
	}
}

std::vector<Vec2>	IA::getEmpties(Map *map) {
	Vec2				pos;
	std::vector<Vec2>	dirs;
	AMapObject			*obj = NULL;

	if (map && _chara) {
		pos = _chara->getPos();
		pos[0] = round(pos[0]);
		pos[1] = round(pos[1]);
		for (size_t i = 0; i < _dirs.size(); i++) {
			if ((obj = map->getObj(pos[0] + _dirs[i][0], pos[1] + _dirs[i][1])) && !obj->isCollide())
				dirs.push_back(_dirs[i]);
		}
	}
	return (dirs);
}

void	IA::ghostLike(Map *map) {
	Vec2				pos;
	Vec2				truepos;
	std::vector<Vec2>	dirs;
	AMapObject			*obj = NULL;
	AMapObject          *objOn = NULL;
	float				delta = 0.01f;

	if (map && _chara) {
		obj = NULL;
		objOn = NULL;
		if (!getEmpties(map).size()) {
			_chara->unmove();
			return ;
		}
		pos = _chara->getPos();
		truepos = pos;
		pos[0] = round(pos[0]);
		pos[1] = round(pos[1]);
		if (_oldPos != pos)
			_nbMov--;
		bool	onObj = (objOn = map->getObj(pos)) && objOn->isCollide();
		bool	centerCase = (_dir[0] < 0 && truepos[0] - delta <= pos[0])
			|| (_dir[0] > 0 && truepos[0] + delta >= pos[0])
			|| (_dir[1] < 0 && truepos[1] - delta <= pos[1])
			|| (_dir[1] > 0 && truepos[1] + delta >= pos[1]);
		bool	nextIsObs = (obj = map->getObj(pos[0] + _dir[0], pos[1] + _dir[1])) && obj->isCollide();
		if (onObj)
			_dir = _dir * -1;
		if (centerCase && (!obj || nextIsObs || _nbMov <= 0) && !onObj) {
			dirs = getEmpties(map);
			if (dirs.size() > 0)
				_dir = dirs[rand() % dirs.size()];
			_nbMov--;
			if (_nbMov <= 0)
				_nbMov = rand() % 5 + 1;
		}
		_oldPos = pos;
		_chara->unmove();
		_chara->move(_dir);
	}
}

void	IA::play(Map *map) {
	if (map && _chara && !_chara->isDead() && !_pause) {
		if (_type == PLAYERLIKE)
			playerLike(map);
		else
			ghostLike(map);
	}
}

eIAType IA::getType() const {
	return (_type);
}

std::vector<t_seMsg>	IA::getMsgs() const {
	return (_msgs);
}

void	IA::clearMsgs() {
	_msgs.clear();
}

Character	*IA::getChara() const {
	if (_chara)
		return (_chara);
	return (NULL);
}

void	IA::update(Map *map) {
	if (_chara)
		_chara->update(map);
}

void	IA::pause(bool val) {
	_pause = val;
	if (_chara && _pause) {
		_chara->unmove();
	}
}

bool	IA::isDead() const {
	if (_chara)
		return (_chara->isDead());
	return (true);
}

Vec2	IA::getPos() const {
	if (_chara)
		return (_chara->getPos());
	return (Vec2(-1, -1));
}

int		IA::getHp() const {
	if (_chara)
		return (_chara->getHp());
	return (0);
}
