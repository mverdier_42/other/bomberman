#include "ParticleSystem.hpp"
#include <cmath>

ParticleSystem::ParticleSystem() : _tex(NULL), _prog(NULL), _maxParticle(0) {}

ParticleSystem::ParticleSystem(const Texture *tex, const ProgramGlsl *prog, unsigned int maxParticle) :
	_tex(tex), _prog(prog), _maxParticle(maxParticle) {
	glGenVertexArrays(1, _vao);
	glGenBuffers(1, _vbo);
	if (_maxParticle)
		_particles = new Particle[_maxParticle];
}
ParticleSystem::ParticleSystem(const ParticleSystem& src) :
	ParticleSystem(src.getTexture(), src.getProg(), src.getMaxParticle()) {}

ParticleSystem::~ParticleSystem() {
	if (_maxParticle)
		delete[] _particles;
	if (_prog)
		delete _prog;
	if (_vao[0])
		glDeleteVertexArrays(1, _vao);
	if (_vbo[0])
		glDeleteBuffers(1, _vbo);
}

ParticleSystem& ParticleSystem::operator=(const ParticleSystem& rhs) {
	_tex = rhs.getTexture();
	setMaxParticle(rhs.getMaxParticle());
	return (*this);
}

void ParticleSystem::setMaxParticle(unsigned int maxParticle)
{
	if (!maxParticle)
	{
		if (_maxParticle)
		{
			delete[] _particles;
			_maxParticle = 0;
		}
	}
	else
	{
		Particle *t = new Particle[maxParticle];
		if (_maxParticle)
		{
			unsigned int mm = std::max(_maxParticle, maxParticle);
			for (unsigned int i = 0; i < mm; ++i)
				t[i] = _particles[i];
			delete[] _particles;
		}
		_particles = t;
	}
}
static float getRand()
{
	int m2 = RAND_MAX / 2;
	return (static_cast<float>(std::rand() - m2) / static_cast<float>(m2));
}
static inline Vec3 getVec3Rand()
{
	return (Vec3(getRand(), 0, getRand()));
}
void ParticleSystem::addParticles(const Vec3 &pos, float speed, float lifetime, unsigned int count)
{
	float ang = M_PI / static_cast<float>(count);
	for (unsigned int i = 0; i < count; ++i)
	{
		Vec3 pp = Vec3(cos(ang * i) * 0.2f, 0, sin(ang * i) * 0.2f);
		addParticle(pos + pp, getVec3Rand() * speed, lifetime);
		pp.negate();
		addParticle(pos + pp, getVec3Rand() * speed, lifetime);
	}
}

void ParticleSystem::addParticle(const Vec3 &pos, const Vec3 &dirSpeed, float lifetime)
{
	if (++_lastAdded >= _maxParticle)
		_lastAdded = 0;
	Particle &part = _particles[_lastAdded];
	part.pos = pos;
	part.dir = dirSpeed;
	part.lifetime = lifetime;
}
unsigned int ParticleSystem::getMaxParticle() const
{
	return (_maxParticle);
}
const Texture *ParticleSystem::getTexture() const
{
	return (_tex);
}
const ProgramGlsl *ParticleSystem::getProg() const
{
	return (_prog);
}
void ParticleSystem::update(float delta) {
	for (unsigned int i = 0; i < _maxParticle; ++i)
	{
		if (_particles[i].lifetime > 0)
		{
			_particles[i].pos += _particles[i].dir * delta;
			_particles[i].dir *= 0.95f;
			_particles[i].lifetime -= delta;
		}
	}
	glBindVertexArray(_vao[0]);
	glBindBuffer(GL_ARRAY_BUFFER, _vbo[0]);
	glBufferData(GL_ARRAY_BUFFER,
		(GLsizeiptr)(sizeof(float) * 7 * _maxParticle),
		_particles, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 28, 0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 28, (void*)12);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 28, (void*)24);
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}
void ParticleSystem::render(const Mat4x4 &projection, const Mat4x4 &cam)
{
	//uni_tex
	_prog->bind();
	if (_tex)
		_tex->bind();
	glUniformMatrix4fv(_prog->getUniformLocation("uni_projection"), 1, GL_FALSE, projection);	
	glUniformMatrix4fv(_prog->getUniformLocation("uni_modelView"), 1, GL_FALSE, cam);
	glDepthMask(false);
	glBindVertexArray(_vao[0]);
	glDrawArrays(GL_POINTS, 0, _maxParticle);
	glBindVertexArray(0);
	glDepthMask(true);
}

