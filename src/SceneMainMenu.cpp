/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SceneMainMenu.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrouthie <jrouthie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/30 01:50:57 by jrouthie          #+#    #+#             */
/*   Updated: 2018/10/13 15:52:38 by jrouthie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cmath>
#include "Core.hpp"
#include "SceneMainMenu.hpp"
#include "SceneGame.hpp"
#include "SceneOptions.hpp"
#include "SceneLoad.hpp"
#include "SceneHost.hpp"
#include "SceneJoin.hpp"
#include "MapGenerator.hpp"
#include <Client.hpp>

static void resumeGameCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = core->getSceneManager();
		Scene *sceMain = man.getScene("main");
		Scene *sceGame = man.getScene("game");
		man.removeScene(sceMain);
		if (!sceGame) {
			sceGame = SceneGame::instance;
			man.addScene(sceGame);
		}
		man.setActiveScene(sceGame);
		delete sceMain;
	}
}

static void buttonNewGameCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = core->getSceneManager();
		Scene *sceMain = man.getScene("main");
		Scene *sceGame = man.getScene("game");

		SceneGame::instance->client.newGame(1, 1, RANDSOLO);
		while (!SceneGame::instance->getMap()) {
			if (SceneGame::instance->client.error)
				return ;
			else
				usleep(10);
		}
		if (!sceGame) {
			sceGame = SceneGame::instance;
			man.addScene(sceGame);
		}
		static_cast<SceneGame*>(sceGame)->audioManager.stopSounds();
		static_cast<SceneGame*>(sceGame)->audioManager.playRandomMusic();
		man.setActiveScene(sceGame);
		man.removeScene(sceMain);
		delete sceMain;
	}
}

static void saveGameCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		SceneGame::instance->saveGame();
/*		t_gameSave gameSave = SceneGame::instance->getGameSave();
		gameSave.seed = SceneGame::instance->client.serv->getRandSeed();
		SceneGame::instance->setGameSave(gameSave);*/
	}
}

static void loadCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = core->getSceneManager();
		Scene *sceMain = man.getScene("main");
		man.removeScene(sceMain);
		Scene *sceLoad = new SceneLoad(*core);
		man.addScene(sceLoad);
		man.setActiveScene(sceLoad);
		delete sceMain;
	}
}

static void buttonJoinGameCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
    (void)mod;
    (void)key;
    (void)userdata;
    if (key == MenuKeyEnter) {
        Core *core = reinterpret_cast<Core*>(userdata);
        SceneManager &man = core->getSceneManager();
        Scene *sceMain = man.getScene("main");
		Scene *sceJoin = new SceneJoin(*core);

		man.addScene(sceJoin);
		man.setActiveScene(sceJoin);
		man.removeScene(sceMain);
		delete sceMain;
    }
}

static void buttonHostGameCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
    (void)mod;
    (void)key;
    (void)userdata;
    if (key == MenuKeyEnter) {
        Core *core = reinterpret_cast<Core*>(userdata);
        SceneManager &man = core->getSceneManager();
        Scene *sceMain = man.getScene("main");
		Scene *sceHost = new SceneHost(*core);
		man.addScene(sceHost);
		man.setActiveScene(sceHost);
		man.removeScene(sceMain);
		delete sceMain;
    }
}

static void optionsCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = core->getSceneManager();
		Scene *sceMain = man.getScene("main");
		man.removeScene(sceMain);
		Scene *sceOptions = new SceneOptions(*core);
		man.addScene(sceOptions);
		man.setActiveScene(sceOptions);
		delete sceMain;
	}
}

static void backToMenuCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;

	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = core->getSceneManager();
		Scene *sceGame = man.getScene("game");
		Scene *sceMain = man.getScene("main");

		SceneGame::instance->client.disconnect();
		if (sceGame)
			man.removeScene(sceGame);

		// Refresh the menu / Go back to Main menu.
		man.removeScene(sceMain);
		delete sceMain;
		sceMain = new SceneMainMenu(*core);
		man.addScene(sceMain);
		man.setActiveScene(sceMain);
		static_cast<SceneGame*>(sceGame)->audioManager.stopSounds();
		static_cast<SceneGame*>(sceGame)->audioManager.playMusic(MAIN);

		SceneGame::instance->setMap(NULL);
	}
}

static void exitCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;

	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);

		SceneGame::instance->client.disconnect();
		core->getWindow().close();
	}
}

SceneMainMenu::SceneMainMenu(Core &core) : 
	_gameRun(!SceneGame::instance->client.stopped && SceneGame::instance->getMap())
{
	if (!_gameRun) {
		_menu.addModule(new MenuButton("New Game", buttonNewGameCallback, &core));
		_menu.addModule(new MenuButton("Load Game", loadCallback, &core));
		_menu.addModule(new MenuButton("Join Game", buttonJoinGameCallback, &core));
		_menu.addModule(new MenuButton("Host Game", buttonHostGameCallback, &core));
		_menu.addModule(new MenuButton("Options", optionsCallback, &core));
		_menu.addModule(new MenuButton("Exit Game", exitCallback, &core));
	}
	else {
		_menu.addModule(new MenuButton("Resume Game", resumeGameCallback, &core));
		if (SceneGame::instance->getMap()->getName() != RANDMULTI) {
			_menu.addModule(new MenuButton("New Game", buttonNewGameCallback, &core));
			_menu.addModule(new MenuButton("Save Game", saveGameCallback, &core));
		}
		_menu.addModule(new MenuButton("Options", optionsCallback, &core));
		_menu.addModule(new MenuButton("Back to Menu", backToMenuCallback, &core));
		_menu.addModule(new MenuButton("Exit Game", exitCallback, &core));
	}
}

SceneMainMenu::~SceneMainMenu() {
}

void SceneMainMenu::eventResize(Core &core, int width, int height) {
	_menu.resize(core);
	(void)width;
	(void)height;
}

void SceneMainMenu::eventKeyPress(Core &core, int key, int mods) {
	(void)mods;
	if (key == GLFW_KEY_ESCAPE) {
		SceneManager &man = core.getSceneManager();
		Scene *sceMain = man.getScene("main");
		Scene *sceGame = man.getScene("game");
		if (SceneGame::instance->getMap()) {
			man.removeScene(sceMain);
			if (!sceGame) {
				sceGame = SceneGame::instance;
				man.addScene(sceGame);
			}
			man.setActiveScene(sceGame);
			delete sceMain;
		}
	}
	_menu.keyPress(core, key);
}

void SceneMainMenu::eventButtonPress(Core &core, int key, int mods) {
	(void)mods;
	if (key == GLFW_MOUSE_BUTTON_LEFT) {
		const Window &win = core.getWindow();
		_menu.buttonPress(core, Vec2(
			(win.getMouseX() / win.getWidth() - 0.5) * 2,
			(win.getMouseY() / win.getHeight() - 0.5) * -2));
	}
}

void SceneMainMenu::update(Core &core) {
	(void)core;
	_menu.update(core);
}

void SceneMainMenu::render(Core &core) {
	SceneGame::instance->renderBackground();
	glClear(GL_DEPTH_BUFFER_BIT);
	_menu.render(core);
}

const std::string &SceneMainMenu::getName() {
	static const std::string name("main");
	return (name);
}
