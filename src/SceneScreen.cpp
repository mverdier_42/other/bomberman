#include <string>
#include "SceneScreen.hpp"
#include "Core.hpp"
#include "Window.hpp"
#include "SceneMainMenu.hpp"
#include "SceneOptions.hpp"
#include "SceneGame.hpp"

t_gameInputs	SceneScreen::gameInputs;

static void exitCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = core->getSceneManager();
		Scene *sceOptions = new SceneOptions(*core);
		Scene *sceScreen = man.getScene("screen");
		man.removeScene(sceScreen);
		man.addScene(sceOptions);
		man.setActiveScene(sceOptions);
		delete sceScreen;
	}
}
static void resolutionCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	Core *core = reinterpret_cast<Core*>(userdata);
	Window &win = core->getWindow();
	MenuButton &but = reinterpret_cast<MenuButton&>(mod);
	int width;
	int height;
	{
		const std::string &txt = but.getText();
		std::string num = txt.substr(13);
		width = std::atoi(num.c_str());
		num = num.substr(num.find('x') + 1);
		height = std::atoi(num.c_str());
	}
	if (key == MenuKeyEnter) {
		win.setWindowOption(width, height, win.isFullscreen());
		SceneScreen::gameInputs.screenWidth = width;
		SceneScreen::gameInputs.screenHeight = height;
	}
	else if (key == MenuKeyLeft || key == MenuKeyRight)
	{
		int count;
		const GLFWvidmode *modes = win.getVideoModes(count);
		int ok = 0;
		for (int i = 0; i < count; ++i)
		{
			if (modes[i].width == width && modes[i].height == height)
			{
				if (key == MenuKeyLeft)
					ok = (i ? i - 1 : 0);
				else if (key == MenuKeyRight)
					ok = (i == count - 1 ? count - 1 : i + 1);
				break;
			}
		}
		width = modes[ok].width;
		height = modes[ok].height;
		but.setText("Resolution : " + std::to_string(width) + "x" + std::to_string(height));
	}
}
static void fullscreenCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	Core *core = reinterpret_cast<Core*>(userdata);
	Window &win = core->getWindow();
	if (key == MenuKeyEnter) {
		win.setWindowOption(win.getWidth(), win.getHeight(), !win.isFullscreen());
	}
}

SceneScreen::SceneScreen(Core &core) {
	Window &win = core.getWindow();
	
	gameInputs = SceneGame::instance->getGameInputs();

	_menu.addModule(new MenuButton("Resolution : " + std::to_string(win.getWidth()) + "x" + std::to_string(win.getHeight()), resolutionCallback, &core));
	_menu.addModule(new MenuButton(win.isFullscreen() ? "Windowed" : "Fullscreen", fullscreenCallback, &core));
	_menu.addModule(new MenuButton("Back to Options", exitCallback, &core));
}

SceneScreen::~SceneScreen() {
	SceneGame::instance->setGameInputs(gameInputs);
}

void SceneScreen::eventChar(Core &core, unsigned int key) {
	(void)core;
	(void)key;
}

void SceneScreen::eventResize(Core &core, int width, int height) {
	_menu.resize(core);
	(void)width;
	(void)height;
}

void SceneScreen::eventKeyPress(Core &core, int key, int mods) {
	(void)mods;
	if (key == GLFW_KEY_ESCAPE)
	{
		SceneManager &man = core.getSceneManager();
		Scene *sceOptions = new SceneOptions(core);
		Scene *sceScreen = man.getScene("screen");
		man.removeScene(sceScreen);
		man.addScene(sceOptions);
		man.setActiveScene(sceOptions);
		delete sceScreen;
	}

	_menu.keyPress(core, key);
}

void SceneScreen::eventButtonPress(Core &core, int key, int mods) {
	(void)mods;
	if (key == GLFW_MOUSE_BUTTON_LEFT)
	{
		const Window &win = core.getWindow();
		_menu.buttonPress(core, Vec2(
			(win.getMouseX() / win.getWidth() - 0.5) * 2,
			(win.getMouseY() / win.getHeight() - 0.5) * -2));
	}
}

void SceneScreen::update(Core &core) {
	(void)core;
	_menu.update(core);
}

void SceneScreen::render(Core &core) {
	SceneGame::instance->renderBackground();
	glClear(GL_DEPTH_BUFFER_BIT);
	_menu.render(core);
}

const std::string &SceneScreen::getName() {
	static const std::string name("screen");
	return (name);
}