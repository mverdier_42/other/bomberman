#include <string>
#include "SceneLoad.hpp"
#include "Core.hpp"
#include "SceneMainMenu.hpp"
#include "SceneGame.hpp"

t_gameSave SceneLoad::gameSave;

static void loadGameCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = core->getSceneManager();
		Scene *sceLoad = man.getScene("load");
		Scene *sceGame = man.getScene("game");
		t_gameSave gameSave = SceneLoad::gameSave;
		SceneGame::instance->client.newGame(1, gameSave.level, RANDSOLO, gameSave.player, gameSave.seed);
		while (!SceneGame::instance->getMap())
			if (SceneGame::instance->client.error)
				return ;
		SceneGame::instance->getMap()->getPlayer(0)->setScore(gameSave.score);
		if (!sceGame) {
			man.addScene(SceneGame::instance);
			sceGame = man.getScene("game");
		}

		man.setActiveScene(sceGame);
		man.removeScene(sceLoad);
		delete sceLoad;
	}
}

static void exitCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = core->getSceneManager();
		Scene *sceMain = new SceneMainMenu(*core);
		Scene *sceLoad = man.getScene("load");
		man.removeScene(sceLoad);
		man.addScene(sceMain);
		man.setActiveScene(sceMain);
		delete sceLoad;
	}
}

SceneLoad::SceneLoad(Core &core) {
	gameSave = SceneGame::instance->getGameSave();

	if (gameSave.seed > -1)
		_menu.addModule(new MenuButton("Load last Save", loadGameCallback, &core));
	else
		_menu.addModule(new MenuButton("No game to load"));
	_menu.addModule(new MenuButton("Back to Menu", exitCallback, &core));
}

SceneLoad::~SceneLoad() {
}

void SceneLoad::eventChar(Core &core, unsigned int key) {
	(void)core;
	(void)key;
}

void SceneLoad::eventResize(Core &core, int width, int height) {
	_menu.resize(core);
	(void)width;
	(void)height;
}

void SceneLoad::eventKeyPress(Core &core, int key, int mods) {
	(void)mods;
	if (key == GLFW_KEY_ESCAPE)
	{
		SceneManager &man = core.getSceneManager();
		Scene *sceMain = new SceneMainMenu(core);
		Scene *sceLoad = man.getScene("load");
		man.removeScene(sceLoad);
		man.addScene(sceMain);
		man.setActiveScene(sceMain);
		delete sceLoad;
	}

	_menu.keyPress(core, key);
}

void SceneLoad::eventButtonPress(Core &core, int key, int mods) {
	(void)mods;
	if (key == GLFW_MOUSE_BUTTON_LEFT)
	{
		const Window &win = core.getWindow();
		_menu.buttonPress(core, Vec2(
			(win.getMouseX() / win.getWidth() - 0.5) * 2,
			(win.getMouseY() / win.getHeight() - 0.5) * -2));
	}
}

void SceneLoad::update(Core &core) {
	(void)core;
	_menu.update(core);
}

void SceneLoad::render(Core &core) {
	SceneGame::instance->renderBackground();
	glClear(GL_DEPTH_BUFFER_BIT);
	_menu.render(core);
}

const std::string &SceneLoad::getName() {
	static const std::string name("load");
	return (name);
}

