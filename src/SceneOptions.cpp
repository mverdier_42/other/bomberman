#include "SceneOptions.hpp"
#include "Core.hpp"
#include "SceneMainMenu.hpp"
#include "SceneKeyboard.hpp"
#include "SceneSounds.hpp"
#include "SceneScreen.hpp"

static void screenCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = core->getSceneManager();
		Scene *sceOptions = man.getScene("options");
		Scene *sceScreen = new SceneScreen(*core);
		man.removeScene(sceOptions);
		man.addScene(sceScreen);
		man.setActiveScene(sceScreen);
		delete sceOptions;
	}
}

static void keyboardCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = core->getSceneManager();
		Scene *sceOptions = man.getScene("options");
		Scene *sceKeyboard = new SceneKeyboard(*core);
		man.removeScene(sceOptions);
		man.addScene(sceKeyboard);
		man.setActiveScene(sceKeyboard);
		delete sceOptions;
	}
}

static void soundsCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = core->getSceneManager();
		Scene *sceOptions = man.getScene("options");
		Scene *sceSounds = new SceneSounds(*core);
		man.removeScene(sceOptions);
		man.addScene(sceSounds);
		man.setActiveScene(sceSounds);
		delete sceOptions;
	}
}

static void exitCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = (*core).getSceneManager();
		Scene *sceOptions = man.getScene("options");
		Scene *sceMain = new SceneMainMenu(*core);
		man.removeScene(sceOptions);
		man.addScene(sceMain);
		man.setActiveScene(sceMain);
		delete sceOptions;
	}
}

SceneOptions::SceneOptions(Core &core) {
	(void)core;
	_menu.addModule(new MenuButton("Keyboard", keyboardCallback, &core));
	_menu.addModule(new MenuButton("Sounds", soundsCallback, &core));
	_menu.addModule(new MenuButton("Screen", screenCallback, &core));
	_menu.addModule(new MenuButton("Back to Menu", exitCallback, &core));
}

SceneOptions::~SceneOptions() {
}

void SceneOptions::eventResize(Core &core, int width, int height) {
	_menu.resize(core);
	(void)width;
	(void)height;
}

void SceneOptions::eventKeyPress(Core &core, int key, int mods) {
	(void)mods;
	if (key == GLFW_KEY_ESCAPE)
	{
		SceneManager &man = core.getSceneManager();
		Scene *sceOptions = man.getScene("options");
		Scene *sceMain = new SceneMainMenu(core);
		man.removeScene(sceOptions);
		man.addScene(sceMain);
		man.setActiveScene(sceMain);
		delete sceOptions;
	}

	_menu.keyPress(core, key);
}

void SceneOptions::eventButtonPress(Core &core, int key, int mods) {
	(void)mods;
	if (key == GLFW_MOUSE_BUTTON_LEFT)
	{
		const Window &win = core.getWindow();
		_menu.buttonPress(core, Vec2(
			(win.getMouseX() / win.getWidth() - 0.5) * 2,
			(win.getMouseY() / win.getHeight() - 0.5) * -2));
	}
}

void SceneOptions::update(Core &core) {
	(void)core;
	_menu.update(core);
}

void SceneOptions::render(Core &core) {
	SceneGame::instance->renderBackground();
	glClear(GL_DEPTH_BUFFER_BIT);
	_menu.render(core);
}

const std::string &SceneOptions::getName() {
	static const std::string name("options");
	return (name);
}
