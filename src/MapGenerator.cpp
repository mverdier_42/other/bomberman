#include <iostream>
#include "MapGenerator.hpp"
#include "Map.hpp"
#include "Bomb.hpp"
#include "PowerUp.hpp"
#include <IA.hpp>
#include <SceneGame.hpp>

namespace MapGenerator {

	void	initMapGenerator() {
		maps.push_back(&genRandomSolo);
		maps.push_back(&genRandomMulti);
		maps.push_back(&genRandomBoss);
	}

	void	genSolidWalls(Map *map, Vec2 size) {
		if (map) {
			for (int i = 0; i < size[0]; i++) {
				for (int j = 0; j < size[1]; j++) {
					if (i % 2 && j % 2)
						map->putObject(new Wall(Vec2(i, j), -1));
				}
			}
		}
	}

	Map		*genWalls(Map *map, Vec2 size, int nbPow, Character *currChar, bool isBoss = false) {
		int		x;
		int		y;
		PowerUp	*power;
		int		nbWalls;
		bool	exit = false;
		int 	max = 0;

		if (!map)
			return (NULL);
		map->putObject(new Wall(Vec2(0, size[1] - 3), 1));
		map->putObject(new Wall(Vec2(2, size[1] - 1), 1));
		if (isBoss) {
			map->putObject(new Wall(Vec2(size[0] - 1, size[1] - 3), 1));
			map->putObject(new Wall(Vec2(size[0] - 3, size[1] - 1), 1));
			map->putObject(new Wall(Vec2(size[0] - 3, 0), 1));
			map->putObject(new Wall(Vec2(size[0] - 1, 2), 1));
			map->putObject(new Wall(Vec2(0, 2), 1));
			map->putObject(new Wall(Vec2(2, 0), 1));
		}
		nbWalls = (size[0] * size[1]) / 3;
		int rc = 0;
		int rseed = 100;
		if (currChar) {
			if (currChar->hasDrill() && currChar->hasControl())
				rseed = 85;
			else if (currChar->hasDrill()) {
				rseed = 95;
				rc = 5;
			} else if (currChar->hasControl()) {
				rseed = 95;
			}
		}
		Effect eff;
		while (nbWalls >= 0 && max < 9999) {
			power = NULL;
			x = rand() % (int)(size[0]);
			y = rand() % (int)(size[1]);
			max++;
			if ((!x && y == size[1] - 1) || (!x && y == size[1] - 2) || (x == 1 && y == size[1] - 1) ||
				(isBoss && !x && !y) || (isBoss && !x && y == 1) || (isBoss && x == 1 && !y) ||
				(isBoss && x == size[0] - 1 && !y) || (isBoss && x == size[0] - 2 && !y) || (isBoss && x == size[0] - 1 && y == 1) ||
				(isBoss && x == size[0] - 1 && y == size[1] - 1) || (isBoss && x == size[0] - 2 && y == size[1] - 1) ||
				(isBoss && x == size[0] - 1 && y == size[1] - 2))
				continue ;
			if (nbWalls - nbPow < 0) {
				int rad = (rand() % rseed) - rc;

				if (rad < 20)
					eff = SPEED;
				else if (rad >= 20 && rad < 55)
					eff= BNB;
				else if (rad >= 55 && rad < 90)
					eff = BPOW;
				else if (rad >= 90 && rad < 95)
					eff = DBOMB;
				else
					eff = CBOMB;
				power = new PowerUp(Vec2(x, y), eff, 1);
			}
			if (map->getObj(x, y) && map->getObj(x, y)->isEmpty()) {
				max = 0;
				if (!exit) {
					if (power) {
						delete(power);
						power = NULL;
					}
					power = new PowerUp(Vec2(x, y), EXIT, 1);
					exit = true;
				}
				map->putObject(new Wall(Vec2(x, y), 1, power));
				nbWalls--;
			}
			else if (power) {
				delete(power);
				power = NULL;
			}
		}
		if (max == 9999)
			std::cout << "max" << std::endl;
		return (map);
	}

	Map		*genIAs(Map * map, size_t nbIAs, Vec2 size, bool client) {
		int 				x;
		int 				y;
		std::vector<Vec2>	IAs;
		bool    			posOk;

		if (!map)
			return (NULL);
		while (nbIAs) {
			posOk = true;
			x = rand() % (int)(size[0]);
			y = rand() % (int)(size[1]);
			if ((!x && y == size[1] - 1) || (!x && y == size[1] - 2) || (x == 1 && y == size[1] - 1))
				continue ;
			if (map->getObj(x, y) && map->getObj(x, y)->isEmpty()) {
				for (size_t i = 0; i < IAs.size(); i++)
					if (IAs[i][0] == x && IAs[i][1] == y) {
						posOk = false;
						break ;
					}
				if (!posOk)
					continue ;
				map->addIA(new IA(new Character(Vec2(x, y), std::string("ia ") + std::to_string(nbIAs), 2, client), GHOSTLIKE));
				nbIAs--;
				IAs.push_back(Vec2(x, y));
			}
		}
		return (map);
	}

	void	printMap(Map *map) {
		std::vector<IA*>	IAs = map->getIAs();
		AMapObject			*obj;
		bool				placeia;

		for (int y = 0; y < map->getSize().y; y++) {
			std::cout << std::endl;
			for (int x = 0; x < map->getSize().x; x++) {
				placeia = false;
				for (size_t i = 0; i < IAs.size(); i++) {
					if (Vec2i(IAs[i]->getPos()) == Vec2(x, y)) {

						obj = map->getObj(x, y);
						if (obj && obj->getObjType() == WALL)
							std::cout << "\033[32m ! \033[0m";
						else
							std::cout << "\033[32m I \033[0m";
						placeia = true;
					}
				}
				if (placeia)
					continue ;
				obj = map->getObj(x, y);
				if (obj && obj->getObjType() == WALL)
					std::cout << "\033[31m # \033[0m";
				else if (obj && obj->getObjType() == POWERUP)
					std::cout << "\033[33m P \033[0m";
				else
					std::cout << " 0 ";
			}
		}
		std::cout << std::endl;
	}

	Map		*genRandomSolo(size_t nbPlayers, size_t nbIAs, Character *player, bool client) {
		(void)nbPlayers;
		(void)nbIAs;

		int		intSize = nbIAs % 2 ? nbIAs + 4 : nbIAs + 5;
		Vec2	size = Vec2(intSize + 2, intSize);
		Map		*map = new Map(size, RANDSOLO);

		if (map) {
			srand(Client::timeRand);
			genSolidWalls(map, size);
			if (player) {
				map->addPlayer(player);
				player->setPosition(Vec2(0, size[1] - 1));
			}
			else {
				map->addPlayer(new Character(Vec2(0, size[1] - 1), std::string("player 1"), 5, client));
			}
			map = genWalls(map, size, 1, player);
			map = genIAs(map, nbIAs, size, client);
			//	printMap(map);
		}
		return (map);
	}


	Map		*genRandomBoss(size_t nbPlayers, size_t nbIAs, Character *player, bool client) {
		(void)nbPlayers;
		(void)nbIAs;

		int		intSize = nbIAs % 2 ? nbIAs + 6 : nbIAs + 7;
		Vec2	size = Vec2(intSize + 2, intSize);
		Map		*map = new Map(size, RANDBOSS);
		if (map) {
			srand(Client::timeRand);
			genSolidWalls(map, size);
			if (player) {
				map->addPlayer(player);
				player->setPosition(Vec2(0, size[1] - 1));
			}
			else {
				map->addPlayer(new Character(Vec2(0, size[1] - 1), std::string("player 1"), 5, client));
			}
			map->addIA(new IA(new Character(Vec2(size[0] - 1,  0), std::string("FABIEN OF THE DEATH"), 2, client), PLAYERLIKE));
			if (nbIAs >= 2)
				map->addIA(new IA(new Character(Vec2(0,  0), std::string("TITI'S WRATH"), 2, client), PLAYERLIKE));
			if (nbIAs >= 3)
				map->addIA(new IA(new Character(Vec2(size[0] - 1,  size[1] - 1), std::string("MAXIME THE CORRUPTED HOLY WARRIOR"), 2, client), PLAYERLIKE));
			map = genWalls(map, size, 3 * nbIAs, player, true);
		//	printMap(map);
		}
		return (map);
	}


	Map		*genWallsMulti(Map *map, Vec2 size) {
		int		x;
		int		y;
		PowerUp	*power;
		int		nbWalls;
		int 	max = 0;

		if (!map)
			return (NULL);
		map->putObject(new Wall(Vec2(0, size[1] - 3), 1));
		map->putObject(new Wall(Vec2(2, size[1] - 1), 1));
		map->putObject(new Wall(Vec2(size[0] - 1, size[1] - 3), 1));
		map->putObject(new Wall(Vec2(size[0] - 3, size[1] - 1), 1));
		map->putObject(new Wall(Vec2(size[0] - 3, 0), 1));
		map->putObject(new Wall(Vec2(size[0] - 1, 2), 1));
		map->putObject(new Wall(Vec2(0, 2), 1));
		map->putObject(new Wall(Vec2(2, 0), 1));
		nbWalls = (size[0] * size[1]) / 3;
		while (nbWalls >= 0 && max < 9999) {
			power = NULL;
			x = rand() % (int)(size[0]);
			y = rand() % (int)(size[1]);
			if ((!x && y == size[1] - 1) || (!x && y == size[1] - 2) || (x == 1 && y == size[1] - 1) ||
				(!x && !y) || (!x && y == 1) || (x == 1 && !y) ||
				(x == size[0] - 1 && !y) || (x == size[0] - 2 && !y) || (x == size[0] - 1 && y == 1) ||
				(x == size[0] - 1 && y == size[1] - 1) || (x == size[0] - 2 && y == size[1] - 1) ||
				(x == size[0] - 1 && y == size[1] - 2)) {
				max++;
				continue ;
			}
			if (rand() % 3 != 1) {
				int     rad = (rand() % 100);

				Effect eff;
				if (rad < 20)
					eff = SPEED;
				else if (rad >= 20 && rad < 55)
					eff= BNB;
				else if (rad >= 55 && rad < 85)
					eff = BPOW;
				else if (rad >= 85 && rad < 95)
					eff = DBOMB;
				else
					eff = CBOMB;
				power = new PowerUp(Vec2(x, y), eff/*(rand() % 5) + 1*/, 1);
			}
			if (map->getObj(x, y) && map->getObj(x, y)->isEmpty()) {
				map->putObject(new Wall(Vec2(x, y), 1, power));
				nbWalls--;
				max = 0;
			}
		}
		return (map);
	}

	Map		*genRandomMulti(size_t nbPlayers, size_t nbIAs, Character *player, bool client) {
		(void)nbIAs;
		(void)player;
		int		intSize = (nbIAs + nbPlayers) % 2 ? (nbIAs + nbPlayers) + 6 : (nbIAs + nbPlayers) + 7;
		Vec2	size = Vec2(intSize + 2, intSize);
		std::cout << size << std::endl;
		Map		*map = new Map(size, RANDMULTI);


		if (map) {
			srand(Client::timeRand);
			genSolidWalls(map, size);
			int spawned = 1;

			map->addPlayer(new Character(Vec2(0, 0), std::string("player 1"), 5, client));
			if (nbPlayers > 1) {
				map->addPlayer(new Character(Vec2(size[0] - 1, size[1] - 1), std::string("player 2"), 5, client));
				spawned++;
			}
			if (nbPlayers > 2) {
				map->addPlayer(new Character(Vec2(0, size[1] - 1), std::string("player 3"), 5, client));
				spawned++;
			}
			if (nbPlayers > 3) {
				map->addPlayer(new Character(Vec2(size[0] -1, 0), std::string("player 4"), 5, client));
				spawned++;
			}
			map = genWallsMulti(map, size);
		//	map = genIAs(map, nbIAs, size);
			if (spawned == 1 && nbIAs > 0) {
				map->addIA(new IA(new Character(Vec2(size[0] - 1, size[1] - 1), std::string("ia 1"), 3, client), PLAYERLIKE));
				spawned++;
				nbIAs--;
			}
			if (spawned == 2 && nbIAs > 0) {
				map->addIA(new IA(new Character(Vec2(0, size[1] - 1), std::string("ia " + std::to_string(spawned - nbPlayers + 1)), 3, client), PLAYERLIKE));
				spawned++;
				nbIAs--;
			}
			if (spawned == 3 && nbIAs > 0)
				map->addIA(new IA(new Character(Vec2(size[0] - 1, 0), std::string("ia " + std::to_string(spawned - nbPlayers + 1)), 3, client), PLAYERLIKE));
		}
//		printMap(map);
		return (map);
	}

	Map		*genMap(size_t nbPlayers, size_t nbIAs, int n, Character *player, bool client) {
		Map	*newMap = maps[n](nbPlayers, nbIAs, player, client);

		return (newMap);
	}

	void	basicMulti(Map *map, Vec2 size) {
		if (!map)
			return ;
		genSolidWalls(map, size);
		for (int i = 0; i < size[0]; i++) {
			for (int j = 0; j < size[1]; j++) {
				if ((!i && (j <= 1 || j >= size[1] - 2)) || (!j && (i <= 1 || i	>= size[0] - 2)) || (i == size[0] - 1 && (j <= 1 || j >= size[1] - 2)) || (j == size[1] - 1 && (i <= 1 || i >= size[0] - 2)))
					continue ;
				if (map->getObj(i, j) && map->getObj(i, j)->isEmpty())
					map->putObject(new Wall(Vec2(i, j), 1));
			}
		}
	}

	t_mapDesc	genMapDesc(Map *map) {
		t_mapDesc	out;
		Vec2i		size;
		AMapObject	*obj;
		t_mapObj	tmp;
		PowerUp		*pow;

		if (!map)
			return (out);
		size = map->getSize();
		tmp.pos = Vec2(size[0], size[1]);
		tmp.type = NOTYPE;
		tmp.hp = 0;
		tmp.val = map->getName();
		out.push_back(tmp);
		for (int i = 0; i < size[0]; i++)
			for (int j = 0; j < size[1]; j++) {
				if ((obj = map->getObj(i, j)) && !obj->isEmpty() && obj->getObjType() == WALL) {
					Wall	*wall = dynamic_cast<Wall*>(obj);
					if (wall) {
						tmp.pos = wall->getPos();
						tmp.type = WALL;
						tmp.hp = wall->getHp();
						tmp.val = 0;
						out.push_back(tmp);
						if ((pow = wall->getPow())) {
							tmp.pos = pow->getPos();
							tmp.type = POWERUP;
							tmp.hp = pow->getValue();
							tmp.val = (int)pow->getEffect();
							out.push_back(tmp);
						}
					}
				}
			}
		std::vector<IA*> IAs = map->getIAs();
		for (size_t i = 0; i < IAs.size(); i++) {
			tmp.pos = IAs[i]->getPos();
			tmp.type = IAOBJ;
			tmp.hp = IAs[i]->getHp();
			tmp.val = (int)IAs[i]->getType();
			out.push_back(tmp);
		}
		return (out);
	}

	Map	*genMapFromDesc(t_mapDesc desc) {
		Map	*map = NULL;

		if (desc.size() > 0)
			map = new Map(desc[0].pos, (eMapName)desc[0].val);
		if (!map)
			return (NULL);
		for (size_t i = 1; i < desc.size(); i++) {
			if (desc[i].type == WALL)
				map->putObject(new Wall(desc[i].pos, desc[i].hp));
			if (desc[i].type == POWERUP) {
				AMapObject	*obj = map->getObj(desc[i].pos);
				if (obj && obj->getObjType() == WALL) {
					Wall	*wall = dynamic_cast<Wall*>(obj);
					if (wall)
						wall->setPow(new PowerUp(desc[i].pos, (Effect)desc[i].val, desc[i].hp));
				}
			}
			if (desc[i].type == IAOBJ)
				map->addIA(new IA(new Character(desc[i].pos, "ia"), (eIAType)desc[i].val));
		}
		return (map);
	}
}
