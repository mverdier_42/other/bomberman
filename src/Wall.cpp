#include "Wall.hpp"
#include "Map.hpp"
#include "PowerUp.hpp"
#include "Character.hpp"



Wall::Wall(Vec2 pos, int hp, PowerUp *poww) {
	_pos = pos;
	_hp = hp;
	_pow = poww;
	if (_pow && poww->getPos() != _pos) {
		std::cout << "Error: pow not at the same Vec2 than the wall !" << std::endl;
		delete (_pow);
		_pow = NULL;
	}
}


Wall::Wall(Wall const& src) {
	*this = src;
}

Wall::~Wall() {
	if (_pow != NULL)
		delete(_pow);
}

Wall& Wall::operator=(Wall const& rhs) {
	(void)rhs;
	return (*this);
}

bool	Wall::exploded(Map *map, bool drill, Character *owner) {

	if (_hp > 0) {
		_hp--;
		if (owner != NULL)
			owner->addScore(10);
	}
	if (_hp == 0) {
		if (_pow)
			map->putObject(_pow);
		else
			map->removeObject(this);
		_pow = NULL;
		delete(this);
		return !drill;
	}
	return true;
}

bool	Wall::isCollide() {
	return (true);
}

void	Wall::update(Map *map) {
	(void)map;
}

void 	Wall::setPow(PowerUp *poww) {
	if (poww->getFX() == _pos[0] && poww->getFY() == _pos[1])
		_pow = poww;
	else
		std::cout << "Error: pow not at the same Vec2 than the wall !" << std::endl;
}
PowerUp *Wall::getPow() {
	return _pow;
}


OBJTYPE			Wall::getObjType() const {
    return      WALL;
}
