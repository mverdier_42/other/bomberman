#include "SceneWinSolo.hpp"
#include "Core.hpp"
#include "SceneMainMenu.hpp"
#include "SceneKeyboard.hpp"
#include "SceneSounds.hpp"

int SceneWinSolo::map;

static void backToMenuCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;

	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = core->getSceneManager();
		Scene *sceGame = man.getScene("game");
		Scene *sceWin = man.getScene("win solo");

		SceneGame::instance->client.disconnect();
		if (sceGame)
			man.removeScene(sceGame);

		// Refresh the menu / Go back to Main menu.
		man.removeScene(sceWin);
		delete sceWin;
		Scene *sceMain = new SceneMainMenu(*core);
		man.addScene(sceMain);
		man.setActiveScene(sceMain);
		static_cast<SceneGame*>(sceGame)->audioManager.stopSounds();
		static_cast<SceneGame*>(sceGame)->audioManager.playMusic(MAIN);
	}
}

static void nextLevelCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;

	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = core->getSceneManager();
		Scene *sceWin = man.getScene("win solo");
		man.removeScene(sceWin);
		Scene *sceGame = man.getScene("game");
		if (!sceGame) {
			sceGame = SceneGame::instance;
			man.addScene(sceGame);
		}
		man.setActiveScene(sceGame);

		SceneGame::instance->nextLevel();
		delete (sceWin);
	}
}

static void newGameCallback(IMenuModule &mod, MenuModuleKey key, void *userdata) {
	(void)mod;
	(void)key;
	(void)userdata;
	if (key == MenuKeyEnter) {
		Core *core = reinterpret_cast<Core*>(userdata);
		SceneManager &man = core->getSceneManager();

		Scene *sceGame = man.getScene("game");
		Scene *sceWin = man.getScene("win solo");

		SceneGame::instance->client.newGame(1, 1, RANDSOLO);
		while (!SceneGame::instance->getMap()) {
			if (SceneGame::instance->client.error)
				return ;
			else
				usleep(10);
		}
		if (!sceGame) {
			sceGame = SceneGame::instance;
			man.addScene(sceGame);
		}
		static_cast<SceneGame*>(sceGame)->audioManager.stopSounds();
		static_cast<SceneGame*>(sceGame)->audioManager.playRandomMusic();
		man.setActiveScene(sceGame);
		man.removeScene(sceWin);
		delete (sceWin);
	}
}

SceneWinSolo::SceneWinSolo(Core &core, int score) {
	(void)core;

	_score = score;

	if (SceneGame::instance->getLevel() < BOSSRATIO * 3)
		_menu.addModule(new MenuButton("Next Level", nextLevelCallback, &core));
	else
		_menu.addModule(new MenuButton("New game", newGameCallback, &core));
	_menu.addModule(new MenuButton("Back to Menu", backToMenuCallback, &core));
}

SceneWinSolo::~SceneWinSolo() {
}

void SceneWinSolo::eventResize(Core &core, int width, int height) {
	_menu.resize(core);
	(void)width;
	(void)height;
}

void SceneWinSolo::eventKeyPress(Core &core, int key, int mods) {
	(void)mods;
	_menu.keyPress(core, key);
}

void SceneWinSolo::eventButtonPress(Core &core, int key, int mods) {
	(void)mods;
	if (key == GLFW_MOUSE_BUTTON_LEFT)
	{
		const Window &win = core.getWindow();
		_menu.buttonPress(core, Vec2(
			(win.getMouseX() / win.getWidth() - 0.5) * 2,
			(win.getMouseY() / win.getHeight() - 0.5) * -2));
	}
}

void SceneWinSolo::update(Core &core) {
	(void)core;
	_menu.update(core);
}

void SceneWinSolo::render(Core &core) {
	SceneGame::instance->renderBackground();
	glClear(GL_DEPTH_BUFFER_BIT);
	_menu.render(core);
	Renderer &rend = core.getRenderer();
	std::string txt;
	if (SceneGame::instance->getLevel() < BOSSRATIO * 3)
		txt = "\nLevel complete\nScore : " + std::to_string(_score) + "\n";
	else 
		txt = "\nYou WON !\nScore : " + std::to_string(_score) + "\n";
	Vec2 sc = rend.getTextSize("X");
	Vec2 ss = rend.getTextSize(txt);
	Vec2 st = ss + sc;
	Vec4 rectSize(st * -0.5 + Vec2(0, sc[1] * 8), st);

	// texture.bind();
	// rend.renderTexture(rectSize, Vec4(1, 1, 1, 0.0));
	rend.renderRect(rectSize, Vec4(0.5, 0.5, 0.5, 0.8));
	rend.renderText(txt, rectSize.xy() + Vec2(0, ss[1]) + sc * 0.5);
}

const std::string &SceneWinSolo::getName() {
	static const std::string name("win solo");
	return (name);
}

void SceneWinSolo::setMap(int mapName) {
	map = mapName;
}
