#include <cmath>
#include "Character.hpp"
#include "Map.hpp"
#include "Bomb.hpp"
#include "PowerUp.hpp"
#include "SceneGame.hpp"
#include "SceneWinSolo.hpp"
#include <Client.hpp>


Character::Character(Vec2 spawn, std::string name, int speed, bool client, int maxBombs, int bombPow, bool drillBomb, bool bombControl)
{
	_walkAnim = 0;
	_pos = spawn;
	_dest = _pos;
	_name = name;
	_tab_pos = -1;
	_hp = 1;
	setSpeed(speed);
	_base_speed = speed;
	_mod_speed = 0;
	_moving = false;
	_wannaPutBomb = false;
	_bombNb = 0;
	setMaxBombs(maxBombs);
	_base_maxBombs = maxBombs;
	_mod_maxBombs = 0;
	_mov.push_back(Vec2(0, 0));
	setBombPow(bombPow);
	_base_bombPow = bombPow;
	_mod_bombPow = 0;
	_dir = Vec2(0.0, 0.0);
	_overBomb = NULL;
	_controlBomb = bombControl;
	_drillBomb = drillBomb;
	bzero(&_ltt, sizeof(_ltt));
    bzero(&_ntt, sizeof(_ntt));
	_dead = false;
	_IA = NOTIA;
	_animate = false;
	_score = 1000;
	_time = 0;
	_client = client;
}

// Check for client/serv creation from load.
Character::Character(t_player player, bool client) : Character() {
	setSpeed(player.speed);
	_base_speed = player.speed;
	_mod_speed = 0;
	_bombNb = 0;
	setMaxBombs(player.maxBombs);
	_base_maxBombs = player.maxBombs;
	_mod_maxBombs = 0;
	setBombPow(player.bombPow);
	_base_bombPow = player.bombPow;
	_mod_bombPow = 0;
	_drillBomb = player.drillBomb;
	_controlBomb = player.bombCtrl;
	_client = client;
}

Character::Character(Character const& src) {
	*this = src;
}

Character::~Character() {
	for (size_t i = 0; i < _powers.size();i++) {
		delete (_powers[i]);
		_powers[i] = NULL;
	}
	_powers.clear();
}

Character& Character::operator=(Character const& rhs) {
	_walkAnim = 0;
	_pos = rhs._pos;
	_dest = _pos;
	_name = rhs._name;
	_tab_pos = rhs._tab_pos;
	_hp = rhs._hp;
	setSpeed(rhs._speed);
	_base_speed = rhs._base_speed;
	_mod_speed = rhs._mod_speed;
	_moving = rhs._moving;
	_wannaPutBomb = false;
	_bombNb = 0;
	setMaxBombs(rhs._maxBombs);
	_base_maxBombs = rhs._base_maxBombs;
	_mod_maxBombs = rhs._mod_maxBombs;
	_mov.push_back(Vec2(0, 0));
	setBombPow(rhs._bombPow);
	_base_bombPow = rhs._base_bombPow;
	_mod_bombPow = rhs._mod_bombPow;
	_dir = Vec2(0.0, 0.0);
	_overBomb = rhs._overBomb;
	_controlBomb = rhs._controlBomb;
	_drillBomb = rhs._drillBomb;
	bzero(&_ltt, sizeof(_ltt));
    bzero(&_ntt, sizeof(_ntt));
	_dead = rhs._dead;
	_IA = rhs._IA;
	_animate = rhs._animate;
	_score = rhs._score;
	_time = rhs._time;
	_client = rhs._client;
	return (*this);
}

bool	Character::exploded(Map *map, bool drill, Character *owner) {

	(void)map;
	(void)drill;

	_dead = true;
	if (_client && _IA == NOTIA)
		SceneGame::instance->audioManager.playSound(DEATH);
	else if (_client)
		SceneGame::instance->audioManager.playSound(DEATH); // IA Death sound.

	if (owner != NULL) {
		if (owner != this)
			owner->addScore(100);
		else
			owner->addScore(-100);
	}
	return false;
}

bool	Character::isCollide() {
	return (false);
}

void	Character::update(Map *map) {
	if (_dead) // MAY CAUSE SEVERE ISSUES
		return;
	if (!_ltt.tv_sec)
		gettimeofday(&_ltt, NULL);
	gettimeofday(&_ntt, NULL);

	if (_dead)
		return ;
	double df = static_cast<double>((_ntt.tv_sec + _ntt.tv_usec / SECOND) - (_ltt.tv_sec + _ltt.tv_usec / SECOND)) * _speed;
	_time += df;
	if (_time > 2.0) {
		_time -= 2.0;
		addScore(-1);
	}

	gettimeofday(&_ltt, NULL);
	for (int j = 0; j < INT(_powers.size()); j++) {
		_powers[j]->updateTime();
	}

	if (_mov.back() != Vec2(0, 0))
		_dir = _mov.back();
	if (_moving && map) {
		_dest[0] = _pos[0] + (((df * _dir[0])));
		_dest[1] = _pos[1] + (((df * _dir[1])));

		Vec2 _rpos = Vec2(round(_pos[0]), round(_pos[1]));
		AMapObject *obj = map->getObj(round(_dest[0] + (0.5 * _dir[0])), round(_dest[1] + (0.5 * _dir[1])));

		if (!obj)
			return ;
		if (_overBomb != NULL &&  map->getObj(INT(_pos[0] + 0.5), INT(_pos[1] + 0.5)) != _overBomb)  {
			_overBomb = NULL;
		}
		if (!obj->isCollide() || obj == _overBomb){
			if (_dir[0] && _pos[1] != _rpos[1]) {
				if (_pos[1] < _rpos[1]) {
					_pos[1] += df;
					if (_pos[1] > _rpos[1])
						_pos[1] = _rpos[1];
				}
				else if (_pos[1] > _rpos[1]) {
					_pos[1] -= df;
					if (_pos[1] < _rpos[1])
						_pos[1] = _rpos[1];
				}
			}
			else if (_dir[1] && _pos[0] != _rpos[0]) {
				if (_pos[0] < _rpos[0]) {
					_pos[0] += df;
					if (_pos[0] > _rpos[0])
						_pos[0] = _rpos[0];
				}
				else if (_pos[0] > _rpos[0])  {
					_pos[0] -= df;
					if (_pos[0] < _rpos[0])
						_pos[0] = _rpos[0];
				}
			}
			else {
				_pos[0] = _dest[0];
				_pos[1] = _dest[1];
			}
		}
	}
	AMapObject *currObj = map->getObj(round(_pos[0]), round(_pos[1]));
	if (currObj && currObj->getObjType() == POWERUP && ((_IA == PLAYERLIKE && dynamic_cast<PowerUp*>(currObj)->getEffect() != EXIT) || (_IA == NOTIA && (dynamic_cast<PowerUp*>(currObj)->getEffect() != EXIT || map->victory())))) {
		map->removeObject(currObj);
		addPow(dynamic_cast<PowerUp *>(currObj));
	}
	if (_wannaPutBomb && _bombNb < _maxBombs) {
		Vec2 bombPos = Vec2(round(_pos[0]), round(_pos[1]));
		if (!map->getObj(round(_pos[0]), round(_pos[1]))->isCollide()) {
			_wannaPutBomb = false;
			Bomb *bomb = new Bomb(this, bombPos, NULL, _bombPow, !_controlBomb, _drillBomb, _controlBomb);
			SceneGame::instance->client.sendToServ(PUTBOMB, 1, _pos);
			addBomb(bomb);
			map->putObject(bomb);
			_overBomb = bomb;
			_bombNb++;
		}
	}
}


void	Character::move(const Vec2 direction) {
	if (!_dead) {
		_animate = true;
		_mov.push_back(direction);
		_moving = true;
	}
}

void	Character::unmove() {
	unmove(Vec2(0, 1));
	unmove(Vec2(0, -1));
	unmove(Vec2(-1, 0));
	unmove(Vec2(1, 0));
}

void	Character::unmove(const Vec2 direction) {
	if (!_dead) {
		for (int i = INT(_mov.size()) - 1; i > 0; i--) {
			if (_mov[i] == direction)
				_mov.erase(_mov.begin() + i);
		}
		if (_mov.back()[0] == 0.0 && _mov.back()[1] == 0.0) {
			_moving = false;
			_animate = false;
		}
	}
}

void	Character::putBomb() {
	if (!_dead)
		_wannaPutBomb = true;
}

void	Character::unputBomb() {
	if (!_dead)
		_wannaPutBomb = false;
}

void	Character::detonate(Map *map) {
	if (_controlBomb && !_dead) {
		int size = _bombs.size();
		for (int i = 0; i < size; i++) {
			if (_bombs.size())
				_bombs[0]->exploded(map, _drillBomb, this);
		}
	}
}

void	Character::lessBomb() {
	if (_bombNb <= 0) {
		std::cout << "bomb < 0. there might be an error in your code." << std::endl;
		_bombNb = 1;
	}
	_bombNb--;
}

int		Character::getTp() const {
	return _tab_pos;
}

void	Character::setTp(int pos) {
	_tab_pos = pos;
}
void	Character::tpMinus() {
	_tab_pos--;
	if (_tab_pos < 0) {
		std::cout << "tpminus: ERROR. there might be something horribly wrong with your code." << std::endl;
		_tab_pos = 0;
	}
}

void	Character::addEffect(PowerUp *pow) {
	if (pow->getEffect() == SPEED) {
		_mod_speed += pow->getValue();
		setSpeed(_base_speed + _mod_speed);
	} else if (pow->getEffect() == BNB) {
		_mod_maxBombs += pow->getValue();
		setMaxBombs(_base_maxBombs + _mod_maxBombs);
	} else if (pow->getEffect() == BPOW) {
		_mod_bombPow += pow->getValue();
		setBombPow(_base_bombPow + _mod_bombPow);
	} else if (pow->getEffect() == DBOMB) {
			_drillBomb = true;
		for (size_t i = 0 ; i < _bombs.size(); i++) {
			_bombs[i]->setDrill(true);
		}
	} else if (pow->getEffect() == CBOMB) {
		_controlBomb = true;
	} else if (pow->getEffect() == EXIT && _IA == NOTIA) {
		unmove();
		unputBomb();

		// Don't add exit effect on client's side character.
		for (size_t i = 0; i < SceneGame::instance->getMap()->getPlayers().size(); i++) {
			if (SceneGame::instance->getMap()->getPlayer(i) == this){
				delete(pow);
				return ;
			}
		}

		Scene *sceWin = new SceneWinSolo(SceneGame::instance->core, _score);
		SceneManager &man = SceneGame::instance->core.getSceneManager();
		man.addScene(sceWin);
		man.setActiveScene(sceWin);
		dynamic_cast<SceneWinSolo*>(sceWin)->setMap(SceneGame::instance->getMap()->getName());
		delete(pow);
	}
}

void	Character::removeEffect(PowerUp *pow) {
	if (pow->getEffect() == SPEED) {
		_mod_speed -= pow->getValue();
		setSpeed(_base_speed + _mod_speed);
	} else if (pow->getEffect() == BNB) {
		_mod_maxBombs -= pow->getValue();
		setMaxBombs(_base_maxBombs + _mod_maxBombs);
	} else if (pow->getEffect() == BPOW) {
		_mod_bombPow -= pow->getValue();
		setBombPow(_base_bombPow + _mod_bombPow);
	} else if (pow->getEffect() == DBOMB) {
		_drillBomb = false;
		for (size_t i = 0 ; i < _bombs.size(); i++) {
			_bombs[i]->setDrill(false);
		}

	} else if (pow->getEffect() == CBOMB) {
		_controlBomb = false;
	}
}

void	Character::addPow(PowerUp *pow) {
	if (pow->getEffect() != EXIT) {
		pow->setTp(_powers.size());
		pow->setParent(this);
		_powers.push_back(pow);
	}
	addEffect(pow);

}

void	Character::removePow(int tpos) {
	if (tpos == -1 || tpos >= INT(_powers.size()))
		std::cout << "ERROR removePow: i out of range !!!!" << std::endl;
	else {
		removeEffect(_powers[tpos]);
		_powers[tpos]->setTp(-1);
		_powers[tpos]->setParent(NULL);
		_powers.erase(_powers.begin() + tpos);
		for (size_t i = UINT(tpos) ; i < _powers.size(); i++) {
			_powers[i]->tpMinus();
		}
	}
}

void	Character::setSpeed(int speed) {
	_speed = speed;
	if (_speed <= 1)
		_speed = 2;
	else if (_speed >= 8)
		_speed = 8;
}

void	Character::setMaxBombs(int maxBombs) {
	_maxBombs = maxBombs;
	if (_maxBombs <= 1)
		_maxBombs = 1;
	else if (_maxBombs >= 9)
		_maxBombs = 9;
}

void	Character::setBombPow(int bombPow) {
	_bombPow = bombPow;
	if (_bombPow <= 1)
		_bombPow = 1;
	else if (_bombPow >= 9)
		_bombPow = 9;
}

void	Character::setBombDrill(bool drillBomb) {
	_drillBomb = drillBomb;
}

void	Character::setBombControl(bool controlBomb) {
	_controlBomb = controlBomb;
}

void	Character::addBomb(Bomb *bomb) {
	bomb->setTp(_bombs.size());
	_bombs.push_back(bomb);

}

void	Character::removeBomb(int tpos) {
	if (tpos == -1 || tpos >= INT(_bombs.size()))
		std::cout << "ERROR removebomb: i out of range !!!!" << std::endl;
	else {
		_bombs[tpos]->setTp(-1);
		_bombs.erase(_bombs.begin() + tpos);
		for (size_t i = UINT(tpos) ; i < _bombs.size(); i++) {
			_bombs[i]->tpMinus();
		}
	}
}
Bomb		*Character::getBomb(size_t tpos) {
	if (tpos > 0 && tpos < _bombs.size())
		return _bombs[tpos];
	return (NULL);
}

std::vector<Bomb *>		Character::getBombs() {
	return _bombs;
}
const Vec2 &Character::getDir() const {
	return (_dir);
}
float Character::getSpeed() const {
	return (_speed);
}

int		Character::getMaxBombs() const {
	return (_maxBombs);
}

int		Character::getBombPow() const {
	return (_bombPow);
}

OBJTYPE	Character::getObjType() const {
    return	CHARACTER;
}

void	Character::setName(std::string name) {
	_name = name;
}

void	Character::setPosition(Vec2 pos, Vec2 dir, bool anim) {
	_animate = anim;
	if (pos[0] >= 0 && pos[1] >= 0)
		_pos = pos;
	_dir = dir;
}

bool	Character::isDead() const {
	return (_dead);
}

void	Character::putBombAt(Vec2 pos, Map *map) {
	Vec2 bombPos = Vec2(INT(round(pos[0])), INT(round(pos[1])));
	if (!_dead && map && !map->getObj(round(pos[0]), round(pos[1]))->isCollide()) {
		Bomb *bomb = new Bomb(this, bombPos, NULL, _bombPow, !_controlBomb, _drillBomb, _controlBomb);
		addBomb(bomb);
		map->putObject(bomb);
		_overBomb = bomb;
		_bombNb++;
	}
}

void	Character::setIA(eIAType val) {
	_IA = val;
}
float Character::getWalkAnim(float delta)
{
	float speed = delta * _speed;
	if (_animate)
		_walkAnim += speed;
	else
		_walkAnim = std::max(0.0f, std::floorf(_walkAnim) - speed);
	return (_walkAnim);
}

bool	Character::getAnimate() const {
	return	(_animate);
}

void	Character::addScore(int toAdd) {
	_score += toAdd;
	if (_score < 0)
		_score = 0;
}

std::string	Character::getName() const {
	return (_name);
}

bool	Character::hasDrill() const {
	return (_drillBomb);
}
bool	Character::hasControl() const {
	return (_controlBomb);
}

t_player	Character::toStruct() const {
	t_player	player;

	player.saved = true;
	player.speed = _speed;
	player.maxBombs = _maxBombs;
	player.bombPow = _bombPow;
	player.drillBomb = _drillBomb;
	player.bombCtrl = _controlBomb;

	return (player);
}

bool		Character::isClient() const {
	return (_client);
}

int			Character::getScore() const {
	return (_score);
}

void		Character::setScore(int val) {
	_score = val;
}
