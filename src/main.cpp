/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrouthie <jrouthie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/05 17:37:02 by jrouthie          #+#    #+#             */
/*   Updated: 2018/06/05 18:30:57 by jrouthie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmglfw.hpp"
#include "SceneMainMenu.hpp"
#include "SceneGame.hpp"
#include "Game.hpp"

int main(int ac, char **av)
{
    (void)ac;
    (void)av;


	Core core;
	Game game;

	MapGenerator::initMapGenerator();

    SceneManager &man = core.getSceneManager();

	Scene *sceGame = new SceneGame(core);
    Scene *sceMain = new SceneMainMenu(core);
	(void)sceGame;
	// man.addScene(sceGame);
	man.addScene(sceMain);
	man.setActiveScene(sceMain);

	core.run();

    return (0);
}
