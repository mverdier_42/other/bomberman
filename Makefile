
RM ?= rm -f

CXX = clang++

# SRC = main.cpp
# SRCS = $(addprefix src/, $(SRC))
SRCS = $(wildcard src/*.cpp)
SRC = $(notdir $(SRCS))

OBJS = $(addprefix obj/, $(SRC:.cpp=.o))
DEPS = $(addprefix dep/, $(SRC:.cpp=.d))

CPPFLAGS = -Wall -Wextra -Werror -g -I include \
	-I libmmatrix/include -I libmglfw/include \
	-I ~/.brew/Cellar/nlohmann_json/3.1.2/include \
	`pkg-config --cflags glfw3` \
	`pkg-config --cflags glew` \
	`pkg-config --cflags sdl_mixer` \
	-std=c++11 #\
	-I ~/.brew/Cellar/libsndfile/1.0.28/include
LDFLAGS= `pkg-config --static --libs glfw3` \
	`pkg-config --static --libs glew` \
	`pkg-config --static --libs sdl_mixer` \
	-framework OPENGL #\
	-L ~/.brew/Cellar/libsndfile/1.0.28/lib
LDLIBS= -L libmmatrix -lmmatrix -L libmglfw -lmglfw -lSDL_mixer


NAME = bomberman
.PHONY: all clean fclean run debug re run libmmatrix libmglfw install


default:all

install:
	brew tap nlohmann/json
	brew install pkgconfig glfw glew nlohmann_json libsndfile portaudio sdl_mixer

libmmatrix:
	make all -C libmmatrix
libmglfw:
	make all -C libmglfw

all: libmmatrix libmglfw $(NAME)

$(NAME): $(DEPS) $(OBJS)
	$(CXX) $(LDFLAGS) -o $(NAME) $(OBJS) $(LDLIBS)

obj/%.o: src/%.cpp
	$(CXX) -o $@ -c $< $(CPPFLAGS)
dep/%.d: src/%.cpp
	@echo "generate $@"
	@set -e; $(RM) $@; \
	$(CXX) -MM $(CPPFLAGS) $< > $@.$$$$; \
		sed 's,\($*\)\.o[ :]*,obj/\1.o $@ : ,g' < $@.$$$$ >> $@; \
		$(RM) $@.$$$$

depend: $(DEPS)

clean:
	$(RM) $(OBJS)
	$(RM) $(DEPS)
	make clean -C libmmatrix
	make clean -C libmglfw
fclean:clean
	$(RM) $(NAME)
	$(RM) .depend
	make fclean -C libmmatrix
	make fclean -C libmglfw
re:fclean all
run:
	./$(NAME) $(ARGS)
debug:all run

-include $(DEPS)
